#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include <QObject>
#include <QSharedPointer>
#include "commondefines.h"

class Playlist;

class AudioPlayer : public QObject
{
    Q_OBJECT

public:
    explicit AudioPlayer(QObject *parent = nullptr);
    AudioPlayer(const AudioPlayer&) = delete;
    AudioPlayer& operator=(const AudioPlayer&) = delete;
    ~AudioPlayer();

    Q_INVOKABLE void play();
    Q_INVOKABLE void pause();
    Q_INVOKABLE void setVolume(int volume);
    Q_INVOKABLE int  getVolume();
    Q_INVOKABLE void playNext();
    Q_INVOKABLE void playPrev();
    Q_INVOKABLE void setCurIndex(quint32 index);
    Q_INVOKABLE bool isPlaying();
    Q_INVOKABLE void setPlaylist(ID id);
    Q_INVOKABLE int  getPlaylistIdByTitle(const QString& title);
    Q_INVOKABLE void updateAlbums();

public slots:
    void successfulSync();

signals:
    void playerCurrentItemChanged(int newIndex);
    Q_INVOKABLE void sync();
    Q_INVOKABLE void addSong(const QString& audiofilePath,
                             const QString& coverfilePath,
                             const QString& songTitle,
                             const QString& albumTitle,
                             const QString& artistTitle,
                             quint16        year);
    Q_INVOKABLE void addSongByAlbum(const QString& audiofilePath,
                                    const QString& songTitle,
                                    ID             albumId);
    Q_INVOKABLE void addPlaylist(const QString& playlistTitle,
                                 const QString& songsIdStr);
    Q_INVOKABLE void editSong(quint32        songId,
                              quint16        year,
                              const QString& songTitle,
                              const QString& albumTitle,
                              const QString& artistTitle,
                              const QString& audiofilePath,
                              const QString& coverfilePath);
    Q_INVOKABLE void editPlaylist(ID             playlistId,
                                  const QString& newTitle,
                                  const QString& songForAdd,
                                  const QString& songsForDelete);
    Q_INVOKABLE void deleteSong(ID songId);
    Q_INVOKABLE void deletePlaylist(ID playlistId);

ACCESS:
    Playlist * m_curPlaylist;
};

#endif // AUDIOPLAYER_H
