#ifndef PLAYLISTDATA_H
#define PLAYLISTDATA_H

#include <QVector>
#include <QSharedPointer>
#include "commondefines.h"

class QString;
class Metadata;

class PlaylistData
{
public:
    using sharedMetadata = QSharedPointer<Metadata>;

    PlaylistData(const ID&          id,
                 const QString&     title,
                 const QVector<ID>& identificators);
    PlaylistData(const PlaylistData&) = default;
    PlaylistData& operator=(const PlaylistData &) = default;
    ~PlaylistData() = default;

public:
    ID                      getId();
    const QString&          getTitle();
    QVector<sharedMetadata> getVectorMetadata();
    const QVector<ID>&      getVectorIdentificators();
    unsigned int            getCountOfidentificators();

    ID getNextId(ID id);
    ID getPrevId(ID id);

ACCESS:
    ID          m_id;
    QString     m_title;
    QVector<ID> m_identificators;
};

#endif // PLAYLISTDATA_H
