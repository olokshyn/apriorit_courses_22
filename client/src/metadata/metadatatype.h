#ifndef METADATATYPE_H
#define METADATATYPE_H

#include <QString>
#include "../commondefines.h"

class MetadataType
{
public:
    MetadataType(quint16         year,
                 ID              albumId,
                 ID              artistId,
                 const QString&  albumTitle,
                 const QString&  artistTitle,
                 const QString&  coverfileName);
    MetadataType(const MetadataType&) = delete;
    MetadataType& operator=(const MetadataType&) = delete;
    ~MetadataType() = default;

public:
    quint16        getYear();
    ID             getAlbumId();
    ID             getArtistId();
    const QString& getAlbumTitle();
    const QString& getArtistTitle();
    const QString& getCoverfileName();

    void setYear(quint16 year);
    void setAlbumTitle(const QString& albumTitle);
    void setArtistTitle(const QString& artistTitle);
    bool coverfileExists();

private:
    quint16 m_year;
    ID      m_albumId;
    ID      m_artistId;
    QString m_albumTitle;
    QString m_artistTitle;
    QString m_coverfileName;
};

#endif // METADATATYPE_H
