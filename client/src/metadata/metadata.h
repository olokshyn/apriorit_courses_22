#ifndef METADATA_H
#define METADATA_H

#include <QString>
#include <QSharedPointer>
#include "../commondefines.h"

class MetadataType;

class Metadata
{
public:
    using sharedMetatype = QSharedPointer<MetadataType>;

    Metadata(ID             songId,
             const QString& songTitle,
             const QString& audiofileName,
             sharedMetatype metaType);
    Metadata(const Metadata&) = delete;
    Metadata& operator=(const Metadata&) = delete;
    ~Metadata() = default;

public:
    ID             getId();
    quint16        getYear();
    ID             getAlbumId();
    const QString& getAlbumTitle();
    ID             getArtistId();
    const QString& getArtistTitle();
    const QString& getSongTitle();
    const QString& getAudiofileName();
    const QString& getCoverfileName();

    bool           coverfileExists();
    bool           audiofileExists();

private:
    ID             m_id;
    QString        m_songTitle;
    const QString  m_audiofileName;
    sharedMetatype m_metadataType;
};

#endif // METADATA_H
