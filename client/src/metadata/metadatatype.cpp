#include "metadatatype.h"
#include <QFile>
#include "../settings.h"

MetadataType::MetadataType(
         quint16         year,
         ID              albumId,
         ID              artistId,
         const QString&  albumTitle,
         const QString&  artistTitle,
         const QString&  coverfileName)
    : m_year(year),
      m_albumId(albumId),
      m_artistId(artistId),
      m_albumTitle(albumTitle),
      m_artistTitle(artistTitle),
      m_coverfileName(coverfileName)
{}

quint16 MetadataType::getYear() {
    return m_year;
}

ID MetadataType::getAlbumId() {
    return m_albumId;
}

const QString& MetadataType::getAlbumTitle() {
    return m_albumTitle;
}

ID MetadataType::getArtistId() {
    return m_artistId;
}

const QString& MetadataType::getArtistTitle() {
    return m_artistTitle;
}

const QString& MetadataType::getCoverfileName() {
    return m_coverfileName;
}

void MetadataType::setYear(quint16 year) {
    m_year = year;
}

void MetadataType::setAlbumTitle(const QString& albumTitle) {
    m_albumTitle = albumTitle;
}

void MetadataType::setArtistTitle(const QString& artistTitle) {
    m_artistTitle = artistTitle;
}

bool MetadataType::coverfileExists() {
    QFile file(Settings::Instance().getPathToCoverfiles()
               + m_coverfileName);
    return file.exists();
}
