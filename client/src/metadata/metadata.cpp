#include "metadata.h"
#include "../settings.h"
#include <QString>
#include <QFile>
#include "metadatatype.h"
#include "../Database/database.h"
#include "metadatafactory.h"

Metadata::Metadata(ID             songId,
                   const QString& songTitle,
                   const QString& audiofileName,
                   sharedMetatype metaType)
    : m_id(songId),
      m_songTitle(songTitle),
      m_audiofileName(audiofileName),
      m_metadataType(metaType)
{}

ID Metadata::getId() {
    return m_id ;
}
const QString& Metadata::getSongTitle() {
    return m_songTitle;
}
const QString& Metadata::getAudiofileName() {
    return m_audiofileName;
}

quint16 Metadata::getYear() {
    return m_metadataType->getYear();
}
ID Metadata::getAlbumId() {
    return m_metadataType->getAlbumId();
}
const QString& Metadata::getAlbumTitle() {
    return m_metadataType->getAlbumTitle();
}
ID Metadata::getArtistId() {
    return m_metadataType->getArtistId();
}
const QString& Metadata::getArtistTitle() {
    return m_metadataType->getArtistTitle();
}
const QString& Metadata::getCoverfileName() {
    return m_metadataType->getCoverfileName();
}

bool Metadata::audiofileExists() {

    QFile file(Settings::Instance().getPathToAudiofiles()
               + m_audiofileName);
    return file.exists();
}

bool Metadata::coverfileExists() {
    return m_metadataType->coverfileExists();
}
