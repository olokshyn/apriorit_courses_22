#include "metadatafactory.h"
#include "metadatatype.h"

MetadataFactory::MetadataFactory()
    : m_types()
{}

MetadataFactory::sharedMetatype
MetadataFactory::getMetadataType(quint16         year,
                                 ID              albumId,
                                 const QString&  albumTitle,
                                 ID              artistId,
                                 const QString&  artistTitle,
                                 const QString&  coverfileName) {
    if(m_types.contains(albumId)) {
        sharedMetatype& type  = m_types[albumId];
        type->setYear(year);
        type->setAlbumTitle(albumTitle);
        type->setArtistTitle(artistTitle);
        return m_types.value(albumId);
    }
    //if type not found, will create a new type
    return m_types.insert(albumId,(
                          sharedMetatype::create(
                                year,
                                albumId,
                                artistId,
                                albumTitle,
                                artistTitle,
                                coverfileName))
                          ).value();
}

const MetadataFactory::listTypes MetadataFactory::getAllTypes() {
    return m_types.values();
}

MetadataFactory::iter MetadataFactory::end() {
    return m_types.end();
}

void MetadataFactory::deleteMetaType(ID albumId) {
    m_types.remove(albumId);
}

MetadataFactory::iter
MetadataFactory::getMetaType(ID albumId) {
    return m_types.find(albumId);
}

QList<ID> MetadataFactory::getAlbumsByArtist(ID artistId) {
    QList<ID> identificators;
    for(const sharedMetatype& type: m_types) {
        if(type->getArtistId() == artistId) {
            identificators.push_back(type->getAlbumId());
        }
    }
    return identificators;
}






