#ifndef METADATAFACTORY_H
#define METADATAFACTORY_H

#include <QHash>
#include <QList>
#include <QSharedPointer>
#include "../commondefines.h"

class MetadataType;

class MetadataFactory
{
public:
    using sharedMetatype = QSharedPointer<MetadataType>;
    using listTypes = QList<sharedMetatype>;
    using hashTypes = QHash<ID, sharedMetatype>;
    using iter = hashTypes::iterator;
    MetadataFactory();

public:
    sharedMetatype getMetadataType(quint16         year,
                                   ID              albumId,
                                   const QString&  albumTitle,
                                   ID              artistId,
                                   const QString&  artistTitle,
                                   const QString&  coverfileName);
    const listTypes getAllTypes();
    iter            end();
    iter            getMetaType(ID albumId);
    void            deleteMetaType(ID albumId);
    QList<ID>       getAlbumsByArtist(ID artistId);

private:
    hashTypes m_types;
};

#endif // METADATAFACTORY_H
