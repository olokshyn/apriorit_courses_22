#include "settings.h"
#include <QSettings>
#include <QDate>
#include <QTime>
#include <QDir>
#include <QDebug>

Settings::Settings(QObject * parent)
    : QObject(parent) {
    m_qsettings = new QSettings("./client_settings.ini",
                             QSettings::IniFormat);
}

Settings::~Settings() {
    delete m_qsettings;
}

Settings& Settings::Instance() {
    static Settings model;
    return model;
}

void Settings::setVolume(int newVolume) {
    m_qsettings->setValue("lastVolume", newVolume);
}

int Settings::getVolume() {
    return m_qsettings->value("lastVolume", 50).toInt();
}

QString Settings::getDateLastSync() {
    QString datetime = QDate(2018,1,1).toString(m_formatDateLastSync) + " " +
            QTime(0,0,0).toString(m_formatTimeLastSync);

    return m_qsettings->value("DateLastSync", datetime).toString();
}

void Settings::setDateLastSync(const QDate& newDate,
                               const QTime& newTime) {
    m_qsettings->setValue("DateLastSync", newDate.toString(m_formatDateLastSync) + " "
                                            + newTime.toString(m_formatTimeLastSync));
}

void Settings::setPathToAudiofiles(const QString& path) {    
    copyFiles(getPathToAudiofiles(), path);
    m_qsettings->setValue("PathToAudiofiles", path);
    emit audioPathChanged(path);
}

QString Settings::getPathToAudiofiles() {
    QString path = (m_qsettings->value("PathToAudiofiles",
                           QDir::currentPath() + "/Music/"))
                    .toString();
    createDir(path);
    return path;
}

void Settings::setPathToCoverfiles(const QString& path) {
    copyFiles(getPathToCoverfiles(), path);
    m_qsettings->setValue("PathToCoverfiles", path);
}

QString Settings::getPathToCoverfiles() {
    QString path = (m_qsettings->value("PathToCoverfiles",
                          QDir::currentPath() + "/Covers/"))
                    .toString();
    createDir(path);
    return path;
}

void Settings::copyFiles(const QString& srcDirPath,
                         const QString& destDirPath) {
    QDir srcDir(srcDirPath);
    createDir(destDirPath);
    QStringList listOfFiles = srcDir.entryList(QDir::Files);
    for(const QString& nameOfFileForCopy: listOfFiles) {
        QFile srcFile(srcDirPath + nameOfFileForCopy);
        QFile::copy(srcDirPath  + nameOfFileForCopy,
                    destDirPath + nameOfFileForCopy); //copy
        srcFile.remove(); //delete old file
    }
}

void Settings::setServerIpAndPort(const QString& serverIpAndPort) {
    m_qsettings->setValue("ServerIpAndPort", serverIpAndPort);
    emit serverIpAndPortchanged(serverIpAndPort);
}

QString Settings::getServerIpAndPort() {
    return m_qsettings->value("ServerIpAndPort",
                              "localhost:50051") /*"10.100.26.140:50051"*/
                        .toString();
}

void Settings::setAudioMillisecPart(quint32 audioMillisecPart) {
    m_qsettings->setValue("AudioMillisecPart", QString::number(audioMillisecPart));
    emit audioMillisecPartChanged(audioMillisecPart);
}

quint32 Settings::getAudioMillisecPart() {
    return m_qsettings->value("AudioMillisecPart",
                              2000) //2 sec
                        .toUInt();
}

void Settings::createDir(const QString& path) {
    QDir dir(path);
    if(! dir.exists()) {
        dir.mkpath(".");
        qInfo() << "Settings::createDir\t Folder on path " << path << "is not exist. Forder was created.";
    }
}

void Settings::setGrpcBytesPortion(quint16 portion) {
    m_qsettings->setValue("SendBytesPortion", QString::number(portion));
}

quint16 Settings::getGrpcBytesPortion() {
    return m_qsettings->value("SendBytesPortion",
                              1024) //1Kb
                        .toUInt();
}


