#ifndef SENDWORKER_H
#define SENDWORKER_H

#include <QObject>
#include <string>
#include "../commondefines.h"

class SendWorker : public QObject
{
    Q_OBJECT
public:
    explicit SendWorker(QObject * parent = nullptr);
    SendWorker(const SendWorker&) = delete;
    SendWorker& operator=(const SendWorker&) = delete;
    ~SendWorker() = default;

signals:
    void sendStatusChanged(const QString& status);
    void resetSendStatus();

public slots:
    void sendSong(const QString& audiofilePath,
                  const QString& coverfilePath,
                  const QString& songTitle,
                  const QString& albumTitle,
                  const QString& artistTitle,
                        quint16 year);
    void sendSongByAlbum(const QString& audiofilePath,
                         const QString& songTitle,
                         quint32 albumId);
    void sendPlaylist(const QString& playlistTitle,
                      const QString& songsIdStr);
    void updateSong(quint32        songId,
                    quint16        year,
                    const QString& songTitle,
                    const QString& albumTitle,
                    const QString& artistTitle,
                    const QString& audiofilePath,
                    const QString& coverfilePath);
    void updatePlaylist(quint32        playlistId,
                        const QString& newTitle,
                        const QString& songForAdd,
                        const QString& songsForDelete);

    void deleteSong(quint32 songId);
    void deletePlaylist(quint32 playlistId);

private:
    std::string getDataFromFile(const QString& fileName);
};

#endif // SENDWORKER_H
