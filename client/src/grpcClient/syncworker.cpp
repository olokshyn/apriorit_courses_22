#include "syncworker.h"
#include "grpcmanager.h"
#include "../settings.h"
#include <QDebug>
#include <QDate>
#include <QTime>

SyncWorker::SyncWorker(QObject * parent)
    : QObject(parent)
{}

void SyncWorker::sync() {
    GrpcManager& grpcManager = GrpcManager::Instance();
    if(!grpcManager.connected()) {
        return;
    }

    emit syncStatusChanged("sync started");

    QString dateLastSync = Settings::Instance().getDateLastSync();

    if(grpcManager.
            getNewAudiosMetadataFromServer(dateLastSync) == STATUS_BAD) {
        qWarning() << "SyncWorker::sync\t fail while sync in getNewAudiosMetadataFromServer()";
        emit resetSyncStatus();
        return;
    }
    if(grpcManager.
            getNewPlaylistsDataFromServer(dateLastSync) == STATUS_BAD) {
        qWarning() << "SyncWorker::sync\t fail while sync in getNewAudiosMetadataFromServer()";
        emit resetSyncStatus();
        return;
    }
    if(grpcManager.
            getDeletedAudiosFromServer(dateLastSync) == STATUS_BAD) {
        qWarning() << "SyncWorker::sync\t fail while sync in getDeletedAudiosFromServer()";
        emit resetSyncStatus();
        return;
    }
    if(grpcManager.
            getDeletedPlaylistsFromServer(dateLastSync) == STATUS_BAD) {
        qWarning() << "SyncWorker::sync\t fail while sync in getDeletedPlaylistsFromServer()";
        emit resetSyncStatus();
        return;
    }
    if(grpcManager.
            getReloadedCoverfiles(dateLastSync) == STATUS_BAD) {
        qWarning() << "SyncWorker::sync\t fail while sync in getReloadedCoverfiles()";
        emit resetSyncStatus();
        return;
    }

    Settings::Instance().setDateLastSync(QDate::currentDate(),QTime::currentTime());

    emit successfulSync();
    emit resetSyncStatus();
}
