#include "grpcclient.h"
#include "../metadata/metadata.h"
#include "../playlistdata.h"
#include "../Database/database.h"
#include "../metadata/metadatafactory.h"
#include "../metadata/metadatatype.h"
#include <QString>
#include <QDebug>
#include <QVector>
#include <QStringList>

using grpc::Channel;
using grpc::ClientReader;
using grpc::ClientContext;
using grpc::ClientWriter;
using grpc::CompletionQueue;
using grpc::Status;
using grpc::StatusCode;
using ms::MS;
using ms::Bytes;
using ms::Str_of_id;
using ms::Date;
using ms::SongID;
using ms::Audiofile_info;
using ms::Audio_section;
using ms::New_song_data;
using ms::Empty;
using ms::Song_id_and_new_name;
using ms::Song_id_and_new_album_and_artist;
using ms::Artist;
using ms::PlaylistID;
using ms::Album_year;
using ms::AlbumId_and_songTitle;
using ms::Song_id_and_date;
using ms::Album_id_and_date;
using ms::Answer;
using ms::AlbumID;

GrpcClient::GrpcClient(std::shared_ptr<Channel> channel, QObject * parent)
    : QObject(parent),
      m_stub(MS::NewStub(channel)),
      m_timeOfLastConnAttempt(QTime::currentTime()),
      m_lastConnStatus(STATUS_OK)
{}

bool GrpcClient::connected() {
    if(m_lastConnStatus == STATUS_BAD
            && m_timeOfLastConnAttempt.msecsTo(QTime::currentTime()) < 300000) {
        //if last connection was failed and not a minute has passed since that moment
        qInfo() << "fail to connect";
    } else {
        Empty request;
        Empty reply;
        ClientContext context;
        context.set_deadline(std::chrono::system_clock::now()
                             + std::chrono::seconds(50));
        Status status = m_stub->test_connect(&context,
                                             request,
                                             &reply);
        if(status.error_code() == 14 ||
                status.error_code() == StatusCode::DEADLINE_EXCEEDED) {
            qInfo() << "fail to connect. error code: " << QString::number(status.error_code());
            m_timeOfLastConnAttempt = QTime::currentTime();
            m_lastConnStatus = STATUS_BAD;
        } else {
            m_lastConnStatus = STATUS_OK;
        }
    }
    return m_lastConnStatus;
}

//requests

bool GrpcClient::was_the_song_reloaded(ID             songId,
                                       const QString& dateLastSync,
                                       bool& ok) {
    Song_id_and_date request;
    request.set_song_id(songId);
    request.set_date(dateLastSync.toStdString());

    Answer reply;

    ClientContext context;

    Status status = m_stub->was_the_song_reloaded(
                            &context,
                            request,
                            &reply);
    bool replyAnswer = false;
    ok = statusOK(status, "was_the_song_reloaded");
    if(ok) {
        replyAnswer = reply.answer();
    }
    return replyAnswer;
}

bool GrpcClient::was_the_cover_reloaded(ID             albumId,
                                        const QString& dateLastSync,
                                        bool& ok) {
    Album_id_and_date request;
    request.set_album_id(albumId);
    request.set_date(dateLastSync.toStdString());

    Answer reply;

    ClientContext context;

    Status status = m_stub->was_the_cover_reloaded(
                            &context,
                            request,
                            &reply);
    bool replyAnswer = false;
    ok = statusOK(status, "was_the_cover_reloaded");
    if(ok) {
        replyAnswer = reply.answer();
    }
    return replyAnswer;
}

ID GrpcClient::send_albumId_and_title_of_new_song(
                                      ID albumId,
                                      const QString& songTitle,
                                      bool ok) {
    AlbumId_and_songTitle request;
    request.set_album_id(albumId);
    request.set_song_title(songTitle.toStdString());

    SongID reply;

    ClientContext context;

    Status status = m_stub->send_albumId_and_title_of_new_song(
                &context,
                request,
                &reply);
    ID songId = 0;
    ok = statusOK(status, "send_albumId_and_title_of_new_song");
    if(ok) {
        songId = reply.id();
    }
    return songId;
}

ID GrpcClient::send_metadata_of_new_song(const QString& songTitle,
                                         const QString& albumTitle,
                                         const QString& artistTitle,
                                         quint16        year,
                                         bool& ok) {
    New_song_data request;
    request.set_song_name(songTitle.toStdString());
    request.set_album_title(albumTitle.toStdString());
    request.set_artist_name(artistTitle.toStdString());
    request.set_year(year);

    SongID reply;

    ClientContext context;

    Status status = m_stub->send_metadata_of_new_song(&context,
                                                      request,
                                                      &reply);
    ID songId = 0;
    ok = statusOK(status, "send_metadata_of_new_song");
    if(ok) {
        songId = reply.id();
    }
    return songId;
}

void GrpcClient::send_new_playlist(
        const QString&     playlistTitle,
        const QString& songsIdentificators,
        bool&              ok) {
    ms::Playlist request;
    request.set_title(playlistTitle.toStdString());
    request.set_id_str(songsIdentificators.toStdString());

    Empty reply;

    ClientContext context;

    Status status = m_stub->send_new_playlist(&context,
                                              request,
                                              &reply);
    ok = statusOK(status, "send_new_playlist");
}

void GrpcClient::send_audio(ID songId,
                            const std::string& data,
                            quint16            bytesPortion,
                            bool&              ok) {
    Empty reply;
    ClientContext context;

    std::unique_ptr<ClientWriter<Bytes> > writer(
        m_stub->send_audio(&context, &reply));

    //send song id like first package
    Bytes firstPackage;
    firstPackage.set_str(std::to_string(songId));
    if(!writer->Write(firstPackage)) {
        qWarning() << "GrpcClient::send_audio\t"
                      "Fail while sending first package "
                      "for song id " << songId;
    } else {
        //send data
        size_t times = (data.size() / bytesPortion) + 1;
        float koefLoadStatus = 100.f / times;
        for (size_t i = 0; i < times; ++i) {
            Bytes bytes;
            bytes.set_str(data.substr(i * bytesPortion, bytesPortion));
            if(!writer->Write(bytes)) {
                std::cout << "GrpcClient::send_audio\t"
                             "Fail while sending " << i
                          << " package for song id " << songId;
                break;
            }
            emit loadStatusChanged("sending audio "
                                   + QString::number(static_cast<size_t>(i * koefLoadStatus)) + "%");
        }
    }
    writer->WritesDone();
    ok = statusOK(writer->Finish(), "send_audio");
    emit resetLoadStatus();
}

void GrpcClient::send_cover(ID songId,
                            const std::string& data,
                            const QString& fileExtention,
                            quint16            bytesPortion,
                            bool& ok) {
    Empty reply;
    ClientContext context;

    std::unique_ptr<ClientWriter<Bytes> > writer(
        m_stub->send_cover(&context, &reply));

    //send song id like first package
    Bytes firstPackage;
    firstPackage.set_str(std::to_string(songId));
    if(!writer->Write(firstPackage)) {
        qWarning() << "GrpcClient::send_cover\t"
                      "Fail while sending first package "
                      "for song id " << songId;
    } else {
        //send cover file extention
        Bytes secondPackage;
        secondPackage.set_str(fileExtention.toStdString());
        if(!writer->Write(secondPackage)) {
            qWarning() << "GrpcClient::send_cover\t"
                          "Fail while sending second package "
                          "for song id " << songId;
        } else {
            //send data
            size_t times = (data.size() / bytesPortion) + 1;
            float koefLoadStatus = 100.f / times;
            for (size_t i = 0; i < times; ++i) {
                Bytes bytes;
                bytes.set_str(data.substr(i * bytesPortion, bytesPortion));
                if(!writer->Write(bytes)) {
                    std::cout << "GrpcClient::send_audio\t"
                                 "Fail while sending " << i
                              << " package for song id " << songId;
                    break;
                }
                emit loadStatusChanged("sending audio "
                                       + QString::number(static_cast<size_t>(i * koefLoadStatus)) + "%");
            }
        }
    }
    writer->WritesDone();
    ok = statusOK(writer->Finish(), "send_cover");
    emit resetLoadStatus();
}

void GrpcClient::update_song_name(ID             songId,
                                  const QString& newSongTitle,
                                  bool&          ok) {
    Song_id_and_new_name request;
    request.set_song_id(songId);
    request.set_new_name(newSongTitle.toStdString());

    Empty reply;

    ClientContext context;

    Status status = m_stub->update_song_name(&context,
                                             request,
                                             &reply);
    ok = statusOK(status, "update_song_name");
}

void GrpcClient::update_album_of_song(ID             songId,
                                      const QString& newAlbumTitle,
                                      ID             artistId,
                                      quint16        year,
                                      bool&          ok) {
    Song_id_and_new_album_and_artist request;
    request.set_song_id(songId);
    request.set_new_album(newAlbumTitle.toStdString());
    request.set_artist_id(artistId);
    request.set_year(year);

    Empty reply;

    ClientContext context;

    Status status = m_stub->update_album_of_song(&context,
                                                 request,
                                                 &reply);
    ok = statusOK(status, "update_album_of_song");
}

void GrpcClient::update_artist(ID             artistId,
                               const QString& newArtistTitle,
                               bool&          ok) {
    Artist request;
    request.set_artist_id(artistId);
    request.set_new_artist_name(newArtistTitle.toStdString());

    Empty reply;

    ClientContext context;

    Status status = m_stub->update_artist(&context,
                                          request,
                                          &reply);
    ok = statusOK(status, "update_artist");
}

void GrpcClient::update_year(ID album_id,
                             quint16 newYear,
                             bool& ok) {
    Album_year request;
    request.set_album_id(album_id);
    request.set_new_year(newYear);

    Empty reply;

    ClientContext context;

    Status status = m_stub->update_year(&context,
                                        request,
                                        &reply);
    ok = statusOK(status, "update_year");
}

void GrpcClient::update_playlist(ID             id,
                                 const QString& songsIdentificators,
                                 const QString& playlistTitle,
                                 bool& ok) {
    ms::Playlist request;
    request.set_playlist_id(id);
    request.set_title(playlistTitle.toStdString());
    request.set_id_str(songsIdentificators.toStdString());

    Empty reply;

    ClientContext context;

    Status status = m_stub->update_playlist(&context,
                                            request,
                                            &reply);
    ok = statusOK(status, "update_playlist");
}

void GrpcClient::delete_song(ID    songId,
                             bool& ok) {
    SongID request;
    request.set_id(songId);

    Empty reply;

    ClientContext context;

    Status status = m_stub->delete_song(&context,
                                        request,
                                        &reply);
    ok = statusOK(status, "delete_song");
}

void GrpcClient::delete_playlist(ID    playlistId,
                                 bool& ok) {
    PlaylistID request;
    request.set_playlist_id(playlistId);

    Empty reply;

    ClientContext context;

    Status status = m_stub->delete_playlist(&context,
                                            request,
                                            &reply);
    ok = statusOK(status, "delete_playlist");
}


AudiofileInfo GrpcClient::get_audiofile_info(ID id, bool& ok) {
    SongID request;
    request.set_id(id);

    Audiofile_info reply;

    ClientContext context;

    Status status = m_stub->get_audiofile_info(&context,
                                                request,
                                               &reply);
    AudiofileInfo ai;
    ok = statusOK(status, "get_audiofile_info");
    if(ok) {
        ai.bytesPerMillisec = reply.bytespermillisec();
        ai.audioDurationInMillisec = reply.audiodurationinmillisec();
        ai.lastLoadDate = QString::fromStdString(reply.lastloaddate());
    }
    return ai;
}

std::string GrpcClient::get_audiofile_header(ID id, bool& ok) {
    SongID request;
    request.set_id(id);

    Bytes reply;

    ClientContext context;

    Status status = m_stub->get_audiofile_header(&context,
                                                 request,
                                                 &reply);
    std::string songHeader;
    ok = statusOK(status, "get_audiofile_header");
    if(ok) {
        songHeader = reply.str();
    }
    return songHeader;
}

void GrpcClient::get_right_audio_section(ID id,
                                         quint64& startByte,
                                         quint64& sectionSize,
                                         bool& ok) {
    Audio_section request;
    request.set_song_id(id);
    request.set_startbyte(startByte);
    request.set_sectionsize(sectionSize);

    Audio_section reply;

    ClientContext context;

    Status status = m_stub->get_right_audio_section(&context,
                                                    request,
                                                    &reply);

    ok = statusOK(status, "get_right_audio_section");
    if(ok) {
        startByte = reply.startbyte();
        sectionSize = reply.sectionsize();
    }
}

std::string GrpcClient::get_audio_section(ID id,
                                          quint64 startByte,
                                          quint64 sectionSize,
                                          bool& ok) {
    Audio_section request;
    request.set_song_id(id);
    request.set_startbyte(startByte);
    request.set_sectionsize(sectionSize);

    Bytes replyBytes;

    ClientContext context;

    std::unique_ptr<ClientReader<Bytes> > reader(
            m_stub->get_audio_section(&context, request));
    std::string songBytes;
    while (reader->Read(&replyBytes)) {
            songBytes += replyBytes.str(); //append part of bytes
    }
    ok = statusOK(reader->Finish(), "get_audio_section");
    return songBytes;
}

std::string GrpcClient::get_cover(ID albumId, bool& ok) {
    AlbumID request;
    request.set_id(albumId);

    Bytes replyBytes;

    ClientContext context;

    std::unique_ptr<ClientReader<Bytes> > reader(
            m_stub->get_cover(&context, request));
    std::string coverBytes;
    while (reader->Read(&replyBytes)) {
            coverBytes += replyBytes.str(); //append part of bytes
    }
    ok = statusOK(reader->Finish(), "get_cover");

    return coverBytes;
}

//rpc get_metadata(Date) returns (stream Metadata){}
QList<GrpcClient::sharedMetadata>
GrpcClient::get_new_metadata(const QString& dateLastSync, bool& ok) {
    Date request;
    request.set_date(dateLastSync.toStdString());

    ms::Metadata replyMetadata;

    ClientContext context;

    std::unique_ptr<ClientReader<ms::Metadata> > reader(
            m_stub->get_new_metadata(&context, request));

    QList<sharedMetadata> listMetadata;
    while (reader->Read(&replyMetadata)) {
        MetadataFactory::sharedMetatype type =
                Database::Instance().getMetadataFactory()->getMetadataType(
                    replyMetadata.year(),
                    replyMetadata.album_id(),
                    QString::fromStdString(replyMetadata.album_title()),
                    replyMetadata.artist_id(),
                    QString::fromStdString(replyMetadata.artist_name()),
                    QString::fromStdString(replyMetadata.coverfile_name()));

        sharedMetadata tempMetadata(
                    sharedMetadata::create(
                        replyMetadata.song_id(),
                        QString::fromStdString(replyMetadata.song_name()),
                        QString::fromStdString(replyMetadata.audiofile_name()),
                        type));
        listMetadata.push_back(tempMetadata);
    }
    ok = statusOK(reader->Finish(), "get_new_metadata");
    return listMetadata;
}

QList<GrpcClient::sharedPlaylistData>
GrpcClient::get_new_playlists(const QString& dateLastSync, bool& ok) {
    Date request;
    request.set_date(dateLastSync.toStdString());

    ms::Playlist replyPlaylist;

    ClientContext context;

    std::unique_ptr<ClientReader<ms::Playlist> > reader(
            m_stub->get_new_playlists(&context, request));

    QList<sharedPlaylistData> listPlaylistData;
    while (reader->Read(&replyPlaylist)) {
        sharedPlaylistData tempPlaylistData(
                    sharedPlaylistData::create(
                        replyPlaylist.playlist_id(),
                        QString::fromStdString(replyPlaylist.title()),
                        splitStrId(replyPlaylist.id_str())
                        ));
        listPlaylistData.push_back(tempPlaylistData);
    }
    ok = statusOK(reader->Finish(), "get_new_playlists");

    return listPlaylistData;
}

QVector<ID> GrpcClient::get_deleted_songs(const QString& dateLastSync, bool& ok) {
    Date request;
    request.set_date(dateLastSync.toStdString());

    Str_of_id reply;

    ClientContext context;

    Status status = m_stub->get_deleted_songs(&context,
                                            request,
                                            &reply);
    QVector<ID> vectorOfIdentificators;
    ok = statusOK(status, "get_deleted_songs");
    if(ok) {
      vectorOfIdentificators = splitStrId(reply.id_str());
    }
    return vectorOfIdentificators;
}

QVector<ID> GrpcClient::get_deleted_playlists(const QString& dateLastSync, bool& ok) {
    Date request;
    request.set_date(dateLastSync.toStdString());

    Str_of_id reply;

    ClientContext context;

    Status status = m_stub->get_deleted_playlists(&context,
                                                request,
                                                &reply);
    QVector<ID> vectorOfIdentificators;
    ok = statusOK(status, "get_deleted_playlists");
    if(ok) {
      vectorOfIdentificators = splitStrId(reply.id_str());
    }
    return vectorOfIdentificators;
}

QVector<ID> GrpcClient::splitStrId(const std::string& str) {
    QString strOfIdentificators = QString::fromStdString(str); //from std::string to QString

    QStringList listOfIdentificators =
            strOfIdentificators.split(";", QString::SkipEmptyParts); //split
    size_t listSize = listOfIdentificators.size();
    QVector<ID> vectorOfIdentificators(listSize); //result QVector

    size_t i = 0;
    for(const QString& str: listOfIdentificators) {
        vectorOfIdentificators[i] = str.toInt();
        ++i;
    }
    return vectorOfIdentificators;
}


bool GrpcClient::statusOK(const grpc::Status& status, const QString& funcName) {
    if (!(status.ok())) {
        qWarning() << "GrpcClient::" << funcName << "\t "
                 << "error_code = " << QString::number(status.error_code())
                 << ", error_message = " << QString::fromStdString(status.error_message());
        return false;
    }
    return true;
}
