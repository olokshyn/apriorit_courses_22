#ifndef SYNCWORKER_H
#define SYNCWORKER_H

#include <QObject>

class SyncWorker : public QObject
{
    Q_OBJECT

public:
    SyncWorker(QObject * parent = nullptr);
    SyncWorker(const SyncWorker&) = delete;
    SyncWorker& operator=(const SyncWorker&) = delete;
    ~SyncWorker() = default;

public slots:
    void sync();

signals:
    void successfulSync();
    void syncStatusChanged(const QString& status);
    void resetSyncStatus();
};

#endif // SYNCWORKER_H
