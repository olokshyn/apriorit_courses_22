#ifndef GRPCCLIENT_H
#define GRPCCLIENT_H

#include <QObject>
#include <memory>
#include <QSharedPointer>
#include <QList>
#include <QTime>
#include <string>
#include "../../../protobuf/ms.grpc.pb.h"
#include "../../../protobuf/ms.pb.h"
#include <grpc++/grpc++.h>
#include "../commondefines.h"

class QString;
class Metadata;
class PlaylistData;
class QByteArray;

struct AudiofileInfo
{
    quint16 bytesPerMillisec;
    quint32 audioDurationInMillisec;
    QString lastLoadDate;
    AudiofileInfo()
        : bytesPerMillisec(0),
          audioDurationInMillisec(0),
          lastLoadDate()
    {}
};

class GrpcClient : public QObject
{
    Q_OBJECT

signals:
    void loadStatusChanged(const QString& status);
    void resetLoadStatus();

public:
    using sharedMetadata = QSharedPointer<Metadata>;
    using sharedPlaylistData = QSharedPointer<PlaylistData>;

    GrpcClient(std::shared_ptr<grpc::Channel> channel, QObject * parent = nullptr);
    GrpcClient(const GrpcClient&) = delete;
    GrpcClient& operator=(const GrpcClient&) = delete;
    ~GrpcClient() = default;

public:
    bool connected();

    bool was_the_song_reloaded(ID             songId,
                               const QString& dateLastSync,
                               bool& ok);
    bool was_the_cover_reloaded(ID             albumId,
                                const QString& dateLastSync,
                                bool& ok);

    ID send_albumId_and_title_of_new_song(quint32 albumId,
                                          const QString& songTitle,
                                          bool ok);
    ID send_metadata_of_new_song(const QString& songTitle,
                                 const QString& albumTitle,
                                 const QString& artistTitle,
                                 quint16        year,
                                 bool&          ok);
    void send_new_playlist(const QString& playlistTitle,
                           const QString& songsIdentificators,
                           bool&          ok);
    void send_audio(ID                 songId,
                    const std::string& data,
                    quint16            bytesPortion,
                    bool&              ok);
    void send_cover(ID                 songId,
                    const std::string& data,
                    const QString& fileExtention,
                    quint16            bytesPortion,
                    bool&              ok);
    void update_song_name(ID songId,
                          const QString& newSongTitle,
                          bool& ok);
    void update_album_of_song(ID             songId,
                              const QString& newAlbumTitle,
                              ID             artistId,
                              quint16        year,
                              bool&          ok);
    void update_artist(ID artistId,
                       const QString& newArtistTitle,
                       bool& ok);
    void update_year(ID album_id,
                     quint16 newYear,
                     bool& ok);
    void update_playlist(ID             id,
                         const QString& songsIdentificators,
                         const QString& playlistTitle,
                         bool& ok);
    void delete_song(ID songId,
                     bool& ok);
    void delete_playlist(ID playlistId,
                         bool& ok);


    AudiofileInfo get_audiofile_info(ID id, bool& ok);
    std::string get_audiofile_header(ID id, bool& ok);

    void get_right_audio_section(ID id,
                                 quint64& startByte,
                                 quint64& sectionSize,
                                 bool& ok);
    std::string get_audio_section(ID id,
                                  quint64 startByte,
                                  quint64 sectionSize,
                                  bool& ok);
    std::string get_cover(ID albumId,
                          bool& ok);
    QList<sharedMetadata> get_new_metadata(const QString& dateLastSync,
                                           bool& ok);
    QList<sharedPlaylistData> get_new_playlists(const QString& dateLastSync,
                                                bool& ok);
    QVector<ID> get_deleted_songs(const QString& dateLastSync,
                                  bool& ok);
    QVector<ID> get_deleted_playlists(const QString& dateLastSync,
                                      bool& ok);

private:
    QVector<ID> splitStrId(const std::string& str);
    bool statusOK(const grpc::Status& status,
                  const QString& funcName);

private:
    std::unique_ptr<ms::MS::Stub> m_stub;
    QTime                         m_timeOfLastConnAttempt;
    bool                          m_lastConnStatus;
};

#endif // GRPCCLIENT_H
