#ifndef GRPCMANAGER_H
#define GRPCMANAGER_H

#include <QObject>
#include <QVector>
#include <QList>
#include <QSharedPointer>
#include <string>
#include "../commondefines.h"
#include "grpcclient.h"

class QString;
class QByteArray;
class Metadata;
class PlaylistData;
class QThread;

class GrpcManager : public QObject
{
    Q_OBJECT
public:
    static GrpcManager& Instance();

public:
    AudiofileInfo get_audiofile_info(ID id, bool& ok);
    std::string get_audiofile_header(ID id, bool& ok);
    void get_right_audio_section(ID id,
                                 quint64& startByte,
                                 quint64& sectionSize,
                                 bool& ok);
    std::string get_audio_section(ID id,
                                  quint64 startByte,
                                  quint64 sectionSize,
                                  bool& ok);    
    bool was_the_song_reloaded(ID             songId,
                               const QString& dateLastSync,
                               bool& ok);
    bool was_the_cover_reloaded(ID             albumId,
                                const QString& dateLastSync,
                                bool& ok);
    bool connected();

public slots:
    void reconnect(const QString& serverIpAndPort);

signals:
    void loadStatusChanged(const QString& status);
    void resetAppStatus();

    void sync();
    void successfulSync();
    void signalSendSong(const QString& audiofilePath,
                        const QString& coverfilePath,
                        const QString& songTitle,
                        const QString& albumTitle,
                        const QString& artistTitle,
                        quint16 year);
    void signalSendSongByAlbum(const QString& audiofilePath,
                               const QString& songTitle,
                               ID        albumId);
    void signalSendPlaylist(const QString& playlistTitle,
                                 const QString& songsIdStr);
    void signalUpdateSong(ID        songId,
                          quint16        year,
                          const QString& songTitle,
                          const QString& albumTitle,
                          const QString& artistTitle,
                          const QString& audiofilePath,
                          const QString& coverfilePath);
    bool signalUpdatePlaylist(
            ID        playlistId,
            const QString& newTitle,
            const QString& songForAdd,
            const QString& songsForDelete);

    bool signalDeleteSong(ID songId);
    bool signalDeletePlaylist(ID playlistId);

private:
    GrpcManager(QObject * parent = nullptr);
    GrpcManager(const GrpcManager&) = delete;
    GrpcManager& operator=(const GrpcManager&) = delete;
    ~GrpcManager();

private:
    friend class SyncWorker;
    friend class SendWorker;
    ID sendMetadata(const QString& songTitle,
                    const QString& albumTitle,
                    const QString& artistTitle,
                    quint16        year);
    ID sendMetadataByAlbum(const QString& songTitle,
                           ID             albumId);
    bool sendPlaylist(const QString& playlistTitle,
                      const QString& songsIdStr);
    bool sendAudioFile(ID                 songId,
                       const std::string& data,
                       quint16            portion);
    bool sendCoverFile(ID                 songId,
                       const std::string& data,
                       const QString&     fileExtension,
                       quint16            portion);

    bool updateSongTitle(ID             songId,
                         const QString& newSongTitle);
    bool updateAlbumTitleOfOneSong(ID             songId,
                                   const QString& newAlbumTitle,
                                   ID             artistId,
                                   quint16        year);
    bool updateArtist(ID artistId,
                      const QString&       newArtistTitle);

    bool updateYear(ID album_id,
                     quint16 newYear);
    bool updatePlaylist(
            ID             playlistId,
            const QString& songsIdStr,
            const QString& newTitle);

    bool deleteSong(ID songId);
    bool deletePlaylist(ID playlistId);


    bool saveCoverBytes(ID albumId);
    bool getNewAudiosMetadataFromServer(const QString& dateLastSync);
    bool getNewPlaylistsDataFromServer(const QString& dateLastSync);
    bool getDeletedAudiosFromServer(const QString& dateLastSync);
    bool getDeletedPlaylistsFromServer(const QString& dateLastSync);
    bool getReloadedCoverfiles(const QString& dateLastSync);


private:
    QString getDateLastSync();
    void setCurDateLastSync();

    void saveBytesToFile(const std::string& bytes,
                         const QString &fileName);

private:
    GrpcClient * m_grpcClient;
    QThread *    m_syncThread;
    QThread *    m_sendThread;
};

#endif // GRPCMANAGER_H
