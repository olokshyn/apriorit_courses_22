#include "grpcmanager.h"
#include "grpcclient.h"
#include "../metadata/metadata.h"
#include "../playlistdata.h"
#include "../settings.h"
#include "../Database/database.h"
#include "../metadata/metadatafactory.h"
#include "../metadata/metadatatype.h"
#include "../ListModel/modelconnection.h"
#include "syncworker.h"
#include "sendworker.h"
#include <QString>
#include <QDate>
#include <QTime>
#include <QDebug>
#include <fstream>
#include <QThread>
#include <QFile>

GrpcManager::GrpcManager(QObject * parent)
    : QObject(parent),
      m_grpcClient(nullptr),
      m_syncThread(nullptr)
{
    reconnect(Settings::Instance().getServerIpAndPort());

    //connect slot sync, signal successfulResult to another thread
    SyncWorker * syncWorker = new SyncWorker();
    m_syncThread = new QThread();
    syncWorker->moveToThread(m_syncThread);

    connect(m_syncThread, SIGNAL(finished()),
            syncWorker,   SLOT(deleteLater()));
    connect(this,         SIGNAL(sync()),
            syncWorker,   SLOT(sync()));
    connect(syncWorker,   SIGNAL(successfulSync()),
            this,         SIGNAL(successfulSync()));
    connect(syncWorker,   SIGNAL(syncStatusChanged(QString)),
            this,         SIGNAL(loadStatusChanged(QString)));
    connect(syncWorker,   SIGNAL(resetSyncStatus()),
            this,         SIGNAL(resetAppStatus()));
    m_syncThread->start();

    //connect slots send to another thread
    SendWorker * sendWorker = new SendWorker();
    m_sendThread = new QThread();
    sendWorker->moveToThread(m_sendThread);

    connect(m_sendThread, SIGNAL(finished()),
            sendWorker,   SLOT(deleteLater()));
    connect(this,         SIGNAL(signalSendSong(QString,QString,QString,QString,QString,quint16)),
            sendWorker,   SLOT(sendSong(QString,QString,QString,QString,QString,quint16)));
    connect(this,         SIGNAL(signalSendSongByAlbum(QString,QString,ID)),
            sendWorker,   SLOT(sendSongByAlbum(QString,QString,ID)));
    connect(this,         SIGNAL(signalSendPlaylist(QString,QString)),
            sendWorker,   SLOT(sendPlaylist(QString,QString)));
    connect(this,         SIGNAL(signalUpdateSong(ID,quint16,QString,QString,QString,QString,QString)),
            sendWorker,   SLOT(updateSong(ID,quint16,QString,QString,QString,QString,QString)));
    connect(this,         SIGNAL(signalUpdatePlaylist(ID,QString,QString,QString)),
            sendWorker,   SLOT(updatePlaylist(ID,QString,QString,QString)));
    connect(this,         SIGNAL(signalDeleteSong(ID)),
            sendWorker,   SLOT(deleteSong(ID)));
    connect(this,         SIGNAL(signalDeletePlaylist(ID)),
            sendWorker,   SLOT(deletePlaylist(ID)));
    connect(sendWorker,   SIGNAL(sendStatusChanged(QString)),
            this,         SIGNAL(loadStatusChanged(QString)));
    connect(sendWorker,   SIGNAL(resetSendStatus()),
            this,         SIGNAL(resetAppStatus()));
    m_sendThread->start();

    connect(m_grpcClient, SIGNAL(loadStatusChanged(QString)),
            this,         SIGNAL(loadStatusChanged(QString)));
    connect(m_grpcClient, SIGNAL(resetLoadStatus()),
            this,         SIGNAL(resetAppStatus()));
    connect(&(Settings::Instance()), SIGNAL(serverIpAndPortchanged(QString)),
            this, SLOT(reconnect(QString)));
}


bool GrpcManager::connected() {
    return m_grpcClient->connected();
}

void GrpcManager::reconnect(const QString& serverIpAndPort) {
    delete m_grpcClient;
    m_grpcClient = nullptr;
    //set grpcClient
    m_grpcClient = new GrpcClient(
                grpc::CreateChannel(
                        serverIpAndPort.toStdString().c_str(),
                        grpc::InsecureChannelCredentials()));
}

GrpcManager::~GrpcManager() {
    m_syncThread->quit();
    m_sendThread->quit();

    m_syncThread->wait();
    m_sendThread->wait();

    delete m_syncThread;
    delete m_grpcClient;
}

GrpcManager& GrpcManager::Instance() {
    static GrpcManager grpcConnection;
    return grpcConnection;
}

ID GrpcManager::sendMetadata(
        const QString& songTitle,
        const QString& albumTitle,
        const QString& artistTitle,
        quint16        year) {
    bool ok = STATUS_BAD;
    ID songId = m_grpcClient->send_metadata_of_new_song(songTitle,
                                                        albumTitle,
                                                        artistTitle,
                                                        year,
                                                        ok);
    return songId;
}

ID GrpcManager::sendMetadataByAlbum(const QString& songTitle,
                                    ID             albumId) {
    bool ok = STATUS_BAD;
    ID songId = m_grpcClient->send_albumId_and_title_of_new_song(albumId,
                                                                 songTitle,
                                                                 ok);
    return songId;
}

bool GrpcManager::sendPlaylist(
        const QString& playlistTitle,
        const QString& songsIdStr) {
    bool ok = STATUS_BAD;

    m_grpcClient->send_new_playlist(playlistTitle,
                                    songsIdStr,
                                    ok);
    return ok;
}

bool GrpcManager::sendAudioFile(ID songId,
                                const std::string& data,
                                quint16 portion) {
    bool ok = STATUS_BAD;
    m_grpcClient->send_audio(songId,
                             data,
                             portion,
                             ok);
    return ok;
}

bool GrpcManager::sendCoverFile(ID songId,
                                const std::string& data,
                                const QString& fileExtension,
                                quint16 portion) {
    bool ok = STATUS_BAD;
    m_grpcClient->send_cover(songId,
                             data,
                             fileExtension,
                             portion,
                             ok);
    return ok;
}

bool GrpcManager::updateSongTitle(
        ID songId,
        const QString& newSongTitle) {
    bool ok = STATUS_BAD;
    m_grpcClient->update_song_name(songId,
                                   newSongTitle,
                                   ok);
    return ok;
}

bool GrpcManager::updateAlbumTitleOfOneSong(
        ID             songId,
        const QString& newAlbumTitle,
        ID             artistId,
        quint16        year) {
    bool ok = STATUS_BAD;
    m_grpcClient->update_album_of_song(songId,
                                       newAlbumTitle,
                                       artistId,
                                       year,
                                       ok);
    return ok;
}

bool GrpcManager::updateArtist(
        ID                 artistId,
        const QString&     newArtistTitle) {
    bool ok = STATUS_BAD;
    m_grpcClient->update_artist(artistId,
                                newArtistTitle,
                                ok);
    return ok;
}

bool GrpcManager::updateYear(ID album_id,
                              quint16 newYear) {
    bool ok = STATUS_BAD;
    m_grpcClient->update_year(album_id,
                              newYear,
                              ok);
    return ok;
}

bool GrpcManager::updatePlaylist(
        ID             playlistId,
        const QString& songsIdStr,
        const QString& newTitle) {
    bool ok = STATUS_BAD;

    m_grpcClient->update_playlist(playlistId,
                                  songsIdStr,
                                  newTitle,
                                  ok);
    return ok;
}

bool GrpcManager::deleteSong(ID songId) {
    bool ok = STATUS_BAD;
    m_grpcClient->delete_song(songId,
                              ok);
    return ok;
}

bool GrpcManager::deletePlaylist(ID playlistId) {
    bool ok = STATUS_BAD;
    m_grpcClient->delete_playlist(playlistId,
                                  ok);
    return ok;
}

AudiofileInfo GrpcManager::get_audiofile_info(ID id, bool& ok) {
    if(!connected()) {
        return AudiofileInfo();
    }
    return m_grpcClient->get_audiofile_info(id, ok);
}

std::string GrpcManager::get_audiofile_header(ID id, bool& ok) {
    if(!connected()) {
        return "";
    }
    return m_grpcClient->get_audiofile_header(id, ok);
}

bool GrpcManager::was_the_song_reloaded(
        ID             songId,
        const QString& dateLastSync,
        bool& ok) {
    if(!connected()) {
        return false;
    }
    return m_grpcClient->was_the_song_reloaded(songId,
                                               dateLastSync,
                                               ok);
}

bool GrpcManager::was_the_cover_reloaded(
        ID             albumId,
        const QString& dateLastSync,
        bool& ok) {
    if(!connected()) {
        return false;
    }
    return m_grpcClient->was_the_cover_reloaded(albumId,
                                               dateLastSync,
                                               ok);
}

void GrpcManager::get_right_audio_section(
        ID id,
        quint64& startByte,
        quint64& sectionSize,
        bool& ok) {
    if(!connected()) {
        return;
    }
    m_grpcClient->get_right_audio_section(id,
                                          startByte,
                                          sectionSize,
                                          ok);
}

std::string GrpcManager::get_audio_section(
        ID id,
        quint64 startByte,
        quint64 sectionSize,
        bool& ok) {
    if(!connected()) {
        return "";
    }
    return m_grpcClient->get_audio_section(id,
                                           startByte,
                                           sectionSize,
                                           ok);
}

QString GrpcManager::getDateLastSync() {
    return Settings::Instance().getDateLastSync();
}

void GrpcManager::setCurDateLastSync() {
    Settings::Instance().setDateLastSync(
                QDate::currentDate(),QTime::currentTime());
}

bool GrpcManager::saveCoverBytes(ID albumId) {
    bool ok = STATUS_BAD;
    std::string bytes = m_grpcClient->get_cover(albumId, ok);
    if(!ok) {
        return STATUS_BAD;
    }

    QSharedPointer<MetadataFactory> factory = Database::Instance().getMetadataFactory();
    MetadataFactory::iter it = factory->getMetaType(albumId);
    if(it != factory->end()) {
        saveBytesToFile(bytes,
            Settings::Instance().getPathToCoverfiles()
            + it->data()->getCoverfileName());
    }
    return STATUS_OK;
}

bool GrpcManager::getNewAudiosMetadataFromServer(const QString& dateLastSync) {
    bool ok = STATUS_BAD;
    QList<QSharedPointer<Metadata>> listOfMetadata =
            m_grpcClient->get_new_metadata(dateLastSync, ok);
    if(!ok) {
        return STATUS_BAD;
    }

    for(QSharedPointer<Metadata> metadata: listOfMetadata) {
        emit Database::Instance().updateSongInDB(metadata); //update in db
        if(metadata->coverfileExists() == NOT_EXISTS) {//if coverfile isn't downladed
            saveCoverBytes(metadata->getAlbumId()); //get a cover from db for every song
        }
    }
    return STATUS_OK;
}

bool GrpcManager::getNewPlaylistsDataFromServer(const QString& dateLastSync) {
    bool ok = STATUS_BAD;
    QList<QSharedPointer<PlaylistData>> listOfPlaylistData =
            m_grpcClient->get_new_playlists(dateLastSync, ok);
    if(!ok) {
        return STATUS_BAD;
    }

    for(auto playlistData: listOfPlaylistData) {
        emit Database::Instance().updatePlaylistInDB(playlistData);
    }
    return STATUS_OK;
}

bool GrpcManager::getDeletedAudiosFromServer(const QString& dateLastSync) {
    bool ok = STATUS_BAD;
    QVector<ID> identificatorsForDelete =
            m_grpcClient->get_deleted_songs(dateLastSync, ok);
    if(!ok) {
        return STATUS_BAD;
    }

    for(const ID& id: identificatorsForDelete) {
        emit Database::Instance().deleteSongFromDB(id);
    }
    return STATUS_OK;
}

bool GrpcManager::getDeletedPlaylistsFromServer(const QString& dateLastSync) {
    bool ok = STATUS_BAD;
    QVector<ID> identificatorsForDelete =
            m_grpcClient->get_deleted_playlists(dateLastSync, ok);
    if(!ok) {
        return STATUS_BAD;
    }

    for(const ID& id: identificatorsForDelete) {
        if(id != 0) { //for not to delete 'playlist' with all songs
            emit Database::Instance().deletePlaylistFromDB(id);
        }
    }
    return STATUS_OK;
}

bool GrpcManager::getReloadedCoverfiles(const QString& dateLastSync) {
    const MetadataFactory::listTypes types =
            Database::Instance().getMetadataFactory()->getAllTypes();
    for(const MetadataFactory::sharedMetatype& type: types) {
        bool ok = STATUS_BAD;
        ID albumId = type->getAlbumId();
        bool wasReloaded = was_the_cover_reloaded(albumId,
                            dateLastSync,
                            ok);
        if(ok == STATUS_BAD) {
            qWarning() << "GrpcManager::getReloadedCoverfiles"
            << "Fail to get reloaded status for album with id = "
            << albumId;
            return STATUS_BAD;
        }

        if(wasReloaded) {
            QFile coverFile(Settings::Instance().getPathToCoverfiles()
                            + type->getCoverfileName());
            coverFile.remove();
            saveCoverBytes(albumId);
        }
    }
    return STATUS_OK;
}

void GrpcManager::saveBytesToFile(const std::string& bytes,
                                const QString &fileName) {
    std::ofstream fout(fileName.toStdString().c_str(),
                       std::ios::out | std::ios::binary);
    fout << bytes;
    fout.close();
}
