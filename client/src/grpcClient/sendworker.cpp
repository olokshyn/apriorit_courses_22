#include "sendworker.h"
#include "grpcmanager.h"
#include "../settings.h"
#include "../Database/database.h"
#include "../metadata/metadata.h"
#include <QByteArray>
#include <QFile>
#include <QDebug>
#include <QSharedPointer>
#include <QFileInfo>

SendWorker::SendWorker(QObject *parent)
    : QObject(parent)
{}

void SendWorker::sendSong(
        const QString& audiofilePath,
        const QString& coverfilePath,
        const QString& songTitle,
        const QString& albumTitle,
        const QString& artistTitle,
        quint16 year) {
    GrpcManager& grpcManager = GrpcManager::Instance();
    if(!grpcManager.connected()) {
        return;
    }

    emit sendStatusChanged("Send song started");
    ID songId = grpcManager.sendMetadata(songTitle,
                                         albumTitle,
                                         artistTitle,
                                         year);
    if(songId == 0) {
        qWarning() << "SendWorker::sendSong\t"
                      "Fail while send new metadata";
        return;
    }

    quint16 bytesPortion = Settings::Instance().getGrpcBytesPortion();
    const std::string audioData = getDataFromFile(audiofilePath);
    grpcManager.sendAudioFile(songId,
                              audioData,
                              bytesPortion);
    const std::string coverData = getDataFromFile(coverfilePath);
    grpcManager.sendCoverFile(songId,
                              coverData,
                              QFileInfo(coverfilePath).suffix(),
                              bytesPortion);
    grpcManager.sync();
    emit resetSendStatus();
}


void SendWorker::sendSongByAlbum(const QString& audiofilePath,
                                 const QString& songTitle,
                                 quint32        albumId) {
    GrpcManager& grpcManager = GrpcManager::Instance();
    if(!grpcManager.connected()) {
        return;
    }

    emit sendStatusChanged("Send song by album started");
    ID songId = grpcManager.sendMetadataByAlbum(songTitle,
                                                albumId);
    if(songId == 0) {
        qWarning() << "SendWorker::sendSongByAlbum\t"
                      "Fail while send new metadata by id";
        return;
    }

    quint16 bytesPortion = Settings::Instance().getGrpcBytesPortion();
    const std::string audioData = getDataFromFile(audiofilePath);
    grpcManager.sendAudioFile(songId,
                              audioData,
                              bytesPortion);
    grpcManager.sync();
}

void SendWorker::sendPlaylist(
        const QString& playlistTitle,
        const QString& songsIdStr) {
    GrpcManager& grpcManager = GrpcManager::Instance();
    if(!grpcManager.connected()) {
        return;
    }
    emit sendStatusChanged("Send playlist started");
    grpcManager.sendPlaylist(playlistTitle,
                             songsIdStr);
    grpcManager.sync();
    emit resetSendStatus();
}

void SendWorker::updateSong(
        quint32        songId,
        quint16        year,
        const QString& songTitle,
        const QString& albumTitle,
        const QString& artistTitle,
        const QString& audiofilePath,
        const QString& coverfilePath) {
    GrpcManager& grpcManager = GrpcManager::Instance();
    if(!grpcManager.connected()) {
        return;
    }

    emit sendStatusChanged("Update song started");
    //check which of data wasn't change
    QSharedPointer<Metadata> metadata =
            Database::Instance().getSongMetadata(songId);
    quint16 oldYear = metadata->getYear();
    bool yearUsed = false;
    if(metadata->getSongTitle() != songTitle) {
        grpcManager.updateSongTitle(songId,
                                    songTitle);
    }
    if(metadata->getAlbumTitle() != albumTitle) {
        if(oldYear != year) { //if unput new album and new year, then update year for new album
            grpcManager.updateAlbumTitleOfOneSong(songId,
                                                  albumTitle,
                                                  metadata->getArtistId(),
                                                  year);
        } else { //else, set for new album year of old album
            grpcManager.updateAlbumTitleOfOneSong(songId,
                                                  albumTitle,
                                                  metadata->getArtistId(),
                                                  oldYear);
        }
        yearUsed = true;
    }
    if(metadata->getArtistTitle() != artistTitle) {
        grpcManager.updateArtist(metadata->getArtistId(),
                                 artistTitle);
    }
    if(!yearUsed && oldYear != year) { //change just year
        grpcManager.updateYear(metadata->getAlbumId(),
                               year);
    }
    if(audiofilePath != "") {
        quint16 bytesPortion = Settings::Instance().getGrpcBytesPortion();
        const std::string audioData = getDataFromFile(audiofilePath);
        grpcManager.sendAudioFile(songId,
                                  audioData,
                                  bytesPortion);
    }
    if(coverfilePath != "") {
        quint16 bytesPortion = Settings::Instance().getGrpcBytesPortion();
        const std::string coverData = getDataFromFile(coverfilePath);
        grpcManager.sendCoverFile(songId,
                                  coverData,
                                  QFileInfo(coverfilePath).suffix(),
                                  bytesPortion);
    }
    grpcManager.sync();
    emit resetSendStatus();
}

void SendWorker::updatePlaylist(
        ID             playlistId,
        const QString& newTitle,
        const QString& songForAdd,
        const QString& songsForDelete) {
    GrpcManager& grpcManager = GrpcManager::Instance();
    if(!grpcManager.connected()) {
        return;
    }

    emit sendStatusChanged("Update playlist started");
    grpcManager.updatePlaylist(playlistId,
                               "+" + songForAdd,
                               newTitle);
    grpcManager.updatePlaylist(playlistId,
                               "-" + songsForDelete,
                               newTitle);
    grpcManager.sync();
    emit resetSendStatus();
}

void SendWorker::deleteSong(ID songId) {
    GrpcManager& grpcManager = GrpcManager::Instance();
    if(!grpcManager.connected()) {
        return;
    }

    emit sendStatusChanged("Delete song started");
    grpcManager.deleteSong(songId);
    grpcManager.sync();
    emit resetSendStatus();
}

void SendWorker::deletePlaylist(ID playlistId) {
    GrpcManager& grpcManager = GrpcManager::Instance();
    if(!grpcManager.connected()) {
        return;
    }

    emit sendStatusChanged("Delete playlist started");
    grpcManager.deletePlaylist(playlistId);
    grpcManager.sync();
    emit resetSendStatus();
}

std::string SendWorker::getDataFromFile(
        const QString& fileName) {
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
        qWarning() << "SendWorker::getDataFromFile\t fail to open file";
    }
    QByteArray byteArray = file.readAll();
    file.close();
    return byteArray.toStdString();
}
