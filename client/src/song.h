#ifndef SONG_H
#define SONG_H

#include <QSharedPointer>
#include <string>
#include "commondefines.h"

class Metadata;

class Song
{
public:
    using sharedMetadata = QSharedPointer<Metadata>;

    Song(ID id);
    Song(const Song&) = delete;
    Song& operator=(const Song&) = delete;
    ~Song();

public:
    void play();
    void pause();
    ID getId();

ACCESS:
    void getAudiofile();
    void loadHeader();

ACCESS:
    ID              m_id;
    sharedMetadata  m_metadata;
    QString         m_fileName;
    bool            m_headerLoaded;
};

#endif // SONG_H
