#ifndef VLCPLAYER_H
#define VLCPLAYER_H

class VlcInstance;
class VlcMedia;
class VlcMediaPlayer;
class VlcWidgetVolumeSlider;
class QString;

class VlcPlayer
{
public:
    static VlcPlayer& Instance();

    void openOnly();
    void open();
    void stop();
    void setMedia(const QString& fileName);
    void setPosition(float position);

    void setVolume(int volume);
    int  getVolume();
    bool isPlaying();

private:
    VlcPlayer();
    VlcPlayer(const VlcPlayer&) = delete;
    VlcPlayer& operator=(const VlcPlayer&) = delete;
    ~VlcPlayer();

private:
    VlcInstance *           m_instance;
    VlcMediaPlayer *        m_player;
    VlcWidgetVolumeSlider * m_volumeSlider;
    VlcMedia *              m_curMedia;
};

#endif // VLCPLAYER_H
