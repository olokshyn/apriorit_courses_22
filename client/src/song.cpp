#include "song.h"
#include "metadata/metadata.h"
#include "Database/database.h"
#include "settings.h"
#include "CacheManager/cacheconnection.h"

Song::Song(ID id)
    : m_id(id),
      m_metadata(),
      m_fileName(),
      m_headerLoaded(false)
{
    m_metadata = Database::Instance().getSongMetadata(m_id);
    CacheConnection::Instance().setSong(m_metadata->getAudiofileName(), m_id);
}

Song::~Song() {
    CacheConnection::Instance().unsetSong();
}

void Song::play() {
    if(!m_headerLoaded) {
        loadHeader();
    }
    CacheConnection::Instance().play();
}

void Song::pause() {
    CacheConnection::Instance().pause();
}

ID Song::getId() {
    return m_id;
}

void Song::loadHeader() {
    CacheConnection::Instance().loadHeader();
    m_headerLoaded = true;
}






