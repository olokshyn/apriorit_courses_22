#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QObject>

class QSettings;
class QDateTime;
class QDir;
class QDate;
class QTime;

class Settings : public QObject
{
    Q_OBJECT

signals:
    void audioPathChanged(const QString& audioPath);
    void serverIpAndPortchanged(const QString& serverIpAndPort);
    void audioMillisecPartChanged(quint32 audioMillicesPart);

public:
    static Settings& Instance();

    void setVolume(int newVolume);
    int getVolume();

    void setDateLastSync(const QDate& newDate,
                         const QTime& newTime);
    Q_INVOKABLE QString getDateLastSync();

    Q_INVOKABLE void setPathToAudiofiles(const QString& path);
    Q_INVOKABLE QString getPathToAudiofiles();

    Q_INVOKABLE void setPathToCoverfiles(const QString& path);
    Q_INVOKABLE QString getPathToCoverfiles();

    Q_INVOKABLE void setServerIpAndPort(const QString& serverIpAndPort);
    Q_INVOKABLE QString getServerIpAndPort();

    Q_INVOKABLE void setAudioMillisecPart(quint32 audioMillisecPart);
    Q_INVOKABLE quint32 getAudioMillisecPart();

    Q_INVOKABLE void setGrpcBytesPortion(quint16 portion);
    Q_INVOKABLE quint16 getGrpcBytesPortion();

private:
    Settings(QObject * parent = nullptr);
    ~Settings();
    Settings(const Settings&) = delete;
    Settings& operator=(const Settings&) = delete;

private:
    void createDir(const QString& path);
    void copyFiles(const QString& srcDirPath,
                   const QString& destDirPath);

private:
    QSettings *     m_qsettings;
    const QString   m_formatDateLastSync = "yyyy/MM/dd";
    const QString   m_formatTimeLastSync = "hh:mm:ss";
};

#endif // SETTINGS_H
