#include "timer.h"
#include <QTimer>

Timer::Timer(QObject *parent)
    : QObject(parent),
      m_limit(0),
      m_curTime(0),
      m_qTimer(nullptr),
      m_eventTimer(nullptr)
{
    m_qTimer = new QTimer();
    m_qTimer->setInterval(1); //every millisecond

    m_eventTimer = new QTimer();
    m_eventTimer->setInterval(50);

    connect(m_qTimer,     SIGNAL(timeout()),
            this,         SLOT(setCurTime()));
    connect(m_eventTimer, SIGNAL(timeout()),
            this,         SLOT(slotTimeoutEvent()));
}

Timer::~Timer() {
    delete m_qTimer;
    delete m_eventTimer;
}

quint32 Timer::getCurTime() {
    return m_curTime;
}

void Timer::setLimit(quint32 limit) {
    m_limit = limit;
}

void Timer::slotTimeoutEvent() {
    emit timeoutEvent(m_curTime);
}

void Timer::start() {
    m_qTimer->start();
    m_eventTimer->start();
}

void Timer::stop() {
    m_qTimer->stop();
    m_eventTimer->stop();
}

void Timer::setEventInterval(quint16 interval) {
    m_eventTimer->setInterval(interval);
}

void Timer::setTime(quint32 time) {
    m_curTime = time;
}

void Timer::setCurTime() {
    if(m_curTime < m_limit) {
        ++m_curTime;
    } else if(m_curTime != 0) {
        emit timeLimitReached();
    }
}

