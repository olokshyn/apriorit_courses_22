#include "cacheaudiomanager.h"
#include "../grpcClient/grpcmanager.h"
#include "../settings.h"
#include "timer.h"
#include "../vlcplayer.h"
#include "../settings.h"
#include <QFile>
#include <QDebug>
#include <QDate>
#include <QTime>

enum FileExtension {WAV = 0, FLAC};

CacheAudioManager::CacheElement::CacheElement()
    : beginByte(0), endByte(0)
{}
CacheAudioManager::CacheElement::CacheElement(quint64 beg, quint64 end)
    : beginByte(beg), endByte(end)
{}
quint64 CacheAudioManager::CacheElement::sectionSize() const {
    return endByte - beginByte;
}
bool CacheAudioManager::CacheElement::operator==(
        const CacheAudioManager::CacheElement& other) const {
    return beginByte == other.beginByte &&
            endByte == other.endByte;
}

CacheAudioManager::CacheAudioManager(QObject *parent)
    : QObject(parent),
      m_dateLastSync(),
      m_fileName(),
      m_cacheinfoFileName(".cacheinfo"),
      m_bytesPerMillisec(0),
      m_headerBytesSize(0),
      m_partBytesSize(0),
      m_audioDurationInBytes(0),
      m_songId(0),
      m_list(),
      m_timer(nullptr)
{
    Settings& settings = Settings::Instance();
    connect(&(settings), SIGNAL(audioPathChanged(QString)),
            this,        SLOT(audioPathChanged(QString)));
    connect(&(settings), SIGNAL(audioMillisecPartChanged(quint32)),
            this,        SLOT(audioMillisecPartChanged(quint32)));
}

CacheAudioManager::~CacheAudioManager() {
    delete m_timer;
}

void CacheAudioManager::audioPathChanged(const QString& audioPath) {
    if(!(m_fileName.isEmpty())) {
        VlcPlayer& player = VlcPlayer::Instance();
        bool wasPlaying = player.isPlaying();
        pause();
        player.setMedia(audioPath + m_fileName);
        if(wasPlaying) {
            play();
        }
    }
}

void CacheAudioManager::audioMillisecPartChanged(quint32 audioMillisecPart) {
    m_partBytesSize = convertMillisecToBytes(audioMillisecPart) - m_headerBytesSize;
}

void CacheAudioManager::play() {
    preload(m_timer->getCurTime()); //preload audioparts

    m_timer->start();
    VlcPlayer::Instance().open();
    setPosition(m_timer->getCurTime());
}

void CacheAudioManager::pause() {
    m_timer->stop();
    VlcPlayer::Instance().stop();
}

void CacheAudioManager::setSong(const QString& fileName,
                                ID             songId) {
    m_fileName = fileName;
    m_cacheinfoFileName = fileName + ".cacheinfo";
    m_songId = songId;
    VlcPlayer::Instance().setMedia(Settings::Instance().getPathToAudiofiles() + m_fileName);

    uploadListFromFile();
    setAudiofileInfoFromServer();
    setCachedSectionsOnTimeline();

    m_timer = new Timer();
    m_timer->setLimit(converBytesToMillisec(m_audioDurationInBytes));

    connect(m_timer, SIGNAL(timeoutEvent(quint32)),
            this,    SIGNAL(setPositionOnSlider(quint32)));
    connect(m_timer, SIGNAL(timeLimitReached()),
            this,    SIGNAL(endOfAudio()));
    connect(m_timer, SIGNAL(timeoutEvent(quint32)),
            this,    SLOT(positionChanged(quint32)));
}

void CacheAudioManager::loadHeader() {
    if(m_list.isEmpty()) { //if doesn't have header yet
        setHeaderFromServer();
    }
    m_partBytesSize = convertMillisecToBytes(
                Settings::Instance().getAudioMillisecPart()) - m_headerBytesSize;
}

void CacheAudioManager::unsetSong() {
    pause();
    unloadListToFile();
    m_fileName = "";
    m_cacheinfoFileName = ".cacheinfo";
    m_bytesPerMillisec = 0;
    m_partBytesSize = 0;
    m_audioDurationInBytes = 0;
    m_songId = 0;
    m_headerBytesSize = 0;
    m_dateLastSync = "";
    m_list.clear();
    delete m_timer;
    m_timer = nullptr;
    setPositionOnSlider(0);
    setDurationOnSlider(0);
}

void CacheAudioManager::setCachedSectionsOnTimeline() {
    for(const CacheElement& elem: m_list) {
        emit setCachedSections(converBytesToMillisec(elem.beginByte),
                               converBytesToMillisec(elem.endByte));
    }
}

void CacheAudioManager::preload(quint32 position) {
    for(unsigned short i = 0; i < 5; ++i) {
        if(positionChanged(position) == STATUS_BAD) {
            return;
        }
    }
}

void CacheAudioManager::setPosition(quint32 position) {
    quint32 posForPlayer = getPositionForPlayer(position);
    float pos = static_cast<float>(posForPlayer) / converBytesToMillisec(m_audioDurationInBytes);
    VlcPlayer::Instance().setPosition(pos);
}

void CacheAudioManager::sliderPositionChangedByClick(quint32 position) {
    if(VlcPlayer::Instance().isPlaying()) {
        pause();
        m_timer->setTime(position);
        play();
    } else {
        m_timer->setTime(position);
        emit setPositionOnSlider(position);
    }
}

quint32 CacheAudioManager::getPositionForPlayer(quint32 position) {
    quint64 bytePosition = convertMillisecToBytes(position);
    quint64 resultBytes = 0;

    for(listIter it = m_list.begin();
        it != m_list.end() && it->beginByte < bytePosition; ++it) {
        if(it->beginByte <= bytePosition && bytePosition < it->endByte) {
            resultBytes += (bytePosition - it->beginByte);
            break;
        } else {
            resultBytes += it->sectionSize();
        }
    }
    return converBytesToMillisec(resultBytes);
}

bool CacheAudioManager::positionChanged(quint32 positionMillisec) {
    //check if audiofile was changed

    //если ближайший некэшированный участок находится на расстоянии меньше,
    //чем двойной участок скачиваемой секции, то надо загружать следующий участок
    quint64 bytePosition = convertMillisecToBytes(positionMillisec);
    quint64 nearestByte = nearestByteOfUnavailableSection(bytePosition);
    //nearestByte = bytePosition, if position is on section, which is not exist

    if((nearestByte - bytePosition) <  5 * m_partBytesSize) {

        CacheElement cacheElem;
        cacheElem.beginByte = nearestByte;
        cacheElem.endByte = getEndOfUnavailableSectionByStartByte(cacheElem.beginByte);

        if(getFileExtension() == FileExtension::FLAC) { //if flac, get right position and section size
            quint64 startByte = cacheElem.beginByte,
                    sectionSize = cacheElem.sectionSize();
            if(setRightSectionFromServer(startByte, sectionSize) == STATUS_BAD) {
                emit endOfAudio();
                return STATUS_BAD;
            } else {
                cacheElem.beginByte = startByte;
                cacheElem.endByte = startByte + sectionSize;
            }
        }

        bool ok = STATUS_BAD;
        std::string sectionBytes = getSectionFromServer(cacheElem.beginByte,
                                                        cacheElem.sectionSize(),
                                                        ok);
        if(ok == STATUS_BAD) { //if troubles with server
            if((nearestByte - bytePosition) <= m_partBytesSize) {
                listIter it = getSectionByByte(bytePosition);
                if(it == m_list.end() || (it + 1) == m_list.end()) {  //if section is last or unavailable, and server is offline
                    emit endOfAudio();
                } else { //have one more section
                    sliderPositionChangedByClick(converBytesToMillisec((it + 1)->beginByte)); //set to start of second section
                }
            }
            return STATUS_BAD;
        }
        quint64 byteForPast = byteNumberForPastToFile(cacheElem.beginByte);
        if(pasteBytesToFile(sectionBytes, byteForPast) == STATUS_OK) {
            addSectionToList(cacheElem);
            emit setCachedSections(converBytesToMillisec(cacheElem.beginByte),
                                   converBytesToMillisec(cacheElem.endByte));
        }
    }
    return STATUS_OK;
}

void CacheAudioManager::setHeaderFromServer() {
    bool ok = false;
    std::string headerBytes = GrpcManager::Instance().get_audiofile_header(m_songId, ok);
    if(!ok) {
        qWarning() << "CacheAudioManager::setHeaderFromServer\t Could not load section of audio '"
                   << m_fileName << "' from server";
    } else {
        m_headerBytesSize = headerBytes.size(); //header size
        if(fileExists() == NOT_EXISTS) {
            pasteBytesToFile(headerBytes, 0); //write header of audiofile to file
            CacheElement elem(0, m_headerBytesSize);
            addSectionToList(elem);
        }
    }
}

void CacheAudioManager::setAudiofileInfoFromServer() {
    if(m_bytesPerMillisec == 0 || m_audioDurationInBytes == 0) { //if yet not downloaded
        bool ok = false;
        AudiofileInfo ai = GrpcManager::Instance().get_audiofile_info(m_songId, ok);
        if(!ok) {
            qWarning() << "CacheAudioManager::setAudiofileInfoFromServer\t Could not load info of audio '"
                       << m_fileName << "' from server";
        } else {
            m_bytesPerMillisec = ai.bytesPerMillisec;
            m_audioDurationInBytes = convertMillisecToBytes(ai.audioDurationInMillisec);
            m_dateLastSync = ai.lastLoadDate;
        }
    }
    emit setDurationOnSlider(converBytesToMillisec(m_audioDurationInBytes));
}

quint64 CacheAudioManager::byteNumberForPastToFile(quint64 byte) {
    quint64 byteForPast = 0;
    for(listIter it = m_list.begin(); it != m_list.end() && it->beginByte < byte; it++) {
        byteForPast += it->sectionSize();
    }
    return byteForPast;
}

void CacheAudioManager::addSectionToList(const CacheElement& cacheElem) {
    for(listIter it = m_list.begin(); it != m_list.end(); it++) {
        if(cacheElem.beginByte < it->beginByte) {
            m_list.insert(it, cacheElem);
            return;
        }
    }
    //else, push like last section
    m_list.push_back(cacheElem);
}

void CacheAudioManager::reset() {
    bool wasPlaying = VlcPlayer::Instance().isPlaying();
    const QString fileName = m_fileName;
    const ID songId = m_songId;
    quint32 curTime = m_timer->getCurTime();
    m_list.clear();
    m_headerBytesSize = 0;
    unsetSong();
    setSong(fileName, songId);
    loadHeader();
    m_timer->setTime(curTime);
    if(wasPlaying) {
        play();
    }
}

bool CacheAudioManager::pasteBytesToFile(const std::string& bytes,
                                         quint64            position){
    QFile file(Settings::Instance().getPathToAudiofiles() + m_fileName);

    //if 'bytes' is not header, and
    //file doesn't exist - some bad person have deleted cache!
    if(position != 0 && file.exists() == NOT_EXISTS) {
        reset();
        return STATUS_BAD;
    }

    if(!file.open(QIODevice::ReadWrite)) {
        qWarning() << "CacheAudioManager::pasteBytesToFile\t fail to open file";
        return STATUS_BAD;
    }
    QByteArray byteArray(bytes.c_str(), bytes.size());

    QByteArray temp;
    file.seek(position); //go to start position for past
    temp = file.readAll(); //read all, starting on start position
    file.seek(position); //again, go to start position for past
    file.write(byteArray); //past raw for past
    file.write(temp); //past previously read part

    file.close();
    return STATUS_OK;
}

void CacheAudioManager::uploadListFromFile() {
    QFile cachedFile(Settings::Instance().getPathToAudiofiles() + m_cacheinfoFileName);
    QFile audioFile(Settings::Instance().getPathToAudiofiles() + m_fileName);
    if(cachedFile.exists() == NOT_EXISTS) {
        //or the song firstly played,
        //or someone have deleted cached audiofile!
        audioFile.remove(); //if cachedfile doesn't exist, then audiofile must not exists
        return;
    }
    if(!cachedFile.open(QIODevice::ReadOnly)) { //cachedFile doesn't exists. for what read nothing..?
        return;
    }
    QTextStream in(&cachedFile);
    m_dateLastSync = in.readLine();
    m_headerBytesSize = in.readLine().toUInt();
    m_bytesPerMillisec = in.readLine().toUInt();
    m_audioDurationInBytes = in.readLine().toUInt();
    while (!in.atEnd()) {
        QString line = in.readLine();
        QStringList list = line.split("/");
        m_list.push_back(CacheElement(list.first().toUInt(),
                                    list.last().toUInt()));
    }
    cachedFile.close();

    if(!(m_list.empty()) && audioFile.exists() == NOT_EXISTS) {
        //cachedfile exists, audiofile doesn't exist - someone have deleted audiofile!
        m_list.clear();
        m_headerBytesSize = 0;
        m_bytesPerMillisec = 0;
        m_audioDurationInBytes = 0;
    }
}

void CacheAudioManager::unloadListToFile() {
    QFile file(Settings::Instance().getPathToAudiofiles() + m_cacheinfoFileName);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        qWarning() << "CacheAudioManager::unloadListToFile\t Fail to open file";
        return;
    }
    QTextStream out(&file);
    out.seek(0);

    out << m_dateLastSync << "\r\n";
    out << m_headerBytesSize << "\r\n";
    out << m_bytesPerMillisec << "\r\n";
    out << m_audioDurationInBytes << "\r\n";

    if(m_list.empty()) {
        return;
    }

    //concatenate neighbors parts
    out << m_list.first().beginByte << "/";
    for(listIter it = m_list.begin(), itNext = it + 1; itNext != m_list.end(); it++, itNext++) {
        if(it->endByte != itNext->beginByte) {
            out << it->endByte << "\r\n";
            out << itNext->beginByte << "/";
        }

    }
    out << m_list.last().endByte << "\r\n";

//    for(const auto &elem: m_list) {
//        out << elem.beginByte
//            << "/"
//            << elem.endByte
//            << "\r\n";
//    }

    file.close();
}

quint64 CacheAudioManager::nearestByteOfUnavailableSection(quint64 bytePosition) {
    listIter it = getSectionByByte(bytePosition);
    quint64 byteEnd = bytePosition;
    if(it != m_list.end()) { //section exists. else, got millisec is not in section
        listIter itNext = it + 1;
        while(itNext != m_list.end()
              && (it->endByte - itNext->beginByte) == 0) { //while don't have blank between sections
            ++it;
            ++itNext;
        }
        byteEnd = it->endByte ;
    }
    return byteEnd;
}

quint64 CacheAudioManager::getEndOfUnavailableSectionByStartByte(quint64 startByte) {
    listIter it = m_list.begin();
    for( ; it != m_list.end(); ++it) {
        if(startByte < it->endByte) {
            if((it->beginByte - startByte) > m_partBytesSize) { //if blank section bigger, then load section
                return startByte + m_partBytesSize;
            } else {
                return it->beginByte;
            }
        }
    }
    quint64 end = startByte + m_partBytesSize;
    if(end >= m_audioDurationInBytes) {
        return m_audioDurationInBytes;
    }
    return end;
}

quint64 CacheAudioManager::convertMillisecToBytes(quint32 millisec) {
    if(m_bytesPerMillisec == 0) {
        return 0;
    } else {
        return (millisec * m_bytesPerMillisec) + m_headerBytesSize;
    }
}

quint32 CacheAudioManager::converBytesToMillisec(quint64 bytes) {
    if(m_bytesPerMillisec == 0 || bytes == 0) {
        return 0;
    } else {
        return (bytes - m_headerBytesSize) / m_bytesPerMillisec;
    }
}

CacheAudioManager::listIter
CacheAudioManager::getSectionByByte(quint64 byte) {
    listIter it = m_list.begin();
    for( ; it != m_list.end(); ++it) {
        if(it->beginByte <= byte && byte < it->endByte) {
            break;
        }
    }
    return it;
}

std::string CacheAudioManager::getSectionFromServer(quint64 startByte,
                                                    quint64 sectionSize,
                                                    bool&   ok) {
    std::string bytes = GrpcManager::Instance().get_audio_section(m_songId,
                                                                     startByte,
                                                                     sectionSize,
                                                                     ok);
    if(ok == STATUS_BAD) {
        qWarning() << "CacheAudioManager::getSectionFromServer\t "
                   << "Could not load section from server from " << startByte
                   << "byte with section size " << sectionSize
                   << " bytes, file name: " << m_fileName;
        bytes = "";
    }
    return bytes;
}

bool CacheAudioManager::setRightSectionFromServer(quint64& startByte, quint64& sectionSize) {
    bool ok = STATUS_BAD;
    GrpcManager::Instance().get_right_audio_section(m_songId,
                                                       startByte,
                                                       sectionSize,
                                                       ok);
    if(ok == STATUS_BAD) {
        qWarning() << "CacheAudioManager::setRightSectionFromServer\t "
                   << "Could not get right section from server";
        return STATUS_BAD;
    }
    return STATUS_OK;
}

bool CacheAudioManager::fileExists() {
    return QFile(Settings::Instance().getPathToAudiofiles() + m_fileName).exists();
}

int CacheAudioManager::getFileExtension() {
    if(m_fileName.endsWith("wav", Qt::CaseInsensitive)) {
        return FileExtension::WAV;
    }
    return FileExtension::FLAC;
}
