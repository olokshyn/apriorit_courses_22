#include "cacheconnection.h"
#include "cacheaudiomanager.h"
#include <QThread>

CacheConnection& CacheConnection::Instance() {
    static CacheConnection cacheConnection;
    return cacheConnection;
}

CacheConnection::CacheConnection(QObject *parent)
    : QObject(parent)
{
    CacheAudioManager * cacheManager = new CacheAudioManager();
    m_cacheManagerThread = new QThread();
    cacheManager->moveToThread(m_cacheManagerThread);

    connect(m_cacheManagerThread, SIGNAL(finished()),
            cacheManager, SLOT(deleteLater()));

    connect(this, SIGNAL(play()),
            cacheManager, SLOT(play()));
    connect(this, SIGNAL(pause()),
            cacheManager, SLOT(pause()));
    connect(this, SIGNAL(setSong(QString,ID)),
            cacheManager, SLOT(setSong(QString,ID)));
    connect(this, SIGNAL(loadHeader()),
            cacheManager, SLOT(loadHeader()));
    connect(this, SIGNAL(unsetSong()),
            cacheManager, SLOT(unsetSong()));

    connect(cacheManager, SIGNAL(setCachedSections(quint32,quint32)),
            this, SIGNAL(setCachedSections(quint32,quint32)));
    connect(cacheManager, SIGNAL(setPositionOnSlider(quint32)),
            this, SIGNAL(setPositionOnSlider(quint32)));
    connect(cacheManager, SIGNAL(setDurationOnSlider(quint32)),
            this, SIGNAL(setDurationOnSlider(quint32)));
    connect(cacheManager, SIGNAL(endOfAudio()),
            this, SIGNAL(endOfAudio()));

    connect(this, SIGNAL(sliderPositionChangedByClick(quint32)),
            cacheManager, SLOT(sliderPositionChangedByClick(quint32)));

    m_cacheManagerThread->start();
}

CacheConnection::~CacheConnection() {
    m_cacheManagerThread->quit();
    m_cacheManagerThread->wait();

    delete m_cacheManagerThread;
}
