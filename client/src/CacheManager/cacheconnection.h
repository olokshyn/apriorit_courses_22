#ifndef CACHECONNECTION_H
#define CACHECONNECTION_H

#include <QObject>
#include "../commondefines.h"

class CacheAudioManager;
class QThread;

class CacheConnection : public QObject
{
    Q_OBJECT
public:
    static CacheConnection& Instance();

signals:
    void setCachedSections(quint32 startPosition,
                           quint32 endPosition);
    void setPositionOnSlider(quint32 newPosition);
    void setDurationOnSlider(quint32 newDuration);
    void endOfAudio();

    void play();
    void pause();
    void setSong(const QString& fileName,
                 ID             songId);
    void loadHeader();
    void unsetSong();

    void sliderPositionChangedByClick(quint32 position);

private:
    explicit CacheConnection(QObject *parent = nullptr);
    ~CacheConnection();

private:
    QThread * m_cacheManagerThread;
};

#endif // CACHECONNECTION_H
