#ifndef CACHEAUDIOMANAGER_H
#define CACHEAUDIOMANAGER_H

#include <QObject>
#include <QList>
#include <string>
#include "../commondefines.h"

class Timer;

class CacheAudioManager : public QObject
{
    Q_OBJECT
public:
    explicit CacheAudioManager(QObject *parent = nullptr);
    CacheAudioManager(const CacheAudioManager&) = delete;
    CacheAudioManager& operator=(const CacheAudioManager&) = delete;
    ~CacheAudioManager();

public slots:
    void play();
    void pause();
    void setSong(const QString& fileName,
                 ID             songId);
    void loadHeader();
    void unsetSong();
    void reset();
    void sliderPositionChangedByClick(quint32 position);

signals:
    void setCachedSections(quint32 startPosition,
                           quint32 endPosition);
    void setPositionOnSlider(quint32 newPosition);
    void setDurationOnSlider(quint32 newDuration);
    void endOfAudio();

ACCESS slots:
    bool positionChanged(quint32 positionMillisec);
    void audioPathChanged(const QString& audioPath);
    void audioMillisecPartChanged(quint32 audioMillisecPart);

ACCESS:
    struct CacheElement
    {
        quint64 beginByte;
        quint64 endByte;
        CacheElement();
        CacheElement(quint64 beg,quint64 end);
        quint64 sectionSize() const;
        bool operator==(const CacheElement& other) const;
    };

    using CachedList = QList<CacheElement>;
    using listIter = CachedList::iterator;

    quint32  getPositionForPlayer(quint32 position);
    void     setPosition(quint32 position);

    quint64  nearestByteOfUnavailableSection(quint64 bytePosition);
    quint64  getEndOfUnavailableSectionByStartByte(quint64 startByte);
    quint64  byteNumberForPastToFile(quint64 byte);
    bool     pasteBytesToFile(const std::string& bytes,
                              quint64            position);
    listIter getSectionByByte(quint64 byte);

    void     uploadListFromFile();
    void     unloadListToFile();
    void     addSectionToList(const CacheElement& cacheElem);

    quint64  convertMillisecToBytes(quint32 millisec);
    quint32  converBytesToMillisec(quint64 bytes);

    void        setHeaderFromServer();
    void        setAudiofileInfoFromServer();
    std::string getSectionFromServer(quint64 startByte, quint64 sectionSize, bool& ok);
    bool        setRightSectionFromServer(quint64& startByte, quint64& sectionSize);
    void        preload(quint32 position);
    bool        fileExists();
    int         getFileExtension();
    void        setCachedSectionsOnTimeline();

ACCESS:
    QString    m_dateLastSync;
    QString    m_fileName;
    QString    m_cacheinfoFileName;
    quint16    m_bytesPerMillisec;
    quint16    m_headerBytesSize;
    quint64    m_partBytesSize;
    quint64    m_audioDurationInBytes;
    ID         m_songId;
    CachedList m_list;
    Timer *    m_timer;
};

#endif // CACHEAUDIOMANAGER_H
