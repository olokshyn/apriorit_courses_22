#ifndef TIMER_H
#define TIMER_H

#include <QObject>
#include "../commondefines.h"

class QTimer;

class Timer : public QObject
{
    Q_OBJECT
public:
    explicit Timer(QObject * parent = nullptr);
    Timer(const Timer&) = delete;
    Timer& operator=(const Timer&) = delete;
    ~Timer();

public:
    void    start();
    void    stop();
    void    setTime(quint32 time);
    void    setLimit(quint32 limit);
    void    setEventInterval(quint16 interval);
    quint32 getCurTime();

signals:
    void timeoutEvent(quint32 time);
    void timeLimitReached();

ACCESS slots:
    void setCurTime();
    void slotTimeoutEvent();

ACCESS:
    quint32  m_limit;
    quint32  m_curTime;
    QTimer * m_qTimer;
    QTimer * m_eventTimer;
};

#endif // TIMER_H
