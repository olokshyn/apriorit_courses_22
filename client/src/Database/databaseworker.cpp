#include "databaseworker.h"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QFile>
#include <QMap>

DatabaseWorker::DatabaseWorker(QObject *parent)
    : QObject(parent),
      m_db(QSqlDatabase::addDatabase("QSQLITE")),
      m_sqlQuery(nullptr)
{
    //check file with db
    QFile file("./db.sqlite");
    bool fileExists = file.exists() && file.size() != 0;
    m_db.setDatabaseName(file.fileName());
    m_db.open();
    m_sqlQuery = new QSqlQuery(m_db);

    //if file doesn't exist and is empty, create db with tables
    if (fileExists == NOT_EXISTS) {
        qWarning() << "Could not open the database. I on the way to create a new database";
        createTables();
    }
}

DatabaseWorker::~DatabaseWorker() {
    delete m_sqlQuery;
    m_db.close();
}

void DatabaseWorker::createTables() {
    m_sqlQuery->exec("CREATE TABLE 'Albums' ("
                     "'album_id'	INTEGER NOT NULL UNIQUE,"
                     "'album_title'	TEXT NOT NULL,"
                     "'album_year'	INTEGER NOT NULL,"
                     "'artist_id'	INTEGER NOT NULL,"
                     "'coverfile_name'	TEXT NOT NULL UNIQUE,"
                     "PRIMARY KEY('album_id')"
                     ");");
    m_sqlQuery->exec("CREATE TABLE 'Artists' ("
                     "'artist_id'	INTEGER NOT NULL UNIQUE,"
                     "'artist_title'	TEXT NOT NULL UNIQUE,"
                     "PRIMARY KEY('artist_id')"
                     ");");
    m_sqlQuery->exec("CREATE TABLE 'Playlists' ("
                     "'playlist_id'	INTEGER NOT NULL UNIQUE,"
                     "'playlist_title'	TEXT NOT NULL UNIQUE,"
                     "PRIMARY KEY('playlist_id')"
                     ");");
    m_sqlQuery->exec("CREATE TABLE 'Song_in_playlist' ("
                     "'playlist_id'	INTEGER NOT NULL,"
                     "'song_id'	INTEGER NOT NULL"
                     ");");
    m_sqlQuery->exec("CREATE TABLE 'Songs' ("
                     "'song_id'	INTEGER NOT NULL UNIQUE,"
                     "'song_title'	TEXT NOT NULL UNIQUE,"
                     "'album_id'	INTEGER NOT NULL,"
                     "'audiofile_name'	TEXT NOT NULL UNIQUE,"
                     "PRIMARY KEY('song_id')"
                     ");");
}

void DatabaseWorker::uploadSongsMetadataFromDB() {

    QSharedPointer<MetadataFactory> metadataFactory(QSharedPointer<MetadataFactory>::create());
    //firstly, get all albums
    if(m_sqlQuery->exec("SELECT album_year, Albums.album_id, album_title, "
                        "Artists.artist_id, artist_title, coverfile_name "
                        "FROM Albums, Artists "
                        "WHERE Albums.artist_id = Artists.artist_id")) {
         while(m_sqlQuery->next()) {
             metadataFactory->getMetadataType(
                        m_sqlQuery->value(0).toInt(),
                        m_sqlQuery->value(1).toInt(),
                        m_sqlQuery->value(2).toString(),
                        m_sqlQuery->value(3).toInt(),
                        m_sqlQuery->value(4).toString(),
                        m_sqlQuery->value(5).toString());
         }
    } else {
        qWarning() << "DatabaseWorker::uploadSongsMetadataFromDB\t Wrong query for metatypes. Error: "
        << m_sqlQuery->lastError().text();
        return;
    }

    emit sendAlbums(metadataFactory);


    QMap<ID, QSharedPointer<Metadata>> metadata;
    //and get songs
    if (m_sqlQuery->exec("SELECT song_id, song_title, album_id, audiofile_name "
                         "FROM Songs"))
    {
        while(m_sqlQuery->next()) {
            ID songId = m_sqlQuery->value(0).toInt();
            QString song_title = m_sqlQuery->value(1).toString();
            ID album_id = m_sqlQuery->value(2).toInt();
            QString audiofile_name = m_sqlQuery->value(3).toString();
            QSharedPointer<Metadata> tempSharedetadata(QSharedPointer<Metadata>::create(
                                        songId,
                                        song_title,
                                        audiofile_name,
                                        *(metadataFactory->getMetaType(album_id)))); //create the metadata of some song
            metadata.insert(songId, tempSharedetadata); //and past
        }
    }
    else {
        qWarning() << "DatabaseWorker::uploadSongsMetadataFromDB\t Wrong query for songs. Error: "
        << m_sqlQuery->lastError().text();
        return;
    }

    emit sendMetadata(metadata);

}

void DatabaseWorker::uploadPlaylistsDataFromDB() {
    QList<ID> identificators;
    QList<QString> titles;
    size_t size = 0;
    //select identificators and titles to the container
    if(m_sqlQuery->exec("SELECT playlist_id, playlist_title "
                  "FROM Playlists")) {
         while(m_sqlQuery->next()) {
             identificators.append(static_cast<ID>(m_sqlQuery->value(0).toInt()));
             titles.append(m_sqlQuery->value(1).toString());
             ++size;
         }
    } else {
        qWarning() << "DatabaseWorker::uploadPlaylistsDataFromDB\t Wrong query for identificators and titles. Error: "
                 << m_sqlQuery->lastError().text();
        return;
    }
    QMap<ID, QSharedPointer<PlaylistData>> playlists;
    //+ select songs and add to a common container
    for(size_t i = 0; i < size; ++i) { //get from the table Songs all about a song and past it to m_songsMetadata
        if (m_sqlQuery->exec("SELECT song_id "
                       "FROM Song_in_playlist "
                       "WHERE playlist_id = \"" + QString::number(identificators[i]) + "\""))
        {
            QVector<ID> songsIdOfPlaylist;
            while(m_sqlQuery->next()) { //add all identificators of playlist
                songsIdOfPlaylist.append(m_sqlQuery->value(0).toInt());
            }
            QSharedPointer<PlaylistData> playlistData(QSharedPointer<PlaylistData>::create(
                                      identificators[i],
                                      titles[i],
                                      songsIdOfPlaylist));
            playlists.insert(identificators[i], playlistData); //and past
        }
        else {
            qWarning() << "DatabaseWorker::uploadPlaylistsDataFromDB\t Wrong query for playlists. Error: "
            << m_sqlQuery->lastError().text();
            return;
        }
    }

    emit sendPlaylists(playlists);
}

void DatabaseWorker::updateSongInDB(QSharedPointer<Metadata> metadata) {
//insert artist (if exists, will not changed)
    const QString artistIdStr = QString::number(metadata->getArtistId());
    if(!m_sqlQuery->exec("REPLACE INTO Artists(artist_id, artist_title) "
                         "VALUES(\"" + artistIdStr + "\","
                         "\"" + metadata->getArtistTitle() + "\")")) {
        qWarning() << "Database::unloadSongsMetadataToDB\t Wrong query for table Artists. Error: "
        << m_sqlQuery->lastError().text();
        return;
    }

//insert album (if exists, will not changed)
    if(!m_sqlQuery->exec("REPLACE INTO Albums(album_id, album_title, album_year, artist_id, coverfile_name) "
                         "VALUES(\"" + QString::number(metadata->getAlbumId()) + "\","
                         "\"" + metadata->getAlbumTitle() + "\","
                         "\"" + QString::number(metadata->getYear()) + "\","
                         "\"" + artistIdStr + "\","
                         "\"" + metadata->getCoverfileName() + "\")")) {
        qWarning() << "DatabaseWorker::unloadSongsMetadataToDB\t Wrong query for table Albums. Error: "
        << m_sqlQuery->lastError().text();
        return;
    }

//insert song (if exists, will not changed)
    if(!m_sqlQuery->exec("REPLACE INTO Songs(song_id, song_title, album_id, audiofile_name) "
                         "VALUES(\"" + QString::number(metadata->getId()) + "\","
                         "\"" + metadata->getSongTitle() + "\","
                         "\"" + QString::number(metadata->getAlbumId()) + "\","
                         "\"" + metadata->getAudiofileName() + "\")")) {
        qWarning() << "DatabaseWorker::unloadSongsMetadataToDB\t Wrong query for table Songs. Error: "
        << m_sqlQuery->lastError().text();
        return;
    }

}

void DatabaseWorker::updatePlaylistInDB(QSharedPointer<PlaylistData> playlistData) {

//replace id and title
    const QString playlistIdStr = QString::number(playlistData->getId());
    if(!m_sqlQuery->exec("REPLACE INTO Playlists(playlist_id, playlist_title) "
                         "VALUES(\"" + playlistIdStr + "\","
                         "\"" + playlistData->getTitle() + "\")")) {
        qWarning() << "DatabaseWorker::unloadPlaylistsDataToDB\t Wrong query for table Playlists. Error: "
        << m_sqlQuery->lastError().text();
        return;
    }

//delete all songs from playlist
    if(!(m_sqlQuery->exec("DELETE FROM Song_in_playlist "
                          "WHERE playlist_id = \"" + playlistIdStr + "\"")))
    {
        qWarning() << "DatabaseWorker::unloadPlaylistsDataToDB\t Wrong query for table Song_in_playlist. Error: "
                << m_sqlQuery->lastError().text();
        return;
    }

//past new songs identificators
    for(const ID id: playlistData->getVectorIdentificators()) {
        if(!(m_sqlQuery->exec("INSERT INTO Song_in_playlist (playlist_id, song_id)"
                              "VALUES (\"" + playlistIdStr + "\", "
                              "\"" + QString::number(id) +"\")")))
        {
            qWarning() << "DatabaseWorker::unloadPlaylistsDataToDB\t Wrong query for table Song_in_playlist. Error: "
                    << m_sqlQuery->lastError().text();
        }
    }
}

void DatabaseWorker::deleteSongFromDB(ID songId) {
//delete a song from db
    if(!(m_sqlQuery->exec("DELETE FROM Songs "
                   "WHERE song_id = " + QString::number(songId) ))) {
       qWarning() << "DatabaseWorker::deleteSongFromDB\t Wrong query for table Songs. Error: "
               << m_sqlQuery->lastError().text();
    }
}

void DatabaseWorker::deletePlaylistFromDB(ID playlistId) {
    QString idStr = QString::number(playlistId);

//delete songs in a playlist
    if(!(m_sqlQuery->exec("DELETE FROM Song_in_playlist "
                    "WHERE playlist_id = \"" + idStr + "\""))) {
        qWarning() << "DatabaseWorker::deletePlaylistFromDB\t Wrong query for table Song_in_playlist. Error: "
                << m_sqlQuery->lastError().text();
    }

//delete the playlist
    if(!(m_sqlQuery->exec("DELETE FROM Playlists "
                    "WHERE playlist_id = \""  + idStr + "\""))) {
        qWarning() << "DatabaseWorker::deletePlaylistFromDB\t Wrong query for table Playlists. Error: "
                << m_sqlQuery->lastError().text();
    }
}
