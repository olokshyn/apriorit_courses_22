#ifndef DATABASEWORKER_H
#define DATABASEWORKER_H

#include <QObject>
#include <QMap>
#include <QSqlDatabase>
#include "../commondefines.h"
#include "../metadata/metadata.h"
#include "../metadata/metadatatype.h"
#include "../metadata/metadatafactory.h"
#include "../playlistdata.h"

class QSqlQuery;

class DatabaseWorker : public QObject
{
    Q_OBJECT

public:
    explicit DatabaseWorker(QObject *parent = nullptr);
    ~DatabaseWorker();

public slots:
    void uploadPlaylistsDataFromDB();
    void uploadSongsMetadataFromDB();

    void updateSongInDB(QSharedPointer<Metadata> metadata);
    void updatePlaylistInDB(QSharedPointer<PlaylistData> playlistData);
    void deleteSongFromDB(ID songId);
    void deletePlaylistFromDB(ID playlistId);

signals:
    void sendAlbums(QSharedPointer<MetadataFactory> metadataFactory);
    void sendMetadata(QMap<ID, QSharedPointer<Metadata>> metadata);
    void sendPlaylists(QMap<ID, QSharedPointer<PlaylistData>> playlistsData);

private:
    void createTables();

private:
    QSqlDatabase m_db;
    QSqlQuery *  m_sqlQuery;
};

#endif // DATABASEWORKER_H
