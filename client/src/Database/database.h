#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QMap>
#include <QSharedPointer>
#include "../commondefines.h"
#include "../metadata/metadata.h"
#include "../metadata/metadatatype.h"
#include "../metadata/metadatafactory.h"
#include "../playlistdata.h"

class QThread;

class Database : public QObject
{
    Q_OBJECT

public:
    static Database& Instance();

    //get
    QSharedPointer<Metadata>              getSongMetadata(ID id);
    QSharedPointer<PlaylistData>          getPlaylistData(ID id);
    QList<QSharedPointer<PlaylistData>>   getAllPlaylists();
    ID                          getPlaylistIdByTitle(const QString& title);
    QSharedPointer<MetadataFactory>       getMetadataFactory();

signals:
    void successfulLoadedMetadata();

    //worker
    void uploadSongsMetadataFromDB();
    void uploadPlaylistsDataFromDB();

    void updateSongInDB(QSharedPointer<Metadata> metadata);
    void updatePlaylistInDB(QSharedPointer<PlaylistData> playlistData);
    void deleteSongFromDB(ID songId);
    void deletePlaylistFromDB(ID playlistId);


private slots:
    //worker
    void getAlbumsFromWorker(QSharedPointer<MetadataFactory> metadataFactory);
    void getMetadataFromWorker(QMap<ID,  QSharedPointer<Metadata>> metadata);
    void getPlaylistsFromWorker(QMap<ID, QSharedPointer<PlaylistData>> playlistsData);

    //for me
    void updateSong(QSharedPointer<Metadata> metadata);
    void updatePlaylist(QSharedPointer<PlaylistData> playlistData);
    void deleteSong(ID songId);
    void deletePlaylist(ID playlistId);

private:
    Database(QObject * parent = nullptr);
    Database(const Database&) = delete;
    Database& operator=(const Database&) = delete;
    ~Database();

private:
    void updatePlaylistWithAllSongs();

ACCESS:
    QMap<ID, QSharedPointer<PlaylistData>> m_playlistsData; //all playlists data
    QMap<ID, QSharedPointer<Metadata>>     m_songsMetadata; //all songs metadata
    QSharedPointer<MetadataFactory>        m_metadataFactory; //all albums
    QThread *                              m_workerThread;
};

#endif // DATABASE_H
