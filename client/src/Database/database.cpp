#include "database.h"
#include "../settings.h"
#include "databaseworker.h"
#include <QFileInfo>
#include <QList>
#include <QVector>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QDebug>
#include <algorithm>
#include <QThread>
#include "../ListModel/modelconnection.h"

Database::Database(QObject * parent)
    : QObject(parent),
      m_playlistsData(),
      m_songsMetadata(),
      m_metadataFactory(QSharedPointer<MetadataFactory>::create()),
      m_workerThread(nullptr)
{

    qRegisterMetaType<QSharedPointer<Metadata>>("QSharedPointer<Metadata>");
    qRegisterMetaType<QSharedPointer<PlaylistData>>("QSharedPointer<PlaylistData>");
    qRegisterMetaType<QSharedPointer<MetadataFactory>>("QSharedPointer<MetadataFactory>");
    qRegisterMetaType<QMap<ID, QSharedPointer<Metadata>>>("QMap<ID, QSharedPointer<Metadata>>");
    qRegisterMetaType<QMap<ID, QSharedPointer<PlaylistData>>>("QMap<ID, QSharedPointer<PlaylistData>>");

    DatabaseWorker * dbWorker = new DatabaseWorker();
    m_workerThread = new QThread();
    dbWorker->moveToThread(m_workerThread);

    connect(m_workerThread, SIGNAL(finished()),
            dbWorker,       SLOT(deleteLater()));
    connect(this,           SIGNAL(uploadSongsMetadataFromDB()),
            dbWorker,       SLOT(uploadSongsMetadataFromDB()));
    connect(this,           SIGNAL(uploadPlaylistsDataFromDB()),
            dbWorker,       SLOT(uploadPlaylistsDataFromDB()));

    connect(this,           SIGNAL(updateSongInDB(QSharedPointer<Metadata>)),
            dbWorker,       SLOT(updateSongInDB(QSharedPointer<Metadata>)));
    connect(this,           SIGNAL(updatePlaylistInDB(QSharedPointer<PlaylistData>)),
            dbWorker,       SLOT(updatePlaylistInDB(QSharedPointer<PlaylistData>)));

    connect(this,           SIGNAL(deleteSongFromDB(ID)),
            dbWorker,       SLOT(deleteSongFromDB(ID)));
    connect(this,           SIGNAL(deletePlaylistFromDB(ID)),
            dbWorker,       SLOT(deletePlaylistFromDB(ID)));

    connect(dbWorker,       SIGNAL(sendMetadata(QMap<ID,QSharedPointer<Metadata>>)),
            this,           SLOT(getMetadataFromWorker(QMap<ID,QSharedPointer<Metadata>>)));
    connect(dbWorker,       SIGNAL(sendPlaylists(QMap<ID,QSharedPointer<PlaylistData>>)),
            this,           SLOT(getPlaylistsFromWorker(QMap<ID,QSharedPointer<PlaylistData>>)));
    connect(dbWorker,       SIGNAL(sendAlbums(QSharedPointer<MetadataFactory>)),
            this,           SLOT(getAlbumsFromWorker(QSharedPointer<MetadataFactory>)));
    m_workerThread->start();

    connect(this,           SIGNAL(deleteSongFromDB(ID)),
            this,           SLOT(deleteSong(ID)));
    connect(this,           SIGNAL(deletePlaylistFromDB(ID)),
            this,           SLOT(deletePlaylist(ID)));
    connect(this,           SIGNAL(updateSongInDB(QSharedPointer<Metadata>)),
            this,           SLOT(updateSong(QSharedPointer<Metadata>)));
    connect(this,           SIGNAL(updatePlaylistInDB(QSharedPointer<PlaylistData>)),
            this,           SLOT(updatePlaylist(QSharedPointer<PlaylistData>)));

    emit uploadSongsMetadataFromDB();
    emit uploadPlaylistsDataFromDB();
}


Database::~Database() {
    m_workerThread->quit();
    m_workerThread->wait();
    delete m_workerThread;
}

Database& Database::Instance() {
    static Database database;
    return database;
}

QSharedPointer<MetadataFactory>
Database::getMetadataFactory() {
    return m_metadataFactory;
}

void Database::getAlbumsFromWorker(QSharedPointer<MetadataFactory> metadataFactory) {
    m_metadataFactory = metadataFactory;
    ModelConnection::Instance().updateAlbums();
}

void Database::getMetadataFromWorker(QMap<ID,  QSharedPointer<Metadata>> metadata) {
    m_songsMetadata = metadata;
    updatePlaylistWithAllSongs();
    ModelConnection::Instance().updateAllMetadata();
    ModelConnection::Instance().updatePlaylists();
    ModelConnection::Instance().setPlaylistData(0);
    emit successfulLoadedMetadata();
}

void Database::getPlaylistsFromWorker(QMap<ID, QSharedPointer<PlaylistData>> playlistsData) {
    m_playlistsData = playlistsData;
    updatePlaylistWithAllSongs();
    ModelConnection::Instance().updatePlaylists();
}

void Database::updateSong(QSharedPointer<Metadata> metadata) {
    //if exist, will replace value
    m_songsMetadata.insert(metadata->getId(),
                           metadata);
}

void Database::updatePlaylist(QSharedPointer<PlaylistData> playlistData) {
    //if exist, will replace value
    m_playlistsData.insert(playlistData->getId(),
                           playlistData);
}

void Database::deleteSong(ID songId) {
//delete an audiofile
    //get audiofileName
    if(m_songsMetadata.value(songId)->audiofileExists() == EXISTS) {
        QString audiofileName = m_songsMetadata[songId]->getAudiofileName();
        //delete an audiofile
        if(audiofileName != "") {
            QFile file(Settings::Instance().getPathToAudiofiles()
                            + audiofileName);
            file.remove();
        }
    }

//delete the song from qmap
    m_songsMetadata.remove(songId);
}

void Database::deletePlaylist(ID playlistId) {
    //delete the playlist from qmap
    m_playlistsData.remove(playlistId);
}

QSharedPointer<Metadata> Database::getSongMetadata(ID id) {
    return m_songsMetadata[id];
}

QSharedPointer<PlaylistData> Database::getPlaylistData(ID id) {
    if(id == 0) { //if need to return list with all songs
        updatePlaylistWithAllSongs();
    }
    return m_playlistsData[id];
}

void Database::updatePlaylistWithAllSongs() {
   QVector<ID> identificators = QVector<ID>::fromList(m_songsMetadata.uniqueKeys()); //get all songs id
   QSharedPointer<PlaylistData> playlist(QSharedPointer<PlaylistData>::create(0, "", identificators));
   m_playlistsData.insert(0, playlist); //add list with all songs id
}

QList<QSharedPointer<PlaylistData>> Database::getAllPlaylists() {
    return m_playlistsData.values();
}

ID Database::getPlaylistIdByTitle(const QString& title) {
    for(const QSharedPointer<PlaylistData>& playlist: m_playlistsData) {
        if(playlist->getTitle() == title) {
            return playlist->getId();
        }
    }
    qWarning() << "Database::getPlaylistIdByTitle\t Could not find playlist.";
    return 0;
}
