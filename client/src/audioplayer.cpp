#include "audioplayer.h"
#include "playlist.h"
#include "playlistdata.h"
#include "Database/database.h"
#include "metadata/metadata.h"
#include "settings.h"
#include "ListModel/modelconnection.h"
#include "grpcClient/grpcmanager.h"
#include "CacheManager/cacheaudiomanager.h"
#include "vlcplayer.h"
#include <QDebug>

AudioPlayer::AudioPlayer(QObject *parent)
    : QObject(parent),
      m_curPlaylist(nullptr)
{
    VlcPlayer::Instance().setVolume(Settings::Instance().getVolume());

    //connections with grpc
    GrpcManager& grpcManager = GrpcManager::Instance();
    connect(this,         SIGNAL(sync()),
            &grpcManager, SIGNAL(sync()));
    connect(&grpcManager, SIGNAL(successfulSync()),
            this,         SLOT(successfulSync()));

    connect(this,         SIGNAL(addSong(QString, QString, QString, QString, QString, quint16)),
            &grpcManager, SIGNAL(signalSendSong(QString,QString,QString,QString,QString,quint16)));
    connect(this,         SIGNAL(addSongByAlbum(QString,QString,ID)),
            &grpcManager, SIGNAL(signalSendSongByAlbum(QString,QString,ID)));
    connect(this,         SIGNAL(addPlaylist(QString, QString)),
            &grpcManager, SIGNAL(signalSendPlaylist(QString,QString)));
    connect(this,         SIGNAL(editSong(ID,quint16,QString,QString,QString,QString,QString)),
            &grpcManager, SIGNAL(signalUpdateSong(ID,quint16,QString,QString,QString,QString,QString)));
    connect(this,         SIGNAL(editPlaylist(ID, QString, QString, QString)),
            &grpcManager, SIGNAL(signalUpdatePlaylist(ID,QString,QString,QString)));
    connect(this,         SIGNAL(deleteSong(ID)),
            &grpcManager, SIGNAL(signalDeleteSong(ID)));
    connect(this,         SIGNAL(deletePlaylist(ID)),
            &grpcManager, SIGNAL(signalDeletePlaylist(ID)));

    connect(&(Database::Instance()),
                          SIGNAL(successfulLoadedMetadata()),
            this,         SLOT(successfulSync()));
}

AudioPlayer::~AudioPlayer() {
    Settings::Instance().setVolume(VlcPlayer::Instance().getVolume()); //save last volume

    delete m_curPlaylist;
}

void AudioPlayer::play(){
    m_curPlaylist->play();
}

void AudioPlayer::pause() {
    m_curPlaylist->pause();
}

void AudioPlayer::setVolume(int volume) {
    VlcPlayer::Instance().setVolume(volume);
}

int AudioPlayer::getVolume() {
    return VlcPlayer::Instance().getVolume();
}

void AudioPlayer::playNext() {
    bool wasPlaying = isPlaying();

    m_curPlaylist->setNext();
    emit playerCurrentItemChanged(m_curPlaylist->getCurIndex());

    if(wasPlaying) {
        play();
    }
}

void AudioPlayer::setCurIndex(quint32 index) {
    bool wasPlaying = isPlaying();

    m_curPlaylist->set(index);
    emit playerCurrentItemChanged(m_curPlaylist->getCurIndex());

    if(wasPlaying) {
        play();
    }
}

void AudioPlayer::playPrev() {
    bool wasPlaying = isPlaying();

    m_curPlaylist->setPrev();
    emit playerCurrentItemChanged(m_curPlaylist->getCurIndex());

    if(wasPlaying) {
        play();
    }
}

bool AudioPlayer::isPlaying() {
    return VlcPlayer::Instance().isPlaying();
}

void AudioPlayer::setPlaylist(ID id) {
    bool wasPlaying = isPlaying();

    if(!m_curPlaylist || m_curPlaylist->getId() != id) { //if new playlist is not current playlist
        delete m_curPlaylist;
        m_curPlaylist = new Playlist(id);
    } else {
        m_curPlaylist->updatePlaylist();
    }

    ModelConnection::Instance().updatePlaylists();
    ModelConnection::Instance().updateAllMetadata();
    ModelConnection::Instance().setPlaylistData(id); //set in Ui footer new playlist
    if(wasPlaying) {
        m_curPlaylist->play();
    }
}

void AudioPlayer::successfulSync() {
    setPlaylist(0);
//    ModelConnection::Instance().updateAlbums();
    ModelConnection::Instance().updateAllMetadata();
  //  ModelConnection::Instance().updatePlaylists();
    emit playerCurrentItemChanged(0);
}

int AudioPlayer::getPlaylistIdByTitle(const QString& title) {
    return Database::Instance().getPlaylistIdByTitle(title);
}

void AudioPlayer::updateAlbums() {
    ModelConnection::Instance().updateAlbums();
}



