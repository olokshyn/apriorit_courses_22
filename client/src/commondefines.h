#ifndef COMMONDEFINES_H
#define COMMONDEFINES_H
#include <QtGlobal>

//#define TESTING

using ID = quint32;

enum status {STATUS_BAD = false,
             STATUS_OK  = true};

enum exists {NOT_EXISTS  = false,
             EXISTS      = true};

#ifdef TESTING
    #define ACCESS public
#else
    #define ACCESS private
#endif

#endif // COMMONDEFINES_H
