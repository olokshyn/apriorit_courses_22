#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QSharedPointer>
#include "commondefines.h"

class PlaylistData;
class Song;

class Playlist
{
public:
    using sharedPlaylistData = QSharedPointer<PlaylistData>;

    Playlist(ID id); //if id == 0, playlist will have all songs
    Playlist(const Playlist&) = delete;
    Playlist& operator=(const Playlist&) = delete;
    ~Playlist();

public:
    void           play();
    void           pause();
    void           setSong(ID id);
    void           setNext();
    void           setPrev();
    void           set(quint32 index);
    ID             getId();
    const QString& getTitle();
    bool           updatePlaylist();
    unsigned int   getCurIndex();

ACCESS:
    sharedPlaylistData  m_playlistData;
    ID                  m_id;
    unsigned int        m_curIndex;
    Song *              m_curSong;
};

#endif // PLAYLIST_H
