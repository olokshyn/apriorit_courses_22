#include "playlistdata.h"
#include "Database/database.h"
#include <QString>

PlaylistData::PlaylistData(const ID&          id,
                           const QString&      title,
                           const QVector<ID>&  identificators)
    : m_id(id),
      m_title(title),
      m_identificators(identificators)
{}

QVector<PlaylistData::sharedMetadata> PlaylistData::getVectorMetadata() {
    size_t size = m_identificators.size();
    QVector<sharedMetadata> vectorMetadata(size);
    Database &database = Database::Instance();
    for(size_t i = 0; i < size; ++i) {
        vectorMetadata[i] = database.getSongMetadata(m_identificators[i]);
    }
    return vectorMetadata;
}

const QVector<ID>& PlaylistData::getVectorIdentificators() {
    return m_identificators;
}

ID PlaylistData::getId() {
    return m_id;
}

const QString& PlaylistData::getTitle() {
    return m_title;
}

ID PlaylistData::getNextId(ID id) {
    size_t size = m_identificators.size();
    for(size_t i = 0; i < size; i++) {
        if(m_identificators[i] == id) {
            if(i < size - 1) { //if id is not last
                return m_identificators[i+1]; //return next
            }
            break;
        }
    }
    return id; //then return current song
}

ID PlaylistData::getPrevId(ID id) {
    size_t size = m_identificators.size();
    for(size_t i = 0; i < size; i++) {
        if(m_identificators[i] == id) {
            if(i > 0) { //if id is not first
                return m_identificators[i-1]; //else return prev
            }
            break;
        }
    }
    return id; //then return current song
}

unsigned int PlaylistData::getCountOfidentificators() {
    return m_identificators.size();
}
