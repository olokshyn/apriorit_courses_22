#include "vlcplayer.h"
#include <VLCQtCore/Instance.h>
#include <VLCQtCore/Media.h>
#include <VLCQtCore/MediaPlayer.h>
#include <VLCQtCore/Common.h>
#include <VLCQtWidgets/WidgetVolumeSlider.h>
#include <VLCQtCore/Enums.h>
#include <QDebug>
#include <QString>

VlcPlayer& VlcPlayer::Instance() {
    static VlcPlayer vlcPlayer;
    return vlcPlayer;
}

VlcPlayer::VlcPlayer()
    : m_instance(nullptr),
      m_player(nullptr),
      m_volumeSlider(nullptr),
      m_curMedia(nullptr)
{
    m_instance = new VlcInstance(VlcCommon::args());
    m_player = new VlcMediaPlayer(m_instance);
    m_volumeSlider = new VlcWidgetVolumeSlider();
    m_volumeSlider->setMinimum(0);
    m_volumeSlider->setMaximum(100);
    m_volumeSlider->setMediaPlayer(m_player);
}

VlcPlayer::~VlcPlayer() {
    delete m_volumeSlider;
    delete m_curMedia;
    delete m_player;
    delete m_instance;
}

void VlcPlayer::openOnly() {
    if(m_curMedia) {
        m_player->openOnly(m_curMedia);
    } else {
        qWarning() << "VlcPlayer::open\t try openOnly, but cur media doesn't set.";
    }
}

void VlcPlayer::open() {
    if(m_curMedia) {
        m_player->open(m_curMedia);
    } else {
        qWarning() << "VlcPlayer::open\ttry open, but cur media doesn't set.";
    }
}

void VlcPlayer::stop() {
    if(m_curMedia) {
        m_player->stop();
    } else {
        qWarning() << "VlcPlayer::stop\ttry stop, but cur media doesn't set.";
    }
}

void VlcPlayer::setMedia(const QString& fileName) {
    if(m_curMedia) {
        stop();
        delete m_curMedia;
        m_curMedia = nullptr;
    }

    m_curMedia = new VlcMedia(fileName, true, m_instance);
}

void VlcPlayer::setPosition(float position) {
    if(m_curMedia) {
        m_player->setPosition(position);
    } else {
        qWarning() << "VlcPlayer::stop\ttry set position, but cur media doesn't set.";
    }
}

void VlcPlayer::setVolume(int volume) {
    if(m_volumeSlider->minimum() <= volume &&
                                    volume <= m_volumeSlider->maximum()) {
        m_volumeSlider->setVolume(volume);
    }
}

int VlcPlayer::getVolume() {
    return m_volumeSlider->volume();
}

bool VlcPlayer::isPlaying() {
    return m_player->state() == Vlc::Playing;
}
