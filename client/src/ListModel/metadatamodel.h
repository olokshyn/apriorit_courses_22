#ifndef METADATAMODEL_H
#define METADATAMODEL_H

#include <QSharedPointer>
#include "ListItem.h"

class Metadata;

class MetadataModel : public Models::ListItem
{
    Q_OBJECT

public:
    using sharedMetadata = QSharedPointer<Metadata>;

    int id() const;
    explicit MetadataModel(QObject *parent = nullptr);
    explicit MetadataModel(sharedMetadata& metadata, QObject *parent = nullptr);

    enum MetaEnum {
        songId = Qt::UserRole + 1,
        year,
        albumId,
        albumTitle,
        artistId,
        artistTitle,
        songTitle,
        coverfile
    };

    QVariant data(int role) const;
    //bool setData(int role, const QVariant &value);
    QHash<int, QByteArray> roleNames() const;

private:
    sharedMetadata m_metadata;
};

#endif // METADATA_H
