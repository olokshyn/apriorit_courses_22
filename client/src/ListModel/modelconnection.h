#ifndef MODELSINGLETON_H
#define MODELSINGLETON_H

#include "ListModel.h"
#include "../commondefines.h"

class ComboBoxModel;

class ModelConnection
{
public:
    static ModelConnection& Instance();
    Models::ListModel* getMetadataModel();
    Models::ListModel* getAllMetadata();
    Models::ListModel* getAlbumsModel();
    ComboBoxModel& getPlaylistsModel();
    void setPlaylistData(ID id);
    void updatePlaylists();
    void updateAllMetadata();
    void updateAlbums();
private:
    ModelConnection();
    ~ModelConnection();

    ModelConnection(const ModelConnection&) = delete;
    ModelConnection& operator=(const ModelConnection&) = delete;
private:
    Models::ListModel * m_metadataModel;
    Models::ListModel * m_allMetadataModel;
    Models::ListModel * m_albumsModel;
    ComboBoxModel *     m_playlistsModel;
};

#endif // MODELSINGLETON_H
