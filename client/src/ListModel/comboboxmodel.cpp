#include "comboboxmodel.h"
#include "qdebug.h"

ComboBoxModel::ComboBoxModel(QObject *parent)
    : QObject(parent)
{}

ComboBoxModel::ComboBoxModel(const QStringList &list,
                             QObject *parent)
    : QObject(parent),
      m_comboList(list)
{}

const QStringList& ComboBoxModel::comboList() {
    return m_comboList;
}

void ComboBoxModel::setComboList(const QStringList& comboList) {
    if (m_comboList != comboList) {
        m_comboList = comboList;
        emit comboListChanged();
    }
}
