#ifndef ALBUMSMODEL_H
#define ALBUMSMODEL_H

#include <QSharedPointer>
#include <QList>
#include "ListItem.h"

class MetadataType;

class AlbumsModel : public Models::ListItem
{
    Q_OBJECT
public:
    using sharedMetatype = QSharedPointer<MetadataType>;

    int id() const;
    explicit AlbumsModel(QObject *parent = nullptr);
    explicit AlbumsModel(sharedMetatype& type,
                         QObject *parent = nullptr);

    enum MetaEnum {
        albumId = Qt::UserRole + 1,
        albumTitle,
        artistId,
        artistTitle,
        year
    };

    QVariant data(int role) const;
    QHash<int, QByteArray> roleNames() const;

private:
    sharedMetatype m_metatype;
};

#endif // ALBUMSMODEL_H
