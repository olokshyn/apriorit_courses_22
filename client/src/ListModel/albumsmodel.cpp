#include "albumsmodel.h"
#include "../commondefines.h"
#include "../metadata/metadatatype.h"
#include <QDebug>

AlbumsModel::AlbumsModel(QObject *parent)
    : Models::ListItem(parent)
{}

AlbumsModel::AlbumsModel(AlbumsModel::sharedMetatype& type,
                         QObject *                     parent)
    : Models::ListItem(parent),
      m_metatype(type)
{}

int AlbumsModel::id() const {
    return m_metatype->getAlbumId();
}

QVariant AlbumsModel::data(int role) const {
    switch(role)
    {
    case albumId:
        return this->m_metatype->getAlbumId();
    case albumTitle:
        return this->m_metatype->getAlbumTitle();
    case artistId:
        return this->m_metatype->getArtistId();
    case artistTitle:
        return this->m_metatype->getArtistTitle();
    case year:
        return this->m_metatype->getYear();
    }
    return QVariant(); //default
}

QHash<int, QByteArray> AlbumsModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[albumId] = "albumId";
    roles[albumTitle] = "albumTitle";
    roles[artistId] = "artistId";
    roles[artistTitle] = "artistTitle";
    roles[year] = "year";
    return roles;
}
