#include "modelconnection.h"
#include "metadatamodel.h"
#include "comboboxmodel.h"
#include "../Database/database.h"
#include "../metadata/metadata.h"
#include "../playlistdata.h"
#include "comboboxmodel.h"
#include "../metadata/metadatafactory.h"
#include "../metadata/metadatatype.h"
#include "albumsmodel.h"
#include <QList>
#include <QVector>
#include <QString>
#include <QSharedPointer>
#include <QStringList>

ModelConnection::ModelConnection() {
    m_metadataModel = new Models::ListModel(new MetadataModel());
    m_allMetadataModel = new Models::ListModel(new MetadataModel());
    m_albumsModel = new Models::ListModel(new AlbumsModel());
    m_playlistsModel = new ComboBoxModel();
    setPlaylistData(0); //set all songs for first time
    updatePlaylists();
}

Models::ListModel * ModelConnection::getMetadataModel() {
    return m_metadataModel;
}

Models::ListModel * ModelConnection::getAllMetadata() {
    return m_allMetadataModel;
}

Models::ListModel * ModelConnection::getAlbumsModel() {
    return m_albumsModel;
}

ComboBoxModel& ModelConnection::getPlaylistsModel() {
    return *m_playlistsModel;
}

ModelConnection::~ModelConnection() {
    delete m_metadataModel;
    delete m_allMetadataModel;
    delete m_playlistsModel;
}

ModelConnection& ModelConnection::Instance() {
    static ModelConnection model;
    return model;
}

void ModelConnection::setPlaylistData(ID id) {
    QSharedPointer<PlaylistData> myplaylistdata = Database::Instance().getPlaylistData(id);
    QVector<QSharedPointer<Metadata>> myvector = myplaylistdata->getVectorMetadata();
    m_metadataModel->clear();
    for(QSharedPointer<Metadata>& elem: myvector) {
        m_metadataModel->appendRow(new MetadataModel(elem));
    }
}

void ModelConnection::updatePlaylists() {
    QList<QSharedPointer<PlaylistData>> playlists =
            Database::Instance().getAllPlaylists();
    QStringList list;
    for(QSharedPointer<PlaylistData>& playlist: playlists) {
        list.push_back(playlist->getTitle());
    }
    m_playlistsModel->setComboList(list);
}

void ModelConnection::updateAllMetadata() {
    QSharedPointer<PlaylistData> myplaylistdata = Database::Instance().getPlaylistData(0);
    QVector<QSharedPointer<Metadata>> myvector = myplaylistdata->getVectorMetadata();
    m_allMetadataModel->clear();
    for(QSharedPointer<Metadata>& elem: myvector) {
        m_allMetadataModel->appendRow(new MetadataModel(elem));
    }
}

void ModelConnection::updateAlbums() {
    QList<QSharedPointer<MetadataType>> albums = Database::Instance().
            getMetadataFactory()->getAllTypes();
    m_albumsModel->clear();
    for(QSharedPointer<MetadataType>& elem: albums) {
        m_albumsModel->appendRow(new AlbumsModel(elem));
    }
}


