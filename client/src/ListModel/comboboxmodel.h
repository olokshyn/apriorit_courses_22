#ifndef PLAYLISTDATAMODEL_H
#define PLAYLISTDATAMODEL_H
#include <QObject>
#include <QStringList>

class ComboBoxModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QStringList comboList READ comboList WRITE setComboList NOTIFY comboListChanged)

public:
    ComboBoxModel(QObject *parent = 0);
    ComboBoxModel(const QStringList &list, QObject *parent = 0);

    const QStringList& comboList();
    void setComboList(const QStringList& comboList);

signals:

    void comboListChanged();

private:

    QStringList m_comboList;
};

#endif // PLAYLISTMODEL_H
