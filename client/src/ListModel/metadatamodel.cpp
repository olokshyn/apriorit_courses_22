#include "metadatamodel.h"
#include <QDebug>
#include "../commondefines.h"
#include "../settings.h"
#include "../metadata/metadata.h"

MetadataModel::MetadataModel(QObject *parent)
    : Models::ListItem(parent)
{}

MetadataModel::MetadataModel(MetadataModel::sharedMetadata& metadata,
                             QObject *                      parent)
    : Models::ListItem(parent),
      m_metadata(metadata)
{}

int MetadataModel::id() const {
    return m_metadata->getId();
}

QVariant MetadataModel::data(int role) const {
    switch(role)
    {
    case songId:
        return this->m_metadata->getId();
    case year:
        return this->m_metadata->getYear();
    case albumId:
        return this->m_metadata->getAlbumId();
    case albumTitle:
        return this->m_metadata->getAlbumTitle();
    case artistId:
        return this->m_metadata->getArtistId();
    case artistTitle:
        return this->m_metadata->getArtistTitle();
    case songTitle:
        return this->m_metadata->getSongTitle();
    case coverfile:
        return "file:/" + Settings::Instance().getPathToCoverfiles()  // file:/ for desktop (why??)
                    + this->m_metadata->getCoverfileName();
    }
    return QVariant(); //default
}

QHash<int, QByteArray> MetadataModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[songId] = "songId";
    roles[year] = "year";
    roles[albumId] = "albumId";
    roles[albumTitle] = "albumTitle";
    roles[artistId] = "artistId";
    roles[artistTitle] = "artistTitle";
    roles[songTitle] = "songTitle";
    roles[coverfile] = "coverfile";
    return roles;
}
