#include "playlist.h"
#include "song.h"
#include "playlistdata.h"
#include "Database/database.h"
#include "metadata/metadata.h"
#include <QDebug>

Playlist::Playlist(ID id)
    : m_playlistData(Database::Instance().getPlaylistData(id)),
      m_id(id),
      m_curIndex(0),
      m_curSong(nullptr)
{}

Playlist::~Playlist() {
    delete m_curSong;
}

void Playlist::play() {
    if(!m_curSong &&
        updatePlaylist() == STATUS_BAD) {
        qWarning() << "Playlist::play\t Can't play song. Playlist is empty.";
        return;
    }
    m_curSong->play();
}

void Playlist::pause() {
    if(!m_curSong &&
        updatePlaylist() == STATUS_BAD) {
        qWarning() << "Playlist::pause\t Can't pause song. Playlist is empty.";
        return;
    }
    m_curSong->pause();
}


void Playlist::setSong(ID id) {
    delete m_curSong;
    m_curSong = nullptr;
    m_curSong = new Song(id);
}

void Playlist::setNext() {
    if(!m_curSong &&
        updatePlaylist() == STATUS_BAD) {
        qWarning() << "Playlist::setNext\t Can't play next song. Playlist is empty.";
        return;
    }
    ID curId = m_curSong->getId();
    ID nextId = m_playlistData->getNextId(curId);
    setSong(nextId);
    if(m_curIndex < m_playlistData->getCountOfidentificators() - 1) { //if don't want to go out of vector
        ++m_curIndex;
    }
}
void Playlist::setPrev() {
    if(!m_curSong &&
        updatePlaylist() == STATUS_BAD) {
        qWarning() << "Playlist::setPrev\t Can't play prev song. Playlist is empty.";
        return;
    }
    ID curId = m_curSong->getId();
    ID prevId = m_playlistData->getPrevId(curId);
    setSong(prevId);
    if(m_curIndex > 0) { //if don't want to go out of vector
        --m_curIndex;
    }
}

void Playlist::set(quint32 index) {
    quint32 min = 0;
    quint32 max = m_playlistData->getCountOfidentificators() - 1;
    if(min <= index &&
              index <= max) {
        if(!m_curSong &&
            updatePlaylist() == STATUS_BAD) {
            qWarning() << "Playlist::setPrev\t Can't play prev song. Playlist is empty.";
            return;
        }
        ID nextSongId = m_playlistData->getVectorIdentificators().at(index);
        setSong(nextSongId);
        m_curIndex = index;
    }
}

ID Playlist::getId() {
    return m_id;
}

const QString& Playlist::getTitle() {
    return m_playlistData->getTitle();
}

bool Playlist::updatePlaylist() {
    m_playlistData.reset();
    m_playlistData = Database::Instance().getPlaylistData(m_id);
    const QVector<ID> vectorIdentificators = m_playlistData->getVectorIdentificators();

    if(vectorIdentificators.empty()) {
        qWarning() << "Playlist::updatePlaylist\t Playlist with id = " << m_id << " is empty. Can't get any song.";
        return STATUS_BAD;
    } else {
        ID firstId = vectorIdentificators[0];
        setSong(firstId);
        m_curIndex = 0;
    }
    return STATUS_OK;
}

unsigned int Playlist::getCurIndex() {
    return m_curIndex;
}



