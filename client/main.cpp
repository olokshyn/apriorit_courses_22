#include <QtQml>
#include <QFile>
#include <QTextStream>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QQmlContext>
#include "src/audioplayer.h"
#include "src/ListModel/modelconnection.h"
#include <QApplication>
#include <QtDebug>
#include <QFile>
#include <QTextStream>
#include "src/ListModel/comboboxmodel.h"
#include "src/CacheManager/cacheconnection.h"
#include "src/settings.h"
#include "src/grpcClient/grpcmanager.h"
#include "src/commondefines.h"
#include "src/vlcplayer.h"

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

int main(int argc, char** argv) {

    QApplication app(argc, argv);
    qInstallMessageHandler(messageHandler);
    qRegisterMetaType<ID>("ID");

    qmlRegisterType<AudioPlayer> ("my.AudioPlayer", 1, 0, "AudioPlayer");
    QQmlApplicationEngine engine;

    ModelConnection& modelConnection = ModelConnection::Instance();
    engine.rootContext()->setContextProperty("myMetadataModel", modelConnection.getMetadataModel());
    engine.rootContext()->setContextProperty("AllMetadataModel", modelConnection.getAllMetadata());
    engine.rootContext()->setContextProperty("AlbumsModel", modelConnection.getAlbumsModel());
    engine.rootContext()->setContextProperty("myPlaylistsModel",&(modelConnection.getPlaylistsModel()));
    engine.rootContext()->setContextProperty("cacheAudioManager",&(CacheConnection::Instance()));
    engine.rootContext()->setContextProperty("SettingsObject",&(Settings::Instance()));
    engine.rootContext()->setContextProperty("GrpcManager",&(GrpcManager::Instance()));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));
    return app.exec();
}

void messageHandler(QtMsgType type, const QMessageLogContext &, const QString &msg)
{
    QFile logFile("myprotoc.log");
    logFile.open(QFile::Append | QFile::Text);
    QTextStream out(&logFile);

    out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ");

    switch (type)
    {
    case QtInfoMsg:     out << "INFO\t"; break;
    case QtDebugMsg:    out << "DEBUG\t"; break;
    case QtWarningMsg:  out << "WARNING\t"; break;
    case QtCriticalMsg: out << "CRITICAL\t"; break;
    case QtFatalMsg:    out << "FATAL\t"; break;
    default: break;
    }
    out << ": " << msg << endl;
    out.flush();
    logFile.close();
}
