import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
Window {
    id: mainWindow
    title: "Add song"
    width: 640
    height: 250
    visible: false
    property color textColor: "white"

    onVisibleChanged: {
        if(visible) {
            editTextAudioPath.text = ""
            editTextCoverpath.text = ""
            editTextSongTitle.text = ""
            editTextAlbumTitle.text = ""
            editTextArtist.text = ""
            editTextYear.text = ""
        }
    }

    function closeDialogs() {
        audioFileDialog.close()
        coverFileDialog.close()
    }

    signal signalAddSong(string audiofilePath, string coverfilePath, string songTitle, string album, string artist, int year)
    signal signalAddSongByAlbum(string audiofilePath, string songTitle, int albumId)

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5
        ColumnLayout {
            id: topColumnLayout
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 5
            spacing: 5
            RowLayout { //audiofile
                Button {
                    id: dialogAudioButton
                    text: "Audio file path: "
                    onClicked: {
                        audioFileDialog.open()
                    }
                }
                BasicTextInput {
                    id: editTextAudioPath
                    color: textColor
                    font.pointSize: 12
                    MouseArea {
                        width: topColumnLayout.width
                        height: editTextAudioPath.font.pixelSize
                        onClicked: editTextAudioPath.focus = true
                    }
                }

                FileDialog {
                    id: audioFileDialog
                    title: "Please choose an audio file"
                    folder: shortcuts.home
                    nameFilters: ["Audio files (*.wav *.flac)"]
                    onAccepted: {
                        editTextAudioPath.text = audioFileDialog.fileUrl
                        if(editTextAudioPath.text.search("file://") != -1) {
                            editTextAudioPath.text = editTextAudioPath.text.replace("file://", "")
                        }
                    }
                }
            }
            RowLayout { //song title
                Text {
                    text: "Song title: "
                    color: textColor
                    font.family: appFont.name
                    font.pointSize: 12
                }
                BasicTextInput {
                    id: editTextSongTitle
                    color: textColor
                    font.pointSize: 12
                    MouseArea {
                        width: topColumnLayout.width
                        height: editTextSongTitle.font.pixelSize
                        onClicked: editTextSongTitle.focus = true
                    }
                }
            }
        }
        Item {
            anchors.top: topColumnLayout.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: acceptButton.top
            anchors.margins: 5
            RowLayout { //switcher
                id: switcherRowLayout
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                Switch {
                    id: switcherAlbum
                    anchors.left: parent.left
                    anchors.right: parent.right
                    checked: false
                }
                Text {
                    text: switcherAlbum.checked ? "Enter new album" : "Use an existing album"
                    color: textColor
                    font.family: appFont.name
                    font.pointSize: 12
                }
            }
            RowLayout { //Use an existing album
                visible: switcherAlbum.checked == false
                anchors.top: switcherRowLayout.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                Item {
                    anchors.fill: parent
                    Component {
                        id: listViewAudioComponent
                        Item {
                            width: parent.width
                            height: 25
                            Row {
                                //anchors.centerIn: parent
                                anchors.fill: parent
                                anchors.margins: 5
                                Row {
                                    anchors.verticalCenter: parent.verticalCenter
                                    Text {
                                        color: colors.mainTextColorSongTitle
                                        font.family: appFont.name
                                        font.pointSize: 12
                                        text: "album '" + albumTitle + "', artist '" + artistTitle + "'"
                                    }
                                }
                            }
                        }
                    }
                    ListView {
                        anchors.fill: parent
                        id: listViewOfAlbums
                        clip: true
                        height: 100
                        focus: true
                        model: AlbumsModel
                        delegate: listViewAudioComponent
                        highlight: Rectangle {
                            color: colors.mainFooterPushColor
                            border.color: colors.mainBorderColor
                        }
                        function getCurAlbumId() {
                            return model.get(currentIndex).albumId
                        }
                        MouseArea {
                            anchors.fill: parent
                            onPressed: {
                                listViewOfAlbums.focus = true
                            }
                        }
                    }
                }
            }
            ColumnLayout { //Enter album
                id: columbEnterAlbum
                visible: switcherAlbum.checked == true
                anchors.top: switcherRowLayout.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                RowLayout { //coverfile
                    Button {
                        id: dialogCoverButton
                        text: "Cover file path: "
                        onClicked: {
                            coverFileDialog.open()
                        }
                    }
                    BasicTextInput {
                        id: editTextCoverpath
                        color: textColor
                        font.pointSize: 12
                        MouseArea {
                            width: columbEnterAlbum.width
                            height: editTextCoverpath.font.pixelSize
                            onClicked: editTextCoverpath.focus = true
                        }
                    }
                    FileDialog {
                        id: coverFileDialog
                        title: "Please choose a cover file"
                        folder: shortcuts.home
                        nameFilters: [ "Image files (*.jpg *.png)"]
                        onAccepted: {
                            editTextCoverpath.text = coverFileDialog.fileUrl
                            if(editTextCoverpath.text.search("file://") != -1) {
                                editTextCoverpath.text = editTextCoverpath.text.replace("file://", "")
                            }
                        }
                    }
                }
                RowLayout {
                    Text {
                        text: "Album title: "
                        color: textColor
                        font.family: appFont.name
                        font.pointSize: 12
                    }
                    BasicTextInput {
                        id: editTextAlbumTitle
                        color: textColor
                        font.pointSize: 12
                        MouseArea {
                            width: columbEnterAlbum.width
                            height: editTextAlbumTitle.font.pixelSize
                            onClicked: editTextAlbumTitle.focus = true
                        }
                    }
                }
                RowLayout {
                    Text {
                        text: "Artist: "
                        color: textColor
                        font.family: appFont.name
                        font.pointSize: 12
                    }
                        BasicTextInput {
                            id: editTextArtist
                            //anchors.fill: parent
                            color: textColor
                            font.pointSize: 12
                            MouseArea {
                                width: columbEnterAlbum.width
                                height: editTextArtist.font.pixelSize
                                onClicked: editTextArtist.focus = true
                            }
                        }
                }
                RowLayout {
                    Text {
                        text: "Year: "
                        color: textColor
                        font.family: appFont.name
                        font.pointSize: 12
                    }
                    BasicTextInput {
                        id: editTextYear
                        color: textColor
                        font.pointSize: 12
                        validator : RegExpValidator { regExp : /[0-9]+/ }
                        MouseArea {
                            width: columbEnterAlbum.width
                            height: editTextYear.font.pixelSize
                            onClicked: editTextYear.focus = true
                        }
                    }
                }
            }
        }

        Button {
            id: acceptButton
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            text: "Add"
            enabled: {
                if(switcherAlbum.checked == true) { //enter album
                    if(editTextAudioPath.text != "" &&
                        editTextCoverpath.text != "" &&
                        editTextSongTitle.text != "" &&
                        editTextAlbumTitle.text != "" &&
                        editTextArtist.text != "" &&
                        editTextYear.text != "") {
                        return true;
                    }
                    return false;
                }
                return true;
            }

            onClicked: {
                closeDialogs()
                mainWindow.visible = false
                if(switcherAlbum.checked == true) { //enter album
                    signalAddSong(editTextAudioPath.text, editTextCoverpath.text,
                                   editTextSongTitle.text, editTextAlbumTitle.text,
                                   editTextArtist.text, editTextYear.text)
                } else {
                    signalAddSongByAlbum(editTextAudioPath.text, editTextSongTitle.text,
                                         listViewOfAlbums.getCurAlbumId())
                }

                editTextAudioPath.text = ""
                editTextCoverpath.text = ""
                editTextSongTitle.text = ""
                editTextAlbumTitle.text = ""
                editTextArtist.text = ""
                editTextYear.text = ""
            }
        }
    }
}
