import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import my.AudioPlayer 1.0

ApplicationWindow {
    id: background
    minimumWidth: 800
    minimumHeight: 500
    x: 500
    y: 200
    visible: true
    title: "MusicStream"

    Connections {
        target: GrpcManager
        onLoadStatusChanged: {
            appStatus.text = status
        }
        onResetAppStatus: {
            appStatus.text = ""
        }
    }



        menuBar: MenuBar {
            id: myMenuBar
            property color mainColor: "#282828"
            property color mainPushColor: "#383838"
            property color mainBorderColor: "white"
            property color mainTextColor: "#c1c1c1"

        Menu {
            title: "Songs"
            MenuItem {
                text: "Add song"
                onTriggered: {
                    playLogic.audioPlayer.updateAlbums()
                    addSongWindow.visible = true
                }
            }
        }
        Menu {
            title: "Playlists"
            MenuItem {
                text: "Add playlist"
                onTriggered: {
                    addPlaylistWindow.songsInPlaylist = ""
                    addPlaylistWindow.visible = true
                }
            }
        }
        Menu {
            title: "About"
            MenuItem {
                text: "Settings"
                onTriggered: {
                    settingsWindow.visible = true
                }
            }
        }

            style: MenuBarStyle {

                padding {
                    left: 5
                    right: 5
                    top: 5
                    bottom: 5
                }

                background: Rectangle {
                    id: rect
                    color: myMenuBar.mainColor
                }

                itemDelegate: Rectangle { // the menus
                    implicitWidth: lab.contentWidth * 2
                    implicitHeight: lab.contentHeight * 1.2
                    color: styleData.selected || styleData.open ? myMenuBar.mainPushColor : myMenuBar.mainColor
                    Label {
                        id: lab
                        anchors.centerIn: parent
                        color: myMenuBar.mainTextColor
                        font.wordSpacing: 10
                        text: styleData.text
                    }
                }

                menuStyle: MenuStyle {  // the menus items
                    id: goreStyle

                    frame: Rectangle {

                        color: myMenuBar.mainColor
                        border.color: "#888888"
                    }

                    itemDelegate {
                        background: Rectangle {
                            color:  styleData.selected || styleData.open ? myMenuBar.mainPushColor
                                                                         : myMenuBar.mainColor
                        }

                        label: Label {
                            color: myMenuBar.mainTextColor
                            text: styleData.text
                        }

                        shortcut: Label {
                            color: "#888888"
                            text: styleData.shortcut
                        }
                    }

                    separator: Rectangle {
                        width: parent.width
                        implicitHeight: 1
                        color: "#888888"
                    }
                }
            }
        } //menuBar

    statusBar: StatusBar {
            width: background.width
            height: 30
            ComboBox {
                id: checkPlaylistCombobox
                anchors.fill: parent
                width: parent.width
                model: myPlaylistsModel.comboList
                function getCurrentPlaylist() {
                    return currentText
                }
                function getCurrentPlaylistID() {
                    return playLogic.audioPlayer.getPlaylistIdByTitle(currentText)
                }

                onCurrentIndexChanged: {
                    playLogic.audioPlayer.setPlaylist(getCurrentPlaylistID())
                    listViewOfAudios.currentIndex = 0
                }
                Item { //buttons
                    anchors.right: parent.right
                    height: parent.height - 4
                    width: parent.height * 2
                    Item { //edit button
                        anchors.left: parent.left
                        height: parent.height
                        width: parent.width / 2
                        Image {
                            id: editPlaylist
                            source: checkPlaylistCombobox.getCurrentPlaylist() === "" ? "" : "icons/edit.png"
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            state: "none"
                            MouseArea {
                                anchors.fill: parent
                                onClicked:  {
                                    if(checkPlaylistCombobox.getCurrentPlaylist() !== "") {
                                        editPlaylistWindow.songsInPlaylistAdd = ""
                                        editPlaylistWindow.songsInPlaylistDelete = ""
                                        editPlaylistWindow.playlistId = checkPlaylistCombobox.getCurrentPlaylistID()
                                        editPlaylistWindow.playlistTitle = checkPlaylistCombobox.getCurrentPlaylist()
                                        editPlaylistWindow.visible = true
                                    }
                                }
                            }
                        }
                    }
                    Item { //delete button
                        anchors.right: parent.right
                        height: parent.height
                        width: parent.width / 2
                        Image {
                            id: deletePlaylist
                            source: checkPlaylistCombobox.getCurrentPlaylist() === "" ? "" : "icons/delete.png"
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            state: "none"
                            MouseArea {
                                anchors.fill: parent
                                onClicked:  {
                                    if(checkPlaylistCombobox.getCurrentPlaylist() !== "") {
                                        deletePlaylistDialog.playlistId = checkPlaylistCombobox.getCurrentPlaylistID()
                                        deletePlaylistDialog.visible = true
                                    }
                                }
                            }
                        }
                    }
                }

                style: ComboBoxStyle {
                    background: Rectangle {
                        anchors.fill: parent
                        color: colors.mainFooterColor

                        CustomBorder {
                            commonBorder: false
                            lBorderwidth: 0
                            rBorderwidth: 0
                            tBorderwidth: 2
                            bBorderwidth: 0
                            borderColor: myMenuBar.mainPushColor
                        }
                    }
                    label: Text {
                        color: colors.mainTextColorArtist
                        text: "Playlist: " + checkPlaylistCombobox.currentText
                    }
                    property Component __dropDownStyle: MenuStyle {
                            __maxPopupHeight: 600
                            __menuItemType: "comboboxitem"

                            frame: Rectangle {
                                color: colors.mainFooterColor
                                border.width: 1
                                border.color: colors.mainFooterPushColor
                            }

                            itemDelegate.label: Text {
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                color: colors.mainTextColorArtist
                                text: styleData.text
                            }
                            itemDelegate.background: Rectangle {
                                color: styleData.selected ? colors.mainFooterPushColor
                                                          : colors.mainFooterColor
                                border.width: styleData.selected ? 1 : 0
                                border.color: styleData.selected ? colors.mainBorderColor
                                                                 : colors.mainFooterPushColor
                            }

                            __scrollerStyle: ScrollViewStyle { }
                        }

                }
            } //ComboBox
            style: StatusBarStyle {
                padding {
                    left: 0
                    right: 0
                    top: 0
                    bottom: 0
                }
            }
        } //statusBar


    Item {
        id: colors
        property color mainHeaderColor: "#282828"
        property color mainFooterColor: "#181818"
        property color mainFooterPushColor: "#383838"
        property color mainBorderColor: "white"
        property color mainTextColorSongTitle: "#c1c1c1"
        property color mainTextColorArtist: "#cccccc"
        property color timelineColor: "white"
    }

    Item {
        id: playLogic

        property var audioPlayer: myAudioPlayer

        AudioPlayer {
            id: myAudioPlayer
            onPlayerCurrentItemChanged: {
                listViewOfAudios.currentIndex = newIndex
            }
        }

        function makeAlbumAndYearLabel(album, year) {
            return "(" + album + ", " + year  + ")"
        }

        function currentListIsEmpty() {
            return listViewOfAudios.count === 0
        }

        function playingAudio() {
            if(!currentListIsEmpty()) {
                if(playLogic.audioPlayer.isPlaying()) {
                    playLogic.audioPlayer.pause()
                    playPause.source = "icons/play.png"
                } else {
                    playLogic.audioPlayer.play()
                    playPause.source = "icons/pause.png"
                }
            }
        }

        function next() {
            audioPlayer.playNext()
        }

        function previous(){
            audioPlayer.playPrev()
        }

        function msToTime(duration) {
            var seconds = parseInt((duration/1000)%60);
            var minutes = parseInt((duration/(1000*60))%60);

            minutes = (minutes < 10) ? "0" + minutes : minutes;
            seconds = (seconds < 10) ? "0" + seconds : seconds;

            return minutes + ":" + seconds;
        }
    }

    FontLoader {
        id: appFont
        name: "OpenSans-Regular"
        source: "fonts/OpenSans-Regular.ttf"
    }


    AddSongWindow {
        id: addSongWindow
        x: background.x + background.width / 4
        y: background.y + background.height / 4
        textColor: colors.mainTextColorArtist
        color: colors.mainHeaderColor
        visible: false
        onSignalAddSong: {
            playLogic.audioPlayer.addSong(audiofilePath, coverfilePath, songTitle, album, artist, year)
        }
        onSignalAddSongByAlbum: {
            playLogic.audioPlayer.addSongByAlbum(audiofilePath, songTitle, albumId)
        }
    }

    AddPlaylistWindow {
        id: addPlaylistWindow
        x: background.x + background.width / 4
        y: background.y + background.height / 4
        textColor: colors.mainTextColorArtist
        color: colors.mainHeaderColor
        visible: false
        onSignalAddPlaylist: {
            playLogic.audioPlayer.addPlaylist(playlistTitle, songsIdStr)
        }
    }

    EditSongWindow {
        id: changeSongWindow
        x: background.x + background.width / 4
        y: background.y + background.height / 4
        textColor: colors.mainTextColorArtist
        color: colors.mainHeaderColor
        visible: false
        onSignalEditSong: {
            playLogic.audioPlayer.editSong(songId, newYear, newSongTitle, newAlbum, newArtist, audiofilePath, coverfilePath)
        }
    }

    EditPlaylistWindow {
        id: editPlaylistWindow
        x: background.x + background.width / 4
        y: background.y + background.height / 4
        textColor: colors.mainTextColorArtist
        color: colors.mainHeaderColor
        visible: false
        onSignalPlaylistEdited: {
            playLogic.audioPlayer.editPlaylist(playlistId, newTitle, songsForAdd, songsForDelete)
        }
    }

    DeleteSongWindow {
        id: deleteSongDialog
        x: background.x + background.width / 4
        y: background.y + background.height / 4
        textColor: colors.mainTextColorArtist
        color: colors.mainHeaderColor
        visible: false
        onSignalDeleteSong: {
            playLogic.audioPlayer.deleteSong(songId)
        }
    }

    DeletePlaylistWindow {
        id: deletePlaylistDialog
        x: background.x + background.width / 4
        y: background.y + background.height / 4
        textColor: colors.mainTextColorArtist
        color: colors.mainHeaderColor
        visible: false
        onSignalDeletePlaylist: {
            playLogic.audioPlayer.deletePlaylist(playlistId)
        }
    }

    SettingsWindow {
        id: settingsWindow
        x: background.x + background.width / 4
        y: background.y + background.height / 4
        textColor: colors.mainTextColorArtist
        color: colors.mainHeaderColor
        visible: false
    }

    onClosing: {
        addSongWindow.close()
        addPlaylistWindow.close()
        changeSongWindow.close()
        editPlaylistWindow.close()
        deleteSongDialog.close()
        deletePlaylistDialog.close()
        settingsWindow.close()
    }

    Item {
        id: mainItem
        anchors.fill: parent
        width: parent.width
        height: parent.height

            Rectangle {
                id: headerRectangle
                anchors.top: mainItem.top
                anchors.left: mainItem.left
                anchors.right: mainItem.right
                color: colors.mainHeaderColor
                width: mainItem.width
                height: 150

                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    onClicked: {
                        if (mouse.button === Qt.RightButton)
                            contextMenuCurrentSong.popup()
                    }
                    onPressAndHold: {
                        if (mouse.source === Qt.MouseEventNotSynthesized)
                            contextMenuCurrentSong.popup()
                    }

                    Menu {
                        id: contextMenuCurrentSong
                        MenuItem {
                            id: contextmenuEditCurrentSong
                            text: "Edit song"
                            enabled: true
                            onTriggered: {
                                if(!playLogic.currentListIsEmpty()) {
                                    changeSongWindow.songId = listViewOfAudios.getSongId()
                                    changeSongWindow.editSongTitle = listViewOfAudios.getSongTitle()
                                    changeSongWindow.editArtist = listViewOfAudios.getArtistTitle()
                                    changeSongWindow.editAlbum = listViewOfAudios.getAlbumTitle()
                                    changeSongWindow.editYear = listViewOfAudios.getYear()
                                    changeSongWindow.visible = true
                                }
                            }
                        }
                        MenuItem {
                            id: contextmenuDeleteCurrentSong
                            text:  "Delete song"
                            enabled: true
                            onTriggered: {
                                if(!playLogic.currentListIsEmpty()) {
                                    deleteSongDialog.songId = listViewOfAudios.getSongId()
                                    deleteSongDialog.visible = true
                                }
                            }
                        }
                    }
                }

                ColumnLayout{
                    id: container
                    anchors.centerIn: parent
                    width: parent.width - 20
                    height: parent.height - 5


                    RowLayout {
                        id: wrapper
                        anchors.fill: parent

                        Rectangle { //cover
                            id: leftWapper
                            height: 126
                            width: 126
                            radius: 7

                            BorderImage {
                                id: coverBorder
                                source: "gfx/cover_overlay.png"
                                anchors.fill: parent
                                anchors.margins: 4
                                border { left: 10; top: 10; right: 10; bottom: 10 }
                                horizontalTileMode: BorderImage.Stretch
                                verticalTileMode: BorderImage.Stretch

                                Image {
                                    id: coverPic
                                    source: "gfx/cover.png"
                                    anchors.fill: coverBorder
                                    anchors.margins: 2
                                    fillMode: Image.PreserveAspectFit
                                    cache: false
                                }
                            }

                        }

                        ColumnLayout {
                            id: rightWapper
                            Layout.fillWidth: true
                            Layout.fillHeight: true

                            Text {
                                id: appStatus
                                color: colors.mainTextColorSongTitle
                                font.family: appFont.name
                                font.pointSize: 12
                            }

                            RowLayout { //buttons + text
                                id: upperWrap
                                Layout.fillWidth: true
                                Layout.preferredHeight: 100
                                Layout.leftMargin: 20
                                spacing: 25

                                Image {
                                    id: prevTrack
                                    source: "icons/rewind.png"
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.leftMargin: 20
                                    state: "none"
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: playLogic.previous()
                                        onPressed: prevTrack.state = "pressed"
                                        onReleased: prevTrack.state = "none"
                                    }
                                    states: State {
                                        name: "pressed"
                                        when: mouseArea.pressed
                                        PropertyChanges { target: prevTrack; scale: 0.8 }
                                    }
                                    transitions: Transition {
                                        NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
                                    }
                                }

                                Rectangle{
                                    width: 30
                                    anchors.verticalCenter: parent.verticalCenter
                                    Image {
                                        id: playPause
                                        source: "icons/play.png"
                                        anchors.verticalCenter: parent.verticalCenter
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        state: "none"
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: playLogic.playingAudio()
                                            onPressed: playPause.state = "pressed"
                                            onReleased: playPause.state = "none"
                                        }
                                        states: State {
                                            name: "pressed"
                                            when: mouseArea.pressed
                                            PropertyChanges { target: playPause; scale: 0.8 }
                                        }
                                        transitions: Transition {
                                            NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
                                        }
                                    }
                                }

                                Image {
                                    id: nextTrack
                                    source: "icons/forward.png"
                                    anchors.verticalCenter: parent.verticalCenter
                                    state: "none"

                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: playLogic.next()
                                        onPressed: nextTrack.state = "pressed"
                                        onReleased: nextTrack.state = "none"
                                    }
                                    states: State {
                                        name: "pressed"
                                        when: mouseArea.pressed
                                        PropertyChanges { target: nextTrack; scale: 0.8 }
                                    }
                                    transitions: Transition {
                                        NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
                                    }
                                }

                                Item { //text
                                    Layout.fillWidth: true
                                        ColumnLayout {
                                            anchors.verticalCenter: parent.verticalCenter
                                            Layout.fillWidth: true
                                                Text {
                                                    id: headersongTitle
                                                    color: colors.mainTextColorSongTitle
                                                    font.family: appFont.name
                                                    font.pointSize: 17
                                                    font.bold: true
                                                    style: Text.Raised
                                                    styleColor: "#111111"
                                                    wrapMode: Text.Wrap
                                                }
                                                Text {
                                                    id: headersongArtist
                                                    color: colors.mainTextColorArtist
                                                    font.family: appFont.name
                                                    font.pointSize: 15
                                                    font.bold: true
                                                    style: Text.Raised
                                                    styleColor: "#111111"
                                                    wrapMode: Text.Wrap
                                                }
                                                Text {
                                                    id: headersongAlbumAndYear
                                                    color: colors.mainTextColorArtist
                                                    font.family: appFont.name
                                                    font.pointSize: 10
                                                    font.bold: true
                                                    style: Text.Raised
                                                    styleColor: "#111111"
                                                    wrapMode: Text.Wrap
                                                }
                                        }
                                }
                                Item {
                                    Layout.fillWidth: true
                                    ColumnLayout {
                                        anchors.verticalCenter: parent.verticalCenter
                                        anchors.right: parent.right
                                        Layout.fillWidth: true
                                        Item {
                                            width: 150
                                            height: 100

                                            RowLayout {
                                                anchors.right: parent.right
                                                anchors.top: parent.top

                                                Image {
                                                    id: syncButton
                                                    source: "icons/sync.png"
                                                    anchors.verticalCenter: parent.verticalCenter
                                                    state: "none"

                                                    MouseArea {
                                                        anchors.fill: parent
                                                        onClicked: playLogic.audioPlayer.sync()
                                                        onPressed: syncButton.state = "pressed"
                                                        onReleased: syncButton.state = "none"
                                                    }
                                                    states: State {
                                                        name: "pressed"
                                                        when: mouseArea.pressed
                                                        PropertyChanges { target: syncButton; scale: 0.8 }
                                                    }
                                                    transitions: Transition {
                                                        NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
                                                    }
                                                }

                                            } //RowLayout

                                            RowLayout {
                                                anchors.right: parent.right
                                                anchors.bottom: parent.bottom
                                                Image {
                                                    anchors.verticalCenter: parent.verticalCenter
                                                    source: "icons/volumeOff.png"
                                                    scale: 0.75
                                                }
                                                Slider {
                                                    id: volumeSlider
                                                    anchors.verticalCenter: parent.verticalCenter
                                                    minimumValue: 0
                                                    maximumValue: 100
                                                    stepSize: 1
                                                    value: playLogic.audioPlayer.getVolume()
                                                    updateValueWhileDragging: true
                                                    onValueChanged: playLogic.audioPlayer.setVolume(volumeSlider.value)
                                                    style: SliderStyle {
                                                        groove: Rectangle {
                                                            implicitWidth: 100
                                                            implicitHeight: 5
                                                            color: colors.mainTextColorArtist
                                                            radius: 8
                                                        }
                                                        handle: Rectangle {
                                                            anchors.centerIn: parent
                                                             color: control.pressed ? colors.mainFooterPushColor : colors.mainFooterColor
                                                            border.color: colors.mainBorderColor
                                                            border.width: 2
                                                            implicitWidth: 25
                                                            implicitHeight: 25
                                                            radius: 12
                                                        }
                                                    }
                                                }
                                                Image {
                                                    anchors.verticalCenter: parent.verticalCenter
                                                    source: "icons/volumeOn.png"
                                                    scale: 0.75
                                                }
                                            } //RowLayout
                                        } //item
                                    } //ColumnLayout
                                }
                            } //rowlayout (buttons + text)

                            RowLayout { //slider
                                id: lowerWrap
                                Layout.fillWidth: true
                                Layout.preferredHeight: 40
                                Layout.leftMargin: 20
                                spacing: 15

                                Text {
                                    id: audioSliderPositionText
                                    text: "00:00"
                                    font.family: appFont.name
                                    color: "#dedede"
                                    font.pointSize: 18
                                }
                                ColumnLayout {

                                    Slider{
                                        property int timelineKoef: 0
                                        id: audioSlider
                                        Layout.fillWidth: true
                                        value: 0
                                        maximumValue: 0
                                        function resetSliderTimeline() {
                                            for(var i = 0; i < itemRepeater.width; ++i) {
                                                audioSliderRepeater.itemAt(i).color = colors.mainHeaderColor
                                            }
                                        }
                                        Connections {
                                            target: cacheAudioManager
                                            onSetCachedSections: {
                                                for(var i = startPosition; i < endPosition; ++i) {
                                                    audioSliderRepeater.itemAt(i / audioSlider.timelineKoef).color = colors.timelineColor
                                                }
                                            }
                                            onSetPositionOnSlider: {
                                                audioSlider.value = newPosition
                                                audioSliderPositionText.text = playLogic.msToTime(newPosition)
                                            }
                                            onSetDurationOnSlider: {
                                                audioSlider.maximumValue = newDuration
                                                audioSliderDurationText.text = playLogic.msToTime(newDuration)
                                                audioSlider.timelineKoef = newDuration / itemRepeater.width
                                                audioSlider.resetSliderTimeline()
                                            }
                                            onEndOfAudio: {
                                                if((listViewOfAudios.currentIndex + 1) != listViewOfAudios.count) { //if not last song in playlist
                                                    playLogic.next()
                                                    playLogic.playingAudio()
                                                } else {
                                                    playPause.source = "icons/play.png"
                                                }
                                            }
                                        }

                                        MouseArea {
                                            anchors.fill: parent
                                            width: parent.width
                                            onClicked: {
                                                if(!playLogic.currentListIsEmpty()) {
                                                    cacheAudioManager.sliderPositionChangedByClick(
                                                            audioSlider.maximumValue * mouse.x / width)
                                                }
                                            }
                                        }

                                    }
                                    Rectangle {
                                        id: itemRepeater
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        height: 5
                                        width: audioSlider.width - 3
                                        Repeater {
                                            id: audioSliderRepeater
                                            model: parent.width
                                            Rectangle{
                                                width: 1
                                                height: parent.height
                                                x: index
                                                color: colors.mainHeaderColor
                                            }
                                        }
                                    }
                                }

                                Text {
                                    id: audioSliderDurationText
                                    text: "00:00"
                                    font.family: appFont.name
                                    color: "#dedede"
                                    font.pointSize: 18
                                }
                            } //lowerwrap (slider)

                        } //ColumnLayout rightwrapper

                    } //RowLayout wrapper

                } //ColumnLayout container

            } //header Rectangle

            Rectangle {

                id: footerRectangle
                color: colors.mainFooterColor
                width: mainItem.width
                height: mainItem.height - headerRectangle.height
                anchors.left: mainItem.left
                anchors.right: mainItem.right
                anchors.bottom: mainItem.bottom

                Component {
                    id: listViewComponent

                    Item {
                        width: parent.width
                        height: 50
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if(listViewOfAudios.currentIndex != index) {
                                    listViewOfAudios.currentIndex = index
                                    playLogic.audioPlayer.setCurIndex(index)
                                }
                            }
                        }

                        Row {
                            anchors.fill: parent
                            anchors.margins: 5
                            spacing: 10
                            Image {
                                width: 40
                                height: 40
                                source: coverfile
                                smooth: true
                                fillMode: Image.PreserveAspectFit
                                cache: false
                            }

                            Row {
                                anchors.verticalCenter: parent.verticalCenter
                                spacing: 5
                                Text {
                                    color: colors.mainTextColorSongTitle
                                    font.family: appFont.name
                                    font.pointSize: 15
                                    font.bold: true
                                    style: Text.Raised
                                    styleColor: "#111111"
                                    wrapMode: Text.Wrap
                                    text: songTitle
                                }
                                Text {
                                    color: colors.mainTextColorArtist
                                    font.family: appFont.name
                                    font.pointSize: 12
                                    font.bold: true
                                    style: Text.Raised
                                    styleColor: "#111111"
                                    wrapMode: Text.Wrap
                                    text: artistTitle
                                }
                            }
                        }
                    }
                }

                ListView {
                    id: listViewOfAudios
                    anchors.fill: footerRectangle
                    focus: true
                    highlight: Rectangle {
                        width: parent.width
                        color: colors.mainFooterPushColor
                        border.color: colors.mainBorderColor
                    }
                    model: myMetadataModel
                    delegate: listViewComponent
                    clip: true
                    Keys.onDownPressed: {
                        playLogic.next()
                    }
                    Keys.onUpPressed: {
                        playLogic.previous()
                    }
                    Keys.onEnterPressed:  {
                        playLogic.playingAudio()
                    }
                    Keys.onSpacePressed: {
                        playLogic.playingAudio()
                    }
                    onCurrentItemChanged: {
                        headersongTitle.text = getSongTitle()
                        headersongArtist.text = getArtistTitle()
                        headersongAlbumAndYear.text = playLogic.makeAlbumAndYearLabel(getAlbumTitle(), getYear())
                        coverPic.source = ""
                        coverPic.source = getCoverfile()
                    }
                    function getSongId() {
                        return model.get(currentIndex).songId
                    }
                    function getSongTitle() {
                        return model.get(currentIndex).songTitle
                    }
                    function getArtistId() {
                        return model.get(currentIndex).artistId
                    }
                    function getArtistTitle() {
                        return model.get(currentIndex).artistTitle
                    }
                    function getAlbumId() {
                        return model.get(currentIndex).albumId
                    }
                    function getAlbumTitle() {
                        return model.get(currentIndex).albumTitle
                    }
                    function getYear() {
                        return model.get(currentIndex).year
                    }
                    function getCoverfile() {
                        return model.get(currentIndex).coverfile
                    }
                }
            } //footer rectangle

        } //mainItem


}
