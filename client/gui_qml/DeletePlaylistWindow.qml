import QtQuick 2.9
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3

Window {
    id: mainWindow
    width: 400
    height: 100
    visible: false
    title: "Deleting a playlist"
    property color textColor: "white"
    property int playlistId: 0

    signal signalDeletePlaylist()

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 5
        spacing: 10
        Text {
            text: "Are you sure you want to delete this playlist?"
            color: textColor
            font.family: appFont.name
            font.pointSize: 12
        }
        RowLayout {
            anchors.right: parent.right
            spacing: 10
            Button {
                text: "Yes, I'm sure"
                onClicked: {
                    mainWindow.visible = false
                    signalDeletePlaylist()
                }
            }
            Button {
                text: "Cancel"
                onClicked: {
                    mainWindow.visible = false
                }
            }
        }
    }
}
