import QtQuick 2.9
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import Qt.labs.platform 1.0

Window {
    id: mainWindow
    width: 640
    height: 300
    visible: false
    title: "Settings"
    property color textColor: "white"
    property string dateLastSync
    property string pathToAudioFiles
    property string pathToCoverFiles
    property string serverIpAndPort
    property string audioMillisecPart
    property string bytesPortionToServer

    onVisibleChanged: {
        if(visible) {
            mainWindow.dateLastSync = SettingsObject.getDateLastSync()
            mainWindow.pathToAudioFiles = SettingsObject.getPathToAudiofiles()
            mainWindow.pathToCoverFiles = SettingsObject.getPathToCoverfiles()
            mainWindow.serverIpAndPort = SettingsObject.getServerIpAndPort()
            mainWindow.audioMillisecPart = SettingsObject.getAudioMillisecPart()
            mainWindow.bytesPortionToServer = SettingsObject.getGrpcBytesPortion()
        }
    }

    function closeDialogs() {
        folderAudioDialog.close()
        folderCoverDialog.close()
    }

    ColumnLayout {
        id: mainColumnLayout
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        ColumnLayout {
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 10

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                color: textColor
                font.pointSize: 15
                text: "Client part"
            }

            RowLayout { //audiofiles folder
                Button {
                    id: folderDialogAudioButton
                    text: "Audio cache path: "
                    onClicked: {
                        folderAudioDialog.open()
                    }
                }
                BasicTextInput {
                    id: editTextAudioPath
                    color: textColor
                    font.pointSize: 12
                    text: pathToAudioFiles
                    MouseArea {
                        width: mainColumnLayout.width
                        height: editTextAudioPath.font.pixelSize
                        onClicked: editTextAudioPath.focus = true
                    }
                }
                FolderDialog {
                    id: folderAudioDialog
                    title: "Please choose an audio cache folder"
                    folder: "file://" + pathToAudioFiles
                    onAccepted: {
                        editTextAudioPath.text = folderAudioDialog.folder
                        if(editTextAudioPath.text.search("file://") != -1) {
                            editTextAudioPath.text = editTextAudioPath.text.replace("file://", "") + "/"
                        }
                    }
                }
            }
            RowLayout { //coverfiles folder
                Button {
                    id: folderDialogCoverButton
                    text: "Cover files path: "
                    onClicked: {
                        folderCoverDialog.open()
                    }
                }
                BasicTextInput {
                    id: editTextCoverPath
                    color: textColor
                    font.pointSize: 12
                    text: pathToCoverFiles
                    MouseArea {
                        width: mainColumnLayout.width
                        height: editTextCoverPath.font.pixelSize
                        onClicked: editTextCoverPath.focus = true
                    }
                }
                FolderDialog {
                    id: folderCoverDialog
                    title: "Please choose an audio files folder"
                    folder: "file://" + pathToCoverFiles
                    onAccepted: {
                        editTextCoverPath.text = folderCoverDialog.folder
                        if(editTextCoverPath.text.search("file://") != -1) {
                            editTextCoverPath.text = editTextCoverPath.text.replace("file://", "") + "/"
                        }
                    }
                }
            }

            RowLayout {
                Text {
                    text: "Server IP and port: "
                    color: textColor
                    font.family: appFont.name
                    font.pointSize: 12
                }
                BasicTextInput {
                    id: editTextIpAndPort
                    color: textColor
                    font.pointSize: 12
                    text: serverIpAndPort
                    MouseArea {
                        width: mainColumnLayout.width
                        height: editTextIpAndPort.font.pixelSize
                        onClicked: editTextIpAndPort.focus = true
                    }
                }
            }

        }

        ColumnLayout {
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 10

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                color: textColor
                font.pointSize: 15
                text: "Server part"
            }

            RowLayout {
                Text {
                    text: "Getting audio part: "
                    color: textColor
                    font.family: appFont.name
                    font.pointSize: 12
                }
                BasicTextInput {
                    id: editTextAudioPart
                    color: textColor
                    font.pointSize: 12
                    text: audioMillisecPart
                    validator : RegExpValidator { regExp : /[0-9]+/ }
                    MouseArea {
                        width: mainColumnLayout.width
                        height: editTextAudioPart.font.pixelSize
                        onClicked: editTextAudioPart.focus = true
                    }
                }
                Text {
                    text: "ms"
                    color: textColor
                    font.family: appFont.name
                    font.pointSize: 12
                }
            }

            RowLayout {
                Text {
                    text: "Sending data packet size: "
                    color: textColor
                    font.family: appFont.name
                    font.pointSize: 12
                }
                BasicTextInput {
                    id: editTextBytesPortion
                    color: textColor
                    font.pointSize: 12
                    text: bytesPortionToServer
                    validator : RegExpValidator { regExp : /[0-9]+/ }
                    MouseArea {
                        width: mainColumnLayout.width
                        height: editTextBytesPortion.font.pixelSize
                        onClicked: editTextBytesPortion.focus = true
                    }
                }
                Text {
                    text: "bytes"
                    color: textColor
                    font.family: appFont.name
                    font.pointSize: 12
                }
            }
        }

        RowLayout {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            Text {
                anchors.left: parent.left
                text: "Date of last sync: " + dateLastSync
                color: textColor
                font.family: appFont.name
                font.pointSize: 10
            }

            RowLayout {
                anchors.right: parent.right
                spacing: 10

                Button {
                    text: "Save"
                    onClicked: {
                        closeDialogs()
                        mainWindow.visible = false
                        if(editTextAudioPath.text != pathToAudioFiles) {
                            SettingsObject.setPathToAudiofiles(editTextAudioPath.text)
                        }
                        if(editTextCoverPath.text != pathToCoverFiles) {
                            SettingsObject.setPathToCoverfiles(editTextCoverPath.text)
                        }
                        if(editTextIpAndPort.text != serverIpAndPort) {
                            SettingsObject.setServerIpAndPort(editTextIpAndPort.text)
                        }
                        if(editTextAudioPart.text != audioMillisecPart) {
                            SettingsObject.setAudioMillisecPart(editTextAudioPart.text)
                        }
                        if(editTextBytesPortion.text != bytesPortionToServer) {
                            SettingsObject.setGrpcBytesPortion(editTextBytesPortion.text)
                        }
                    }
                }
                Button {
                    text: "Cancel"
                    onClicked: {
                        closeDialogs()
                        mainWindow.visible = false
                    }
                }
            }

        }


    }


}
