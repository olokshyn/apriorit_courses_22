import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Window {
    id: mainWindow
    title: "Create playlist"
    width: 500
    height: 210
    visible: false
    property color textColor: "white"
    property string songsInPlaylist: ""

    onVisibleChanged: {
        if(visible) {
            editTextPlaylistTitle.text = ""
        }
    }

    signal signalAddPlaylist(string playlistTitle, string songsIdStr)

    ColumnLayout {
        id: topColumnLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 10

        RowLayout {
            id: rowLayoutTitle
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            Text {
                text: "Playlist title: "
                color: textColor
                font.family: appFont.name
                font.pointSize: 15
            }
            BasicTextInput {
                id: editTextPlaylistTitle
                color: textColor
                MouseArea {
                    width: topColumnLayout.width
                    height: editTextPlaylistTitle.font.pixelSize
                    onClicked: editTextPlaylistTitle.focus = true
                }
            }
        }

        ScrollView {
            id: scrollViewSongs
            anchors.top: rowLayoutTitle.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            width: mainWindow.width
            height: mainWindow.height - rowLayoutTitle.height - acceptButton.height - 10
            Column {

                Repeater {
                    id: repeaterPlaylist
                    anchors.fill: parent
                    width: parent.width
                    height: parent.height
                    model: AllMetadataModel
                    delegate: RowLayout {
                        spacing: 5
                        CheckBox {
                            id: checkboxPlaylist
                            checked: false
                            onClicked: {
                                if(checked === true) {
                                    if(songsInPlaylist.search(songId + ";") === -1) {
                                            songsInPlaylist += songId + ";"
                                    }
                                } else {
                                    if(songsInPlaylist.search(songId + ";") !== -1) {
                                        songsInPlaylist = songsInPlaylist.replace(songId + ";", "")
                                    }
                                }
                            }
                        } //checkbox
                        Text {
                            text: songTitle + " " + artistTitle
                            color: textColor
                        }
                    }
                }
            }
        }


        Button {
            anchors.top: scrollViewSongs.bottom
            anchors.right: parent.right
            id: acceptButton
            text: "Create"
            enabled: {
                if(editTextPlaylistTitle.text != "") {
                    return true
                }
                return false
            }
            onClicked: {
                mainWindow.visible = false
                signalAddPlaylist(editTextPlaylistTitle.text, songsInPlaylist)
            }
        }
    }
}
