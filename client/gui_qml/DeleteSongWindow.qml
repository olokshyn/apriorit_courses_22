import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3

Window {
    id: mainWindow
    width: 400
    height: 100
    visible: false
    title: "Deleting a song"
    property color textColor: "white"
    property int songId: 0

    signal signalDeleteSong()

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 5
        spacing: 10
        Text {
            text: "Are you sure you want to delete this song?"
            color: textColor
            font.family: appFont.name
            font.pointSize: 12
        }
        RowLayout {
            anchors.right: parent.right
            spacing: 10
            Button {
                text: "Yes, I'm sure"
                onClicked: {
                    mainWindow.visible = false
                    signalDeleteSong()
                }
            }
            Button {
                text: "Cancel"
                onClicked: {
                    mainWindow.visible = false
                }
            }
        }
    }
}
