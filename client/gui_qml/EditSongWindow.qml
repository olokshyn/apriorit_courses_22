import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Window {
    title: "Edit song"
    id: mainWindow
    width: 500
    height: 250
    property color textColor: "white"
    property int    songId: 0
    property string editSongTitle: ""
    property string editArtist: ""
    property string editAlbum: ""
    property int editYear: 0

    signal signalEditSong(string newSongTitle, string newAlbum, string newArtist,
                          int newYear, string audiofilePath, string coverfilePath)

    ColumnLayout {
        id: topColumnLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 10
        RowLayout { //set new audiofile
            Button {
                id: dialogAudioButton
                text: "Set new audio file: "
                onClicked: {
                    audioFileDialog.open()
                }
            }
            BasicTextInput {
                id: editTextAudioPath
                color: textColor
                font.pointSize: 12
                MouseArea {
                    width: topColumnLayout.width
                    height: editTextAudioPath.font.pixelSize
                    onClicked: editTextAudioPath.focus = true
                }
            }

            FileDialog {
                id: audioFileDialog
                title: "Please choose an audio file"
                folder: shortcuts.home
                nameFilters: ["Audio files (*.wav *.flac)"]
                onAccepted: {
                    editTextAudioPath.text = audioFileDialog.fileUrl
                    if(editTextAudioPath.text.search("file://") != -1) {
                        editTextAudioPath.text = editTextAudioPath.text.replace("file://", "")
                    }
                }
            }
        }
        RowLayout { //set new audiofile
            Button {
                id: dialogCoverButton
                text: "Set new cover file: "
                onClicked: {
                    coverFileDialog.open()
                }
            }
            BasicTextInput {
                id: editTextCoverPath
                color: textColor
                font.pointSize: 12
                MouseArea {
                    width: topColumnLayout.width
                    height: editTextCoverPath.font.pixelSize
                    onClicked: editTextCoverPath.focus = true
                }
            }

            FileDialog {
                id: coverFileDialog
                title: "Please choose an audio file"
                folder: shortcuts.home
                nameFilters: ["Image files (*.jpg *.png)"]
                onAccepted: {
                    editTextCoverPath.text = coverFileDialog.fileUrl
                    if(editTextCoverPath.text.search("file://") != -1) {
                        editTextCoverPath.text = editTextCoverPath.text.replace("file://", "")
                    }
                }
            }
        }
        RowLayout {
            Text {
                text: "Song title: "
                color: textColor
                font.family: appFont.name
                font.pointSize: 15
            }
            BasicTextInput {
                id: editTextSongTitle
                color: textColor
                text: editSongTitle
                MouseArea {
                    width: topColumnLayout.width
                    height: editTextSongTitle.font.pixelSize
                    onClicked: editTextSongTitle.focus = true
                }
            }
        }
        RowLayout {
            Text {
                text: "Artist: "
                color: textColor
                font.family: appFont.name
                font.pointSize: 15
            }
            BasicTextInput {
                id: editTextArtist
                color: textColor
                text: editArtist
                MouseArea {
                    width: topColumnLayout.width
                    height: editTextArtist.font.pixelSize
                    onClicked: editTextArtist.focus = true
                }
            }
        }
        RowLayout {
            Text {
                text: "Album title: "
                color: textColor
                font.family: appFont.name
                font.pointSize: 15
            }
            BasicTextInput {
                id: editTextAlbum
                color: textColor
                text: editAlbum
                MouseArea {
                    width: topColumnLayout.width
                    height: editTextAlbum.font.pixelSize
                    onClicked: editTextAlbum.focus = true
                }
            }
        }
        RowLayout {
            Text {
                text: "Album year: "
                color: textColor
                font.family: appFont.name
                font.pointSize: 15
            }
            BasicTextInput {
                id: editTextYear
                color: textColor
                text: editYear
                validator : RegExpValidator { regExp : /[0-9]+/ }
                MouseArea {
                    width: topColumnLayout.width
                    height: editTextYear.font.pixelSize
                    onClicked: editTextYear.focus = true
                }
            }
        }
        Button {
            id: acceptButton
            anchors.right: parent.right
            text: "Save"
            enabled: {
                if(editTextSongTitle.text != "" &&
                    editTextAlbum.text != "" &&
                    editTextArtist.text != "" &&
                    editTextYear.text != "") {
                    return true
                }
                return false
            }

            onClicked: {
                mainWindow.visible = false
                signalEditSong(editTextSongTitle.text, editTextAlbum.text, editTextArtist.text,
                               editTextYear.text, editTextAudioPath.text, editTextCoverPath.text)
            }
        }
    }
}
