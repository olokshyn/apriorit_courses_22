import QtQuick 2.9

TextInput {
    id: root
    text: ""
    color: "white"
    font.family: appFont.name
    font.pointSize: 15
}
