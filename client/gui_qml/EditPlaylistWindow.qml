import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4

Window {
    id: mainWindow
    width: 500
    height: 210
    visible: false
    title: "Edit playlist"
    property color textColor: "white"
    property int playlistId: 0
    property string playlistTitle: ""

    property string songsInPlaylistAdd: ""
    property string songsInPlaylistDelete: ""


    signal signalPlaylistEdited(int playlistId, string newTitle, string songsForAdd, string songsForDelete)

    ColumnLayout {
        id: topColumnLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5
            RowLayout {
                id: rowLayoutTitle
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                Text {
                    text: "Playlist title: "
                    color: textColor
                    font.family: appFont.name
                    font.pointSize: 15
                }
                BasicTextInput {
                    id: editTextPlaylistTitle
                    color: textColor
                    text: playlistTitle
                    MouseArea {
                        width: topColumnLayout.width
                        height: editTextPlaylistTitle.font.pixelSize
                        onClicked: editTextPlaylistTitle.focus = true
                    }
                }
            }

            ScrollView {
                id: scrollViewSongs
                anchors.top: rowLayoutTitle.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                width: mainWindow.width
                height: mainWindow.height - rowLayoutTitle.height - acceptButton.height - 10
                Column {

                    Repeater {
                        id: repeaterPlaylist
                        anchors.fill: parent
                        width: parent.width
                        height: parent.height
                        model: AllMetadataModel
                        delegate: RowLayout {
                            function inCurPlaylist(sngid) {
                                for(var i = 0; i < myMetadataModel.count; i++) {
                                    if(myMetadataModel.get(i).songId === sngid) {
                                        return true;
                                    }
                                }
                                return false;
                            }
                            spacing: 5
                            CheckBox {
                                checked: inCurPlaylist(songId)
                                onClicked: {
                                    if(checked === true) {
                                        if(inCurPlaylist(songId) === false) {
                                            if(songsInPlaylistAdd.search(songId + ";") === -1) {
                                                    songsInPlaylistAdd += songId + ";"
                                            }
                                        }
                                        if(songsInPlaylistDelete.search(songId + ";") !== -1) {
                                            songsInPlaylistDelete = songsInPlaylistDelete.replace(songId + ";", "")
                                        }
                                    } else {
                                        if(inCurPlaylist(songId) === true) {
                                            if(songsInPlaylistDelete.search(songId + ";") === -1) {
                                                    songsInPlaylistDelete += songId + ";"
                                            }
                                        }
                                        if(songsInPlaylistAdd.search(songId + ";") !== -1) {
                                            songsInPlaylistAdd = songsInPlaylistAdd.replace(songId + ";", "")
                                        }
                                    }
                                }
                            } //checkbox
                            Text {
                                text: songTitle + " " + artistTitle
                                color: textColor
                            }
                        }
                    }
                }
            }



        Button {
            id: acceptButton
            anchors.top: scrollViewSongs.bottom
            anchors.right: parent.right
            text: "Save"
            enabled: {
                if(editTextPlaylistTitle.text != "") {
                    return true
                }
                return false
            }
            onClicked: {
                mainWindow.visible = false
                signalPlaylistEdited(playlistId, editTextPlaylistTitle.text, songsInPlaylistAdd, songsInPlaylistDelete)
                editTextPlaylistTitle.text
            }
        }
    }
}
