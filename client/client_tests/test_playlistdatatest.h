#ifndef TEST_PLAYLISTDATATEST_H
#define TEST_PLAYLISTDATATEST_H

#include <QTest>

class PlaylistDataTest : public QObject
{
    Q_OBJECT
public:
    PlaylistDataTest();

private slots:
    void creating();

    void getNextId();
    void getPrevId();
};

#endif // TEST_PLAYLISTDATATEST_H
