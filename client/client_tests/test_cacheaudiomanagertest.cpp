#include "test_cacheaudiomanagertest.h"
#include "../src/cacheaudiomanager.h"
#include "../src/vlcplayer.h"
#include "../src/timer.h"
#include <QFile>
#include <QByteArray>

CacheAudioManagerTest::CacheAudioManagerTest()
{}

//1)при неправильном параметре, пустой файл увеличится
//2)при неправильном параметре, содержимое файла не изменится
void CacheAudioManagerTest::positionChanged() {
    CacheAudioManager& cacheManager = CacheAudioManager::Instance();
    QString fileName = "mysong.wav";
    cacheManager.m_fileName = fileName;
    QFile file(fileName);
    file.open(QIODevice::ReadWrite | QIODevice::Append);
    file.close();
    cacheManager.positionChanged(-10);
    file.open(QIODevice::ReadWrite | QIODevice::Append);
    QCOMPARE(file.size(), 0);
    file.close();

    file.open(QIODevice::ReadWrite | QIODevice::Append);
    file.write("data");
    QByteArray arr = file.readAll();
    file.close();
    cacheManager.positionChanged(-10);
    file.open(QIODevice::ReadWrite | QIODevice::Append);
    QCOMPARE(file.readAll(), arr);
    file.remove();
    file.close();
}

//1)для пустого списка вернет ту же позицию
//2)для позиции за пределами списка вернет ту же позицию
//3)для позиции в списке вернет самую правую границу
void CacheAudioManagerTest::nearestByteOfUnavailableSection() {
    CacheAudioManager& cacheManager = CacheAudioManager::Instance();
    cacheManager.m_list.clear();
    quint32 pos = 1000;
    QCOMPARE(cacheManager.nearestByteOfUnavailableSection(pos), pos);
    cacheManager.m_list.append(CacheAudioManager::CacheElement(0, 44));
    QCOMPARE(cacheManager.nearestByteOfUnavailableSection(pos), pos);
    cacheManager.m_list.append(CacheAudioManager::CacheElement(100, 200));
    pos = 200;
    QCOMPARE(cacheManager.nearestByteOfUnavailableSection(100), pos);
}

//1)для пустого списка вернет позицию + partSize
//2)для позиции за списком вернет позицию + partSize
//3)для позиции, где конец ближайшей недоступной секции будет больше,
//  чем partSize, вернет позицию + partSize
void CacheAudioManagerTest::getEndOfUnavailableSectionByStartByte() {
    CacheAudioManager& cacheManager = CacheAudioManager::Instance();
    cacheManager.m_list.clear();
    const quint32 partSize = 100;
    const quint32 audioDuration = 1000;
    cacheManager.m_partBytesSize = partSize;
    cacheManager.m_audioDurationInBytes = audioDuration;
    quint32 pos = 100;
    QCOMPARE(cacheManager.getEndOfUnavailableSectionByStartByte(pos), pos + partSize);
    cacheManager.m_list.append(CacheAudioManager::CacheElement(0, 44));
    QCOMPARE(cacheManager.getEndOfUnavailableSectionByStartByte(pos), pos + partSize);
    cacheManager.m_list.append(CacheAudioManager::CacheElement(200, 300));
    pos = 50;
    QCOMPARE(cacheManager.getEndOfUnavailableSectionByStartByte(pos), pos + partSize);
}

//1)для пустого списка вернет end
//2)для позиции за списком вернет end
//3)для позиции на границе между секциями, вернет правую секцию
void CacheAudioManagerTest::getSectionByByte() {
    CacheAudioManager& cacheManager = CacheAudioManager::Instance();
    cacheManager.m_list.clear();
    quint32 pos = 1000;
    QVERIFY((cacheManager.getSectionByByte(pos)) == cacheManager.m_list.end());
    cacheManager.m_list.append(CacheAudioManager::CacheElement(0, 44));
    QVERIFY((cacheManager.getSectionByByte(pos)) == cacheManager.m_list.end());
    cacheManager.m_list.append(CacheAudioManager::CacheElement(44, 100));
    pos = 44;
    CacheAudioManager::listIter it = cacheManager.getSectionByByte(pos);
    QVERIFY(it != cacheManager.m_list.end()); //перед разыменованием - проверить на конец
    QCOMPARE(*(it), cacheManager.m_list.last());
}



