#include "test_audioplayertest.h"
#include <QScopedPointer>
#include "../src/audioplayer.h"
#include "../src/settings.h"
#include "../src/database.h"
#include "../src/playlist.h"
#include "../src/playlistdata.h"
#include "../src/song.h"

AudioPlayerTest::AudioPlayerTest()
{}

//1)объект создается
//2)все поля инициализируются
void AudioPlayerTest::creating() {
    QScopedPointer<AudioPlayer> audioPlayer(new AudioPlayer());
    QVERIFY(audioPlayer->m_curPlaylist != nullptr);
    QVERIFY(audioPlayer->m_player != nullptr);
}

//1)при вызове неправильного значения, громкость не изменится
//2)при вызове setVolume, громкость действительно изменится
void AudioPlayerTest::setVolume() {
    QScopedPointer<AudioPlayer> audioPlayer(new AudioPlayer());

    int volume = audioPlayer->getVolume();
    audioPlayer->setVolume(-10);
    QCOMPARE(audioPlayer->getVolume(), volume);

    audioPlayer->setVolume(10000);
    QCOMPARE(audioPlayer->getVolume(), volume);

    audioPlayer->setVolume(0);
    QCOMPARE(audioPlayer->getVolume(), 0);

    audioPlayer->setVolume(100);
    QCOMPARE(audioPlayer->getVolume(), 100);
}

//1)при первом вызове getVolume, вернет значение с последней сессии
void AudioPlayerTest::getVolume() {
    QScopedPointer<AudioPlayer> audioPlayer(new AudioPlayer());

    QCOMPARE(audioPlayer->getVolume(), Settings::Instance().getVolume());
}

//1)при создании плеера, плейлист будет с id = 0 (плейрист со всеми плейлистами)
//2)при установке плейлиста - он действительно будет текущим плейлистом
void AudioPlayerTest::setPlaylist() {
    QScopedPointer<AudioPlayer> audioPlayer(new AudioPlayer());

    ID id = 0;
    QCOMPARE(audioPlayer->m_curPlaylist->getId(), id);

    id = Database::Instance().m_playlistsData.last()->getId();
    audioPlayer->setPlaylist(id);
    QCOMPARE(audioPlayer->m_curPlaylist->getId(), id);
}
