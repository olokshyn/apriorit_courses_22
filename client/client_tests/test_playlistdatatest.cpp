#include "test_playlistdatatest.h"
#include "../src/playlistdata.h"
#include <QScopedPointer>

PlaylistDataTest::PlaylistDataTest()
{}

//1)все поля инициализируются нужными значениями
void PlaylistDataTest::creating() {
    ID id = 100;
    QString title = "title";
    QVector<ID> ids = {3,5,7};
    QScopedPointer<PlaylistData> playlistData(
                new PlaylistData(id, title, ids));
    QCOMPARE(playlistData->m_id, id);
    QCOMPARE(playlistData->m_title, title);
    QCOMPARE(playlistData->m_identificators.size(), ids.size());
    for(QVector<ID>::iterator it_original = ids.begin(),
        it_member = playlistData->m_identificators.begin(),
        it_end = playlistData->m_identificators.end();
        it_member != it_end; ++it_original, ++it_member) {
        QCOMPARE(*it_member, *it_original);
    }
}

//1)вернет тот же id, если передать такой, которого нет в плейлисте
//2)если передать последний id плейлиста, то вернется тот же id
//3)если передать не последний id, то вернет действительно следующий id
void PlaylistDataTest::getNextId() {
    ID id = 100;
    QString title = "title";
    QVector<ID> ids = {3,5,7};
    QScopedPointer<PlaylistData> playlistData(
                new PlaylistData(id, title, ids));
    id = 0;
    QCOMPARE(playlistData->getNextId(id), id);
    id = 7;
    QCOMPARE(playlistData->getNextId(id), id);

    id = 5;
    ID nextId = 7;
    QCOMPARE(playlistData->getNextId(id), nextId);
}

//1)вернет тот же id, если передать такой, которого нет в плейлисте
//2)если передать первый id плейлиста, то вернется тот же id
//3)если передать не первый id, то вернет действительно предыдущий id
void PlaylistDataTest::getPrevId() {
    ID id = 100;
    QString title = "title";
    QVector<ID> ids = {3,5,7};
    QScopedPointer<PlaylistData> playlistData(
                new PlaylistData(id, title, ids));
    id = 0;
    QCOMPARE(playlistData->getPrevId(id), id);
    id = 3;
    QCOMPARE(playlistData->getPrevId(id), id);

    id = 5;
    ID prevId = 3;
    QCOMPARE(playlistData->getPrevId(id), prevId);
}

