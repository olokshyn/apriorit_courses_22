#include <QTest>
#include <QApplication>
#include "test_audioplayertest.h"
#include "test_playlistdatatest.h"
#include "test_playlisttest.h"
#include "test_cacheaudiomanagertest.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    AudioPlayerTest test1;
    PlaylistDataTest test2;
    PlaylistTest test3;
    CacheAudioManagerTest test4;

    QTest::qExec(&test1, argc, argv);
    QTest::qExec(&test2, argc, argv);
    QTest::qExec(&test3, argc, argv);
    QTest::qExec(&test4, argc, argv);

    return app.exec();
}
