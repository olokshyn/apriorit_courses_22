#ifndef TEST_CACHEAUDIOMANAGERTEST_H
#define TEST_CACHEAUDIOMANAGERTEST_H

#include <QtTest>

class CacheAudioManagerTest : public QObject
{
    Q_OBJECT
public:
    CacheAudioManagerTest();

private slots:
    void positionChanged();
    void nearestByteOfUnavailableSection();
    void getEndOfUnavailableSectionByStartByte();
    void getSectionByByte();
};

#endif // TEST_CACHEAUDIOMANAGERTEST_H
