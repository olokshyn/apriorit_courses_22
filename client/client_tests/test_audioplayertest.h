#ifndef TEST_AUDIOPLAYERTEST_H
#define TEST_AUDIOPLAYERTEST_H

#include <QtTest>

class AudioPlayerTest : public QObject
{
    Q_OBJECT
public:
    AudioPlayerTest();

private slots:
    void creating();

    void setVolume();
    void getVolume();
    void setPlaylist();
};

#endif // TEST_AUDIOPLAYERTEST_H
