#include "test_playlisttest.h"
#include "../src/vlcplayer.h"
#include "../src/playlist.h"
#include "../src/playlistdata.h"
#include "../src/database.h"
#include <QSharedPointer>
#include <QScopedPointer>

PlaylistTest::PlaylistTest()
{}

//1)установленный плейлист с id = 0 - установится
//2)все поля инициализируются (кроме песни)
//3)текущий индекс 0
void PlaylistTest::creating() {
    QSharedPointer<VlcPlayer> audioPlayer (QSharedPointer<VlcPlayer>::create());
    ID id = 0;
    QScopedPointer<Playlist> playlist(new Playlist(audioPlayer, id));
    QCOMPARE(playlist->m_id, id);
    QVERIFY(playlist->m_curSong == nullptr);
    QCOMPARE(playlist->m_player.isNull(), false);
    QCOMPARE(playlist->m_playlistData.isNull(), false);
    unsigned int index = 0;
    QCOMPARE(playlist->m_curIndex, index);
}
