QT += quick qml widgets quickwidgets core sql
CONFIG += c++11
DEFINES += QT_DEPRECATED_WARNINGS


test {
    QT += testlib
    HEADERS += \
        client_tests/test_audioplayertest.h \
        client_tests/test_playlisttest.h \
        client_tests/test_playlistdatatest.h \
        client_tests/test_cacheaudiomanagertest.h
    SOURCES += client_tests/main.cpp \
        client_tests/test_audioplayertest.cpp \
        client_tests/test_playlisttest.cpp \
        client_tests/test_playlistdatatest.cpp \
        client_tests/test_cacheaudiomanagertest.cpp

} else {
    SOURCES += main.cpp
}

HEADERS += \
    src/audioplayer.h \
    src/playlist.h \
    src/song.h \
    src/playlistdata.h \
    src/metadata/metadata.h \
    src/Database/database.h \
    src/commondefines.h \
    src/ListModel/ListModel.h \
    src/ListModel/ListItem.h \
    src/ListModel/metadatamodel.h \
    ../protobuf//ms.pb.h \
    ../protobuf//ms.grpc.pb.h \
    src/ListModel/modelconnection.h \
    src/settings.h \
    src/ListModel/comboboxmodel.h \
    src/CacheManager/cacheaudiomanager.h \
    src/CacheManager/timer.h \
    src/metadata/metadatafactory.h \
    src/metadata/metadatatype.h \
    src/grpcClient/grpcmanager.h \
    src/grpcClient/syncworker.h \
    src/grpcClient/sendworker.h \
    src/ListModel/albumsmodel.h \
    src/grpcClient/grpcclient.h \
    src/CacheManager/cacheconnection.h \
    src/vlcplayer.h \
    src/Database/databaseworker.h

SOURCES += \
    src/audioplayer.cpp \
    src/playlist.cpp \
    src/song.cpp \
    src/playlistdata.cpp \
    src/metadata/metadata.cpp \
    src/Database/database.cpp \
    src/ListModel/ListModel.cpp \
    src/ListModel/metadatamodel.cpp \
    ../protobuf/ms.pb.cc \
    ../protobuf/ms.grpc.pb.cc \
    src/ListModel/modelconnection.cpp \
    src/settings.cpp \
    src/ListModel/comboboxmodel.cpp \
    src/CacheManager/cacheaudiomanager.cpp \
    src/CacheManager/timer.cpp \
    src/metadata/metadatafactory.cpp \
    src/metadata/metadatatype.cpp \
    src/grpcClient/grpcmanager.cpp \
    src/grpcClient/syncworker.cpp \
    src/grpcClient/sendworker.cpp \
    src/ListModel/albumsmodel.cpp \
    src/grpcClient/grpcclient.cpp \
    src/CacheManager/cacheconnection.cpp \
    src/vlcplayer.cpp \
    src/Database/databaseworker.cpp

RESOURCES += gui_qml/qml.qrc

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


#grpc, protobuf
LIBS += -L/usr/local/bin -lprotobuf \
    -lrt \
     -L/usr/local/include/grpc++ -lgrpc++

#vlc-qt
LIBS       += -lVLCQtCore -lVLCQtWidgets
